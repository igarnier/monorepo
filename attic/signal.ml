module A = Bigarray.Array1

type radian_p_s = Radian_per_second of float [@@inline]

type hertz = Hertz of float [@@inline]

type second = Second of float [@@inline]

let round_to_even i = if i mod 2 = 0 then i else i + 1

module Time = struct
  type t =
    { data : Owl_fft.float64_vec; ti : second; freq : hertz; period : float }

  let of_raw ti (Hertz hz as freq) data =
    let period = 1.0 /. hz in
    { data; ti; freq; period }

  let discretise :
      signal:(second -> float) ->
      sample_freq:hertz ->
      rec_interval:second * second ->
      t =
   fun ~signal
       ~sample_freq:(Hertz hz)
       ~rec_interval:(Second rec_start, Second rec_end) ->
    if rec_end <= rec_start then
      invalid_arg "discretise: empty recording interval. Aborting."
    else
      let duration = rec_end -. rec_start in
      let num_samples = round_to_even (truncate (duration *. hz)) in
      let period = 1.0 /. hz in
      let data =
        Array.init num_samples (fun i ->
            signal (Second (rec_start +. (float_of_int i *. period))))
        |> A.of_array Bigarray.float64 Bigarray.c_layout
      in
      { data; ti = Second rec_start; freq = Hertz hz; period }
end

module Freq = struct
  type t =
    { data : Owl_fft.complex64_vec;
      ti : second;
      freq : hertz;
      period : float;
      resl : hertz
    }

  let length { data; _ } = A.dim data

  let get_index_freq signal index =
    let (Hertz resl) = signal.resl in
    Hertz (float_of_int index *. resl)
    [@@inline]

  let get_freq_index signal (Hertz hz : hertz) =
    let (Hertz resl) = signal.resl in
    int_of_float (hz /. resl)
    [@@inline]

  let get_freq signal (fr : hertz) = signal.data.{get_freq_index signal fr}
end

let fft : Time.t -> Freq.t =
 fun ({ data; ti; freq = Hertz hz; period } as signal) ->
  let dim = A.dim data in
  let data = Owl_fft.rfft data in

  { data; ti; freq = signal.freq; period; resl = Hertz (hz /. float dim) }

let ifft : Freq.t -> Time.t =
 fun ({ data; _ } as signal) ->
  let dim = Freq.length signal in
  let data = Owl_fft.irfft data in
  let scale = 1.0 /. float (2 * (dim - 1)) in
  for i = 0 to A.dim data - 1 do
    data.{i} <- data.{i} *. scale
  done ;
  { Time.data; ti = signal.ti; freq = signal.freq; period = signal.period }

let ifft_unscaled : Freq.t -> Time.t =
 fun ({ data; _ } as signal) ->
  let data = Owl_fft.irfft data in
  { Time.data; ti = signal.ti; freq = signal.freq; period = signal.period }

let autocorrelation (s : Time.t) =
  let s_hat = fft s in
  let data = s_hat.data in
  for i = 0 to A.dim s_hat.data - 1 do
    data.{i} <- { re = Complex.norm2 data.{i}; im = 0.0 }
  done ;
  ifft s_hat

(* let convolution (s1 : Time.t) (s2 : Time.t) =
 *   let (Hertz fre1) = s1.freq in
 *   let (Hertz fre2) = s2.freq in
 *   if fre1 <> fre2 then
 *     Format.kasprintf
 *       invalid_arg
 *       "convolution: signals have been sampled at different frequencies: %f vs \
 *        %f. Aborting."
 *       fre1
 *       fre2 ;
 *   (\* Pad the signals so that they start at the same time. *\)
 *   (\* TODO: instead of checking for equality of ti1, ti2, check for subsample-almost-equality.*\)
 *   let (Second ti1) = s1.ti in
 *   let (Second ti2) = s2.ti in
 *   let (fft1, fft2) =
 *     let (s1, s2) =
 *       match Float.compare ti1 ti2 with
 *       | 0 -> (s1, s2)
 *       | -1 ->
 *           let deltat = ti2 -. ti1 in
 *           let s2 = Time.prepend_zero_padding_t s2 deltat in
 *           (s1, s2)
 *       | 1 ->
 *           let deltat = ti1 -. ti2 in
 *           let s1 = Time.prepend_zero_padding_t s1 deltat in
 *           (s1, s2)
 *     in
 *     (\* Pad the signals s.t. their number of samples is the smallest power of 2 (for efficiency)
 *          above the sum of the length of the signals (to store all delays) *\)
 *     let len1 = Time.length s1 in
 *     let len2 = Time.length s2 in
 *     let target = minpow2 (len1 + len2 - 1) in
 *     let s1 = Time.append_zero_padding s1 (target - len1) in
 *     let s2 = Time.append_zero_padding s2 (target - len2) in
 *     (fft s1, fft s2)
 *   in
 *   let prod = ZVec.mul (Freq.data fft1) (Freq.data fft2) in
 *   (\* we put dummy nan values into the signal - these should not get out of this function anyway. *\)
 *   let fsignal =
 *     Freq.
 *       { data = prod;
 *         ti = nan;
 *         freq = hertz nan;
 *         period = nan;
 *         resl = hertz nan
 *       }
 *   in
 *   let tsignal = ifft fsignal in
 *   let data =
 *     Array1.sub
 *       (Tools.array1_of_vector (Time.data tsignal))
 *       0
 *       (Time.length s1 + Time.length s2 - 1)
 *   in
 *   { Time.data = Tools.vector_of_array1 data;
 *     (\* TODO: sub to len1 + len2 - 1 *\)
 *     Time.freq = Dims.hertz fre1;
 *     Time.period = 1.0 /. fre1;
 *     Time.ti = ti2 -. (ti1 +. Time.duration s1)
 *   } *)

(* let reverse : Time.t -> Time.t =
 *  fun signal ->
 *   let len = Time.length signal in
 *   let data = Vec.clone (Time.data signal) in
 *   for i = 0 to (len / 2) - 1 do
 *     let tmp = Vec.get data i in
 *     Vec.set data i (Vec.get data (len - i - 1)) ;
 *     Vec.set data (len - i - 1) tmp
 *   done ;
 *   { signal with Time.data }
 *
 * let cross_correlation s1 s2 = convolution (reverse s1) s2 *)
