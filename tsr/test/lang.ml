(* Dot export *)
open Core_diff
open Error_monad

let ( + ) = add

let ( * ) = mul

let f x = 
  let* mul = x * x in
  let* mul = mul * x in
  scalar 10.0 + mul 

let grad = run @@ differentiate f

let () = Format.printf "%a@." Core.pp grad

let () =
  Out_channel.with_open_text "out.dot" @@ fun oc -> 
  Core.Dot.to_dot oc (Core.Dot.to_graph grad)

(*let () =
  let graph = Lang.Ground.to_graph ground in
  Out_channel.with_open_text "affine_grad.dot" @@ fun oc ->
  Lang.Ground.Dot.output_graph oc graph
*)
