open Tsr_shape

type 'a t

type ('a, 'elt) tensor = ('a, 'elt) Core.tensor Core.t

type 'a vec = 'a Core.vec

type ('a, 'b) mat = ('a * 'b) vec

type 'a boolean = 'a Core.boolean

type index = Core.index

val array :
  (float, Bigarray.float64_elt, Bigarray.c_layout) Bigarray.Array1.t ->
  int vec t

val zero : 'a Shape.t -> 'a vec t

val scalar : float -> index vec t

val add : 'a vec t -> 'a vec t -> 'a vec t Error_monad.t

val sub : 'a vec t -> 'a vec t -> 'a vec t Error_monad.t

val mul : 'a vec t -> 'a vec t -> 'a vec t Error_monad.t

val div : 'a vec t -> 'a vec t -> 'a vec t Error_monad.t

val and_ : 'a boolean t -> 'a boolean t -> 'a boolean t Error_monad.t

val or_ : 'a boolean t -> 'a boolean t -> 'a boolean t Error_monad.t

val neg : 'a vec t -> 'a vec t Error_monad.t

val sqrt : 'a vec t -> 'a vec t Error_monad.t

val recip : 'a vec t -> 'a vec t Error_monad.t

val get : ('a, 'b) tensor t -> 'a -> (index, 'b) tensor t Error_monad.t

val cond :
  'a boolean t ->
  ('a, 'elt) tensor t ->
  ('a, 'elt) tensor t ->
  ('a, 'elt) tensor t Error_monad.t

val true_ : 'a Shape.t -> 'a boolean t Error_monad.t

val false_ : 'a Shape.t -> 'a boolean t Error_monad.t

val mm :
  ('o, 'm) mat t -> ('m, 'i) mat t -> ('o * 'i, float) tensor t Error_monad.t

val dot : 'a vec t -> 'a vec t -> index vec t Error_monad.t

val slice :
  ('a, 'elt) tensor t ->
  ('a, 'b) Shape.slice ->
  ('b, 'elt) tensor t Error_monad.t

val contract : 'a vec t -> ('a, 'b) Shape.path -> 'b vec t Error_monad.t

val diagonal :
  'base vec t -> ('base, 'diag) Shape.diagonal -> 'diag vec t Error_monad.t

val iso :
  ('a, 'elt) tensor t -> ('a, 'b) Shape.iso -> ('b, 'elt) tensor t Error_monad.t

val differentiate :
  ?shape:'input Shape.t ->
  ('input vec t -> 'output vec t Error_monad.t) ->
  (('output * 'input) vec, Error_monad.error list) result

val reflect : 'a 'elt. ('a, 'elt) tensor -> ('a, 'elt) tensor t Error_monad.t

module WithExn : sig
  val ( + ) : 'a vec t -> 'a vec t -> 'a vec t

  val ( - ) : 'a vec t -> 'a vec t -> 'a vec t

  val ( * ) : 'a vec t -> 'a vec t -> 'a vec t

  val ( / ) : 'a vec t -> 'a vec t -> 'a vec t

  val array :
    (float, Bigarray.float64_elt, Bigarray.c_layout) Bigarray.Array1.t ->
    int vec t

  val zero : 'a Shape.t -> 'a vec t

  val scalar : float -> index vec t

  val and_ : 'a boolean t -> 'a boolean t -> 'a boolean t

  val or_ : 'a boolean t -> 'a boolean t -> 'a boolean t

  val true_ : 'a Shape.t -> 'a boolean t

  val false_ : 'a Shape.t -> 'a boolean t

  val ( ~- ) : 'a vec t -> 'a vec t

  val sqrt : 'a vec t -> 'a vec t

  val recip : 'a vec t -> 'a vec t

  val get : ('a, 'b) tensor t -> 'a -> (index, 'b) tensor t

  val cond :
    'a boolean t ->
    ('a, 'elt) tensor t ->
    ('a, 'elt) tensor t ->
    ('a, 'elt) tensor t

  val mm : ('o, 'm) mat t -> ('m, 'i) mat t -> ('o * 'i, float) tensor t

  val dot : 'a vec t -> 'a vec t -> index vec t

  val slice : ('a, 'elt) tensor t -> ('a, 'b) Shape.slice -> ('b, 'elt) tensor t

  val contract : 'a vec t -> ('a, 'b) Shape.path -> 'b vec t

  val diagonal : 'base vec t -> ('base, 'diag) Shape.diagonal -> 'diag vec t

  val iso : ('a, 'elt) tensor t -> ('a, 'b) Shape.iso -> ('b, 'elt) tensor t
end
