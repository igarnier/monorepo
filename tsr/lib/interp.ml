(* Naive interpreter for [Flat.t] expressions *)
open Bigarray

type 'elt tensor =
  { kind : 'elt Core.elt_kind; shape : Flat.shape; data : Flat.index -> 'elt }

type float_genarray = (float, float64_elt, c_layout) Genarray.t

type char_genarray = (char, int8_unsigned_elt, c_layout) Genarray.t

type 'elt materialized_tensor =
  | Float : float_genarray -> float materialized_tensor
  | Bool : char_genarray -> bool materialized_tensor

type 'elt ex_kind =
  | Ex_kind_float : (float, float64_elt) kind -> float ex_kind
  | Ex_kind_bool : (char, int8_unsigned_elt) kind -> bool ex_kind

let ( .%{} ) = Bigarray.Genarray.get

let ( .%{}<- ) = Bigarray.Genarray.set

let bool_of_char = function '\x00' -> false | _ -> true

let char_of_bool = function false -> '\x00' | _ -> '\x01'

let bigarray_kind_of_kind : type elt. elt Core.elt_kind -> elt ex_kind =
 fun kind ->
  match kind with
  | Core.Float -> Ex_kind_float Float64
  | Core.Bool -> Ex_kind_bool Char

let rematerialize : type elt. elt tensor -> elt materialized_tensor =
 fun { kind; shape; data } ->
  match bigarray_kind_of_kind kind with
  | Ex_kind_float k -> Float (Genarray.init k c_layout shape data)
  | Ex_kind_bool k ->
      Bool (Genarray.init k c_layout shape (fun i -> char_of_bool (data i)))

let dematerialize : type elt. elt materialized_tensor -> elt tensor =
 fun t ->
  match t with
  | Float ba ->
      { kind = Core.Float;
        shape = Genarray.dims ba;
        data = (fun i -> Genarray.get ba i)
      }
  | Bool ba ->
      { kind = Core.Bool;
        shape = Genarray.dims ba;
        data = (fun i -> Genarray.get ba i |> bool_of_char)
      }

let tensor_of_bigarray ba =
  let shape = Flat.shape_of_bigarray ba in
  { kind = Core.Float; shape; data = (fun i -> ba.{i.(0)}) }

let const_tensor kind shape f = { kind; shape; data = (fun _ -> f) }

(* TODO: is this needed? *)
let iter_sub_shape (sub_shape : [ `fixed of int | `loop of int ] array)
    (f : int array -> unit) =
  let (fixed_domain, iter_domain, _) =
    Array.fold_left
      (fun (fixed, iter, i) spec ->
        match spec with
        | `fixed index -> ((i, index) :: fixed, iter, i + 1)
        | `loop len -> (fixed, (i, len) :: iter, i + 1))
      ([], [], 0)
      sub_shape
  in
  let iter_domain_indices = List.rev_map fst iter_domain |> Array.of_list in
  let iter_domain_values = List.rev_map snd iter_domain |> Array.of_list in
  let index = Array.make (Array.length sub_shape) 0 in
  List.iter (fun (i, fixed_i) -> index.(i) <- fixed_i) fixed_domain ;
  let rec loop i =
    if i = Array.length iter_domain_indices then f index
    else
      let index_i = iter_domain_indices.(i) in
      let len = iter_domain_values.(i) in
      for j = 0 to len - 1 do
        index.(index_i) <- j ;
        loop (i + 1)
      done
  in
  loop 0

let rec iter_shape_raw (shape : Flat.shape) (acc : int array) (i : int)
    (last : int) (f : int array -> unit) =
  if i >= last then f acc
  else
    let len = shape.(i) in
    for j = 0 to len - 1 do
      acc.(i) <- j ;
      iter_shape_raw shape acc (i + 1) last f
    done

let iter_shape (shape : Flat.shape) (f : int array -> unit) =
  let len = Array.length shape in
  iter_shape_raw shape (Array.make len 0) 0 len f

let apply_unop_float (ba : float_genarray) (op : float -> float) =
  let shape = Genarray.dims ba in
  let res = Genarray.create Float64 c_layout shape in
  iter_shape shape (fun i -> Genarray.set res i (op ba.%{i})) ;
  res

let apply_unop_bool (ba : char_genarray) (op : bool -> bool) =
  let shape = Genarray.dims ba in
  let res = Genarray.create Char c_layout shape in
  iter_shape shape (fun i ->
      Genarray.set res i (char_of_bool @@ op (bool_of_char ba.%{i}))) ;
  res

let apply_binop_float (ba1 : float_genarray) (ba2 : float_genarray)
    (op : float -> float -> float) : float_genarray =
  let shape = Genarray.dims ba1 in
  assert (Flat.equal_shape shape (Genarray.dims ba2)) ;
  let res = Genarray.create Float64 c_layout shape in
  iter_shape shape (fun i -> Genarray.set res i (op ba1.%{i} ba2.%{i})) ;
  res

let apply_binop_bool (ba1 : char_genarray) (ba2 : char_genarray)
    (op : bool -> bool -> bool) : char_genarray =
  let shape = Genarray.dims ba1 in
  assert (Flat.equal_shape shape (Genarray.dims ba2)) ;
  let res = Genarray.create Char c_layout shape in
  iter_shape shape (fun i ->
      Genarray.set
        res
        i
        (char_of_bool @@ op (bool_of_char ba1.%{i}) (bool_of_char ba2.%{i}))) ;
  res

let apply_unop :
    type elt. elt materialized_tensor -> (elt -> elt) -> elt materialized_tensor
    =
 fun t op ->
  match t with
  | Float ba -> Float (apply_unop_float ba op)
  | Bool ba -> Bool (apply_unop_bool ba op)

let apply_binop :
    type elt.
    elt materialized_tensor ->
    elt materialized_tensor ->
    (elt -> elt -> elt) ->
    elt materialized_tensor =
 fun t1 t2 op ->
  match (t1, t2) with
  | (Float ba1, Float ba2) -> Float (apply_binop_float ba1 ba2 op)
  | (Bool ba1, Bool ba2) -> Bool (apply_binop_bool ba1 ba2 op)

let dot_product (ba1 : float_genarray) (ba2 : float_genarray) : float =
  let shape = Genarray.dims ba1 in
  assert (Flat.equal_shape shape (Genarray.dims ba2)) ;
  let res = ref 0.0 in
  iter_shape shape (fun i -> res := !res +. (ba1.%{i} *. ba2.%{i})) ;
  !res

let matmul (mid_dim : int) (lhs : float_genarray) (rhs : float_genarray) :
    float_genarray =
  let lhs_shape = Genarray.dims lhs in
  let rhs_shape = Genarray.dims rhs in
  let common_shape = Array.sub rhs_shape 0 mid_dim in

  let row_dim = Array.length lhs_shape - mid_dim in
  let col_dim = Array.length rhs_shape - mid_dim in
  let row_shape = Array.sub lhs_shape 0 row_dim in
  let col_shape = Array.sub rhs_shape mid_dim col_dim in
  if not (common_shape = Array.sub lhs_shape row_dim mid_dim) then (
    Format.eprintf "lhs: %a@." Helpers.pp_shape lhs_shape ;
    invalid_arg "matmul") ;
  let res_shape = Array.concat [row_shape; col_shape] in
  let result = Genarray.create Float64 c_layout res_shape in

  let result_index = Array.make (row_dim + col_dim) 0 in
  let lhs_index = Array.make (row_dim + mid_dim) 0 in
  let rhs_index = Array.make (mid_dim + col_dim) 0 in

  let rec col_iter col_i =
    if col_i >= col_dim then row_iter 0
    else
      let len = col_shape.(col_i) in
      for j = 0 to len - 1 do
        rhs_index.(mid_dim + col_i) <- j ;
        result_index.(row_dim + col_i) <- j ;
        col_iter (col_i + 1)
      done
  and row_iter row_i =
    if row_i >= row_dim then (
      let acc = ref 0.0 in
      mid_iter 0 acc ;
      result.%{result_index} <- !acc)
    else
      let len = row_shape.(row_i) in
      for j = 0 to len - 1 do
        lhs_index.(row_i) <- j ;
        result_index.(row_i) <- j ;
        row_iter (row_i + 1)
      done
  and mid_iter mid_i acc =
    if mid_i >= mid_dim then
      acc := !acc +. (lhs.%{lhs_index} *. rhs.%{rhs_index})
    else
      let len = common_shape.(mid_i) in
      for j = 0 to len - 1 do
        lhs_index.(row_dim + mid_i) <- j ;
        rhs_index.(mid_i) <- j ;
        mid_iter (mid_i + 1) acc
      done
  in
  col_iter 0 ;
  result

let contract (tensor : float_genarray) (path : bool array) =
  let shape = Genarray.dims tensor in
  assert (Array.length path = Array.length shape) ;
  let reduced_count =
    Array.fold_left (fun acc b -> if b then acc + 1 else acc) 0 path
  in
  let kept_count = Array.length shape - reduced_count in
  let kept_shape = Array.make kept_count 0 in
  let kept_embed = Array.make kept_count 0 in
  let reduced_shape = Array.make reduced_count 0 in
  let reduced_embed = Array.make reduced_count 0 in
  let kept_index = ref 0 in
  let reduced_index = ref 0 in
  for i = 0 to Array.length path - 1 do
    if path.(i) then (
      reduced_shape.(!reduced_index) <- shape.(i) ;
      reduced_embed.(!reduced_index) <- i ;
      incr reduced_index)
    else (
      kept_shape.(!kept_index) <- shape.(i) ;
      kept_embed.(!kept_index) <- i ;
      incr kept_index)
  done ;
  let result = Genarray.create Float64 c_layout kept_shape in

  let kept_index = Array.make kept_count 0 in
  let tensor_index = Array.make (Array.length shape) 0 in

  let rec iter_kept i =
    if i >= kept_count then (
      let acc = ref 0.0 in
      iter_reduced 0 acc ;
      result.%{kept_index} <- !acc)
    else
      let len = kept_shape.(i) in
      for j = 0 to len - 1 do
        tensor_index.(kept_embed.(i)) <- j ;
        kept_index.(i) <- j ;
        iter_kept (i + 1)
      done
  and iter_reduced i acc =
    if i >= reduced_count then acc := !acc +. tensor.%{tensor_index}
    else
      let len = reduced_shape.(i) in
      for j = 0 to len - 1 do
        tensor_index.(reduced_embed.(i)) <- j ;
        iter_reduced (i + 1) acc
      done
  in
  iter_kept 0 ;
  result

let is_index_in_slice update_shape slice index =
  assert (Array.length slice = Array.length index) ;
  let result = Array.make (Array.length update_shape) 0 in
  let exception Not_in_slice in
  try
    let c = ref 0 in
    for i = 0 to Array.length slice - 1 do
      match slice.(i) with
      | None ->
          assert (index.(i) < update_shape.(i)) ;
          result.(!c) <- index.(i) ;
          incr c
      | Some k -> if index.(i) <> k then raise Not_in_slice
    done ;
    assert (!c = Array.length update_shape) ;
    Some result
  with Not_in_slice -> None

let rec interp :
    type elt. elt Flat.t -> float tensor -> elt tensor Error_monad.t =
  let open Error_monad in
  fun (Flat.Flat { tag = _; desc; shape }) t ->
    match desc with
    | Var -> return t
    | Float f -> return (const_tensor Core.Float shape f)
    | True -> return (const_tensor Core.Bool shape true)
    | False -> return (const_tensor Core.Bool shape false)
    | Array ba ->
        let result = tensor_of_bigarray ba in
        assert (Flat.equal_shape shape result.shape) ;
        return result
    | Binop (op, l, r) ->
        let* l = interp l t in
        let* r = interp r t in
        interp_binop op l r
    | Unop (op, e) ->
        let* e = interp e t in
        interp_unop op e
    | Get (e, i) ->
        let* e = interp e t in
        return (const_tensor e.kind shape (e.data i))
    | Cond (cond, ift, iff) ->
        let* cond = interp cond t in
        let* ift = interp ift t in
        let* iff = interp iff t in
        return
          { kind = ift.kind;
            shape = ift.shape;
            data = (fun i -> if cond.data i then ift.data i else iff.data i)
          }
    | Set_slice { operand; update; slice } ->
        let* operand = interp operand t in
        let* update = interp update t in
        return
          { kind = operand.kind;
            shape = operand.shape;
            data =
              (fun i ->
                match is_index_in_slice update.shape slice i with
                | None -> operand.data i
                | Some i -> update.data i)
          }
    | Contract (e, path) ->
        let* e = interp e t in
        let (Float e) = rematerialize e in
        let result = contract e path in
        return (dematerialize (Float result))
    | Broadcast (e, path) ->
        let broadcast_shape = shape in
        assert (Array.length path = Array.length broadcast_shape) ;
        let* e = interp e t in
        let actual_shape = e.shape in
        (* kept_indices: actual_shape -> broadcast_shape *)
        let indices =
          Array.to_seq path
          |> Seq.mapi (fun i b -> if b then Some i else None)
          |> Seq.filter_map Fun.id |> Array.of_seq
        in
        return
          { kind = e.kind;
            shape;
            data =
              (fun broadcast_i ->
                let index =
                  Array.init (Array.length actual_shape) (fun i ->
                      broadcast_i.(indices.(i)))
                in
                e.data index)
          }
    | Slice (e, slice) ->
        let* e = interp e t in
        let index =
          Array.init (Array.length e.shape) (fun i ->
              match slice.(i) with Some k -> k | None -> 0)
        in
        let embd =
          slice |> Array.to_seq
          |> Seq.mapi (fun i opt ->
                 match opt with Some _ -> None | None -> Some i)
          |> Seq.filter_map Fun.id |> Array.of_seq
        in
        return
          { kind = e.kind;
            shape;
            data =
              (fun slice_index ->
                for i = 0 to Array.length slice_index - 1 do
                  index.(embd.(i)) <- slice_index.(i)
                done ;
                e.data index)
          }
    | Iso (e, iso) ->
        let* e = interp e t in
        return
          { kind = e.kind;
            shape;
            data =
              (fun i ->
                let index =
                  Array.init (Array.length shape) (fun j -> i.(iso.(j)))
                in
                e.data index)
          }
    | Diagonal (e, (base_shape, dim)) ->
        assert (dim > 0) ;
        let* e = interp e t in
        assert (base_shape = e.shape) ;
        let base_shape = e.shape in
        let base_shape_len = Array.length base_shape in
        return
          { kind = e.kind;
            shape;
            data =
              (fun i ->
                let index = Array.sub i 0 base_shape_len in
                let exception Break in
                try
                  (* All sub-indices must be equal in order to be on the diagonal. *)
                  for j = 1 to dim - 1 do
                    let index' =
                      Array.sub i (base_shape_len * j) base_shape_len
                    in
                    if index <> index' then raise Break
                  done ;
                  e.data index
                with Break -> 0.0)
          }

and interp_binop :
    type a b c.
    (a, b, c) Flat.binop -> a tensor -> b tensor -> c tensor Error_monad.t =
 fun op l r ->
  let open Error_monad in
  let l = rematerialize l in
  let r = rematerialize r in
  match op with
  | Add -> return (apply_binop l r ( +. ) |> dematerialize)
  | Sub -> return (apply_binop l r ( -. ) |> dematerialize)
  | Mul -> return (apply_binop l r ( *. ) |> dematerialize)
  | Div -> return (apply_binop l r ( /. ) |> dematerialize)
  | Matmul { mid } ->
      let (Float l) = l in
      let (Float r) = r in
      Float (matmul mid l r) |> dematerialize |> return
  | Dot ->
      let (Float l) = l in
      let (Float r) = r in
      return (dot_product l r |> const_tensor Core.Float [| 1 |])
  | And -> return (apply_binop l r ( && ) |> dematerialize)
  | Or -> return (apply_binop l r ( || ) |> dematerialize)

and interp_unop :
    type a b. (a, b) Flat.unop -> a tensor -> b tensor Error_monad.t =
 fun op e ->
  let open Error_monad in
  let e = rematerialize e in
  match op with
  | Neg -> return (apply_unop e Float.neg |> dematerialize)
  | Sqrt -> return (apply_unop e sqrt |> dematerialize)
  | Recip -> return (apply_unop e (fun x -> 1. /. x) |> dematerialize)

(* let rec iter_shape shape f = *)
(*   match shape with *)
(*   | [| |] -> f [||] *)
(*   | [| d |] -> *)
(*       for i = 0 to d - 1 do *)
(*         f [| i |] *)
(*       done *)
(*   | [| d; d' |] -> *)
(*       for i = 0 to d - 1 do *)
(*         for j = 0 to d' - 1 do *)
(*           f [| i; j |] *)
(*         done *)
(*       done *)
(*   | [| d; d'; d'' |] -> *)
(*       for i = 0 to d - 1 do *)
(*         for j = 0 to d' - 1 do *)
(*           for k = 0 to d'' - 1 do *)
(*             f [| i; j; k |] *)
(*           done *)
(*         done *)
(*       done *)
(*   | [| d; d'; d''; d''' |] -> *)
(*       for i = 0 to d - 1 do *)
(*         for j = 0 to d' - 1 do *)
(*           for k = 0 to d'' - 1 do *)
(*             for l = 0 to d''' - 1 do *)
(*               f [| i; j; k; l |] *)
(*             done *)
(*           done *)
(*         done *)
(*       done *)
(*   | [| d; d'; d''; d'''; d'''' |] -> *)
(*       for i = 0 to d - 1 do *)
(*         for j = 0 to d' - 1 do *)
(*           for k = 0 to d'' - 1 do *)
(*             for l = 0 to d''' - 1 do *)
(*               for m = 0 to d'''' - 1 do *)
(*                 f [| i; j; k; l; m |] *)
(*               done *)
(*             done *)
(*           done *)
(*         done *)
(*       done *)
(*   | _ -> assert false *)
