module C = Core
module Shape = Tsr_shape.Shape

type ('a, 'elt) tensor = ('a, 'elt) C.tensor C.t

type 'a vec = 'a C.vec

type 'a boolean = 'a C.boolean

type index = Core.index

type ('a, 'b) mat = ('a * 'b) vec

type 'elt elt_kind = 'elt C.elt_kind

(*
   During backpropagation, the jacobians acts on the accumulator from the right:

   accumulator : (node_output, global_output) mat
   jacobian_i : (node_output_i, node_output) mat

   accumulator_i : (node_output_i, global_output) += mm accumulator jacobian_i

   hence we define jacobians as maps implementing (accumulator |-> mm accumulator jacobian_i)
*)
type ('inputs, 'outputs) jacobian =
  { form : 'x. acc:('x, 'outputs) mat -> ('x, 'inputs) mat option Error_monad.t
  }

type (_, _, _) internal_spec =
  | Returning : 'res_shape Shape.t -> (unit, unit, 'res_shape) internal_spec
  | Arg :
      'elt elt_kind * ('args, 'ja_res, 'res_shape) internal_spec
      -> ( ('a, 'elt) tensor * 'args,
           ('a, 'res_shape) jacobian * 'ja_res,
           'res_shape )
         internal_spec

let rec get_res_shape_from_internal_spec :
    type a b s. (a, b, s) internal_spec -> s Shape.t =
 fun spec ->
  match spec with
  | Returning shape -> shape
  | Arg (_kind, spec) -> get_res_shape_from_internal_spec spec

type _ accu =
  (* Existentially quantify over global output type *)
  | Accu : ('glob_out * 'ret, float) tensor -> 'ret accu
  | Zero_accu : 'a accu

module Node = struct
  type _ kernel =
    | Diff :
        { spec : ('args, 'ja_res, 'res_shape) internal_spec;
          elt_ty : 'elt elt_kind;
          bwd : 'args node_vector;
          forward : 'args -> ('res_shape, 'elt) tensor Error_monad.t;
          jacobian : 'args -> 'ja_res Error_monad.t
        }
        -> ('args -> ('res_shape, 'elt) tensor) kernel

  and ('args, 'ret) node =
    | Node :
        { id : int;
          tag : string;
          mutable fwd : opaque list;
          mutable value : ('ret_shape, 'elt) tensor option;
          mutable accu : 'ret_shape accu;
          kernel : ('args -> ('ret_shape, 'elt) tensor) kernel
        }
        -> ('args, ('ret_shape, 'elt) tensor) node

  and _ t = Applied : ('args, 'ret) node -> 'ret t

  and opaque = Opaque : 'ret t -> opaque

  and _ node_vector =
    | [] : unit node_vector
    | ( :: ) :
        ('a, 'elt) tensor t * 'b node_vector
        -> (('a, 'elt) tensor * 'b) node_vector

  type (_, _, _) spec =
    | Ret : 'res_shape Shape.t -> (unit, unit, 'res_shape) spec
    | Arg :
        ('a, 'elt) tensor t * ('args, 'ja_res, 'res_shape) spec
        -> ( ('a, 'elt) tensor * 'args,
             ('a, 'res_shape) jacobian * 'ja_res,
             'res_shape )
           spec

  let return shape = Ret shape

  let elt_type (Applied (Node { kernel; _ })) =
    let (Diff { elt_ty; _ }) = kernel in
    elt_ty

  let ( @-> ) node spec = Arg (node, spec)

  let rec proj_backward : type a b c. (a, b, c) spec -> a node_vector = function
    | Ret _ -> []
    | Arg (node, rest) -> node :: proj_backward rest

  let rec proj_spec : type a b c. (a, b, c) spec -> (a, b, c) internal_spec =
    function
    | Ret shape -> Returning shape
    | Arg (node, rest) -> Arg (elt_type node, proj_spec rest)

  let fresh =
    let x = ref 0 in
    fun () ->
      let v = !x in
      incr x ;
      v

  let rec iter_vec : type a. (opaque -> unit) -> a node_vector -> unit =
   fun f vec ->
    match vec with
    | [] -> ()
    | node :: tl ->
        f (Opaque node) ;
        iter_vec f tl

  let rec iter_vec_e :
      type a.
      (opaque -> unit Error_monad.t) -> a node_vector -> unit Error_monad.t =
   fun f vec ->
    let open Error_monad in
    match vec with
    | [] -> return_unit
    | node :: tl ->
        let* () = f (Opaque node) in
        iter_vec_e f tl

  let rec fold_vec :
      type a acc. (opaque -> acc -> acc) -> a node_vector -> acc -> acc =
   fun f vec acc ->
    match vec with
    | [] -> acc
    | node :: tl -> fold_vec f tl (f (Opaque node) acc)

  (* TODO Optim: do not store twice the same node in [fwd] *)
  let add_fwd : type a. a t -> opaque -> unit =
   fun node fwd ->
    match node with Applied (Node node) -> node.fwd <- fwd :: node.fwd

  let create :
      type args ret elt.
      tag:string ->
      (args -> (ret, elt) tensor) kernel ->
      (args, (ret, elt) tensor) node =
   fun ~tag kernel ->
    let (Diff payload) = kernel in
    let n =
      Node
        { id = fresh (); tag; fwd = []; value = None; accu = Zero_accu; kernel }
    in
    let opaque_n = Opaque (Applied n) in
    iter_vec (fun (Opaque bwd) -> add_fwd bwd opaque_n) payload.bwd ;
    n

  let get_kernel (Node { kernel; _ }) = kernel

  let get_tag (Node { tag; _ }) = tag

  let opaque_get_id (o : opaque) =
    let (Opaque an) = o in
    match an with Applied (Node node) -> node.id

  let opaque_get_fwd (o : opaque) =
    let (Opaque an) = o in
    match an with Applied (Node node) -> node.fwd

  let get_value_exn ~__LOC__ (Node node) = Helpers.opt_get ~__LOC__ node.value

  let set_value (Node node) v = node.value <- Some v

  let get_accu (Node node) = node.accu

  let set_accu (Node node) v = node.accu <- v

  let rec vec_get : type a. a node_vector -> a =
   fun vec ->
    match vec with
    | [] -> ()
    | Applied node :: tl ->
        let v = get_value_exn ~__LOC__ node in
        let r = vec_get tl in
        (v, r)

  let get_bwd : type a. a t -> opaque list =
   fun node ->
    match node with
    | Applied (Node node) -> (
        match node.kernel with
        | Diff { bwd; _ } ->
            fold_vec (fun opaque acc -> List.(opaque :: acc)) bwd []
            (*| Cond { cond; ift; iff } -> List.[Opaque cond; Opaque ift; Opaque iff]*)
        )

  let rec refresh : opaque -> unit Error_monad.t =
   fun (Opaque an) ->
    let open Error_monad in
    match an with
    | Applied node -> (
        match node with
        | Node { value = Some _; _ } -> return_unit
        | Node ({ value = None; kernel; _ } as payload) -> (
            match kernel with
            | Diff { bwd; forward; _ } ->
                let* () = iter_vec_e refresh bwd in
                let* nv = forward (vec_get bwd) in
                payload.value <- Some nv ;
                return_unit))

  let get_value : type a elt. (a, elt) tensor t -> (a, elt) tensor Error_monad.t
      =
   fun node ->
    let open Error_monad in
    let* () = refresh (Opaque node) in
    match node with Applied n -> return (get_value_exn ~__LOC__ n)

  let get_value_shape node =
    let open Error_monad in
    let+ v = get_value node in
    C.get_shape v

  let get_res_shape : type a elt. (a, elt) Core.tensor Core.t t -> a Shape.t =
   fun (Applied (Node node)) ->
    match node.kernel with
    | Diff { spec; _ } -> get_res_shape_from_internal_spec spec

  let diff ~tag spec elt_ty forward jacobian =
    create
      ~tag
      (Diff
         { spec = proj_spec spec;
           elt_ty;
           bwd = proj_backward spec;
           forward;
           jacobian
         })

  let applied node = Applied node
end

type 'a t = 'a Node.t

(* Jacobian helpers *)

type Error_monad.error +=
  | Generic_differentiation_error of string
  | Accumulator_shape_not_ground : string * _ Shape.t -> Error_monad.error
  | Output_shape_not_ground : _ Shape.t -> Error_monad.error

let () =
  Error_monad.register
    (function Generic_differentiation_error loc -> Some loc | _ -> None)
    (fun fmtr loc ->
      Format.fprintf fmtr "Generic_differentiation_error(%s)" loc)

let () =
  Error_monad.register
    (function
      | Accumulator_shape_not_ground (tag, shape) ->
          Some (Format.asprintf "%s:%a" tag Shape.pp shape)
      | _ -> None)
    (fun fmtr s -> Format.fprintf fmtr "Accumulator_shape_not_ground(%s)" s)

let () =
  Error_monad.register
    (function
      | Output_shape_not_ground sh -> Some (Format.asprintf "%a" Shape.pp sh)
      | _ -> None)
    (fun fmtr s -> Format.fprintf fmtr "Output_shape_not_ground(%s)" s)

let trace_generic ~loc k =
  Error_monad.trace (fun () -> Generic_differentiation_error loc) k

let trace_generic_msg ~msg k =
  Error_monad.trace (fun () -> Generic_differentiation_error (msg ())) k

(* Jacobian helpers *)

let identity : ('a, 'a) jacobian =
  { form = (fun ~acc -> Error_monad.return_some acc) }

let neg_identity = { form = (fun ~acc -> C.neg acc |> Error_monad.lift_option) }

let diagonal (type a) (v : (a, float) tensor) : (a, a) jacobian =
  let open Error_monad in
  { form =
      (fun (type output) ~(acc : (output, a) mat) ->
        let col_dim = Shape.create () in
        let row_dim = Shape.create () in
        let acc_shape = Shape.tensor row_dim col_dim in
        let* () = C.shape_join acc_shape (C.get_shape acc) in
        let* v = C.broadcast v acc_shape Shape.Lcont in
        C.mul acc v |> Error_monad.lift_option)
  }

(*
   get : 'a Core.vector Core.t Node.t -> 'a -> int Core.t Core.vector Core.t Node.t
   index : 'a
   -----

   f : Rn -> R
   f = (x1, ..., xn) |=> xi

   J(f) : Rn -> (Rn -o R)
   J(f) = _v -> (0,  ... 0, 1, 0, ... 0)

   delta index = (acc : R -o Rk) |=> acc . J(f)
   delta index : Rn -o Rk = zero matrix with a copy of [acc] at index [i]

   Q: should we not represent all matrices/jacobian in dual forms, not just jacobians?

*)

let delta_jacobian shape index : ('a, C.index) jacobian =
  let open Error_monad in
  { form =
      (fun (type x) ~(acc : (x, C.index) mat) ->
        let row_dim = Shape.create () in
        let acc_shape = Shape.tensor row_dim Shape.scalar in
        let* () = C.shape_join acc_shape (C.get_shape acc) in
        let* (acc' : x vec) = C.slice acc (Shape.Rset 0) in
        C.set_slice
          (C.zero (Shape.tensor row_dim shape))
          acc'
          (Shape.Rset index)
        |> Option.some |> Error_monad.return)
  }

let children_first_infix_traversal root f acc =
  let visited = Hashtbl.create 11 in
  let rec loop (seq : Node.opaque Seq.t) acc =
    match Seq.uncons seq with
    | None -> acc
    | Some ((Opaque node as o), rest) -> (
        let id = Node.opaque_get_id o in
        match Hashtbl.find_opt visited id with
        | None ->
            (* Node never visited, visit successors first *)
            Hashtbl.add visited id `gray ;
            loop (Seq.append (List.to_seq (Node.opaque_get_fwd o)) seq) acc
        | Some `gray ->
            (* Successors of node visited, visit node itself then ancestors *)
            Hashtbl.replace visited id `black ;
            let acc = f acc o in
            loop (Seq.append rest (List.to_seq (Node.get_bwd node))) acc
        | Some `black ->
            (* Ancestors and successors of node visited *)
            loop rest acc)
  in
  loop (Seq.return root) acc

let children_first_infix_iter_e root f =
  children_first_infix_traversal
    root
    (fun acc elt -> Result.bind acc (fun () -> f elt))
    (Ok ())

let array ba =
  Node.diff
    ~tag:"array"
    (Node.return (Shape.rank_one (Ba.length ba)))
    Float
    (fun () -> C.array ba |> Error_monad.return)
    (fun () -> Error_monad.return_unit)
  |> Node.applied

let scalar x =
  Node.diff
    ~tag:"scalar"
    (Node.return Shape.scalar)
    Float
    (fun () -> C.float Shape.scalar x |> Error_monad.return)
    (fun () -> Error_monad.return_unit)
  |> Node.applied

(* set_slice : operand:R^(jxk) -> update:R^k -> i:[0;j-1] -> R^(jxk)
   J(set_slice)(i) : R^(jxk) x R^k -> (R^(jxk) x R^k -o R^(jxk))
   J(set_slice)(i)(_) = (ja_operand, ja_update)
    ja_operand = same as acc with 0 on slice
    ja_update = same as acc on slice, 0 elsewhere *)
let set_slice (type a b) (operand : (a, float) tensor Node.t)
    (update : (b, float) tensor Node.t) (slice : (a, b) Shape.slice) =
  let open Error_monad in
  let operand_shape = Node.get_res_shape operand in
  let update_shape = Node.get_res_shape update in
  Node.diff
    ~tag:"set_slice"
    Node.(operand @-> update @-> return operand_shape)
    (Node.elt_type operand)
    (fun (operand, (update, ())) -> return (C.set_slice operand update slice))
    (fun (_operand, (_update, ())) ->
      let ja_operand =
        { form =
            (fun ~acc ->
              let acc_shape = C.get_shape acc in
              let output_shape = Shape.create () in
              let+ () =
                C.shape_join acc_shape (Shape.tensor output_shape operand_shape)
              in
              C.set_slice
                acc
                (C.zero (Shape.tensor output_shape update_shape))
                (Shape.R slice)
              |> Option.some)
        }
      in
      let ja_update =
        { form = (fun ~acc -> C.slice acc (Shape.R slice) |> lift_option) }
      in
      return (ja_operand, (ja_update, ())))
  |> Node.applied |> return

(* broadcast : R^j -> R^(jxk)
    broadcast(v)(j,_) = v(j) = k copies of v
    J(broadcast)(v) : R^j -o R^(jxk)
     - for k = 1, J = identity
     - for k > 1, J = k copies of the identity matrix
       => J(broadcast) copies its input k times
   acc x J(broadcast) x v = acc x (v, .., v) = contract(acc, L) x v
*)
let broadcast x shape path =
  Node.diff
    ~tag:"broadcast"
    Node.(x @-> return shape)
    (Node.elt_type x)
    (fun (x, ()) -> C.broadcast x shape path)
    (fun (_x, ()) ->
      Error_monad.return
        ( { form =
              (fun ~acc ->
                C.contract acc Shape.(R path) |> Error_monad.lift_option)
          },
          () ))
  |> Node.applied |> Error_monad.return

let zero shape =
  Node.diff
    ~tag:"zero"
    Node.(return shape)
    Float
    (fun () -> C.zero shape |> Error_monad.return)
    (fun () -> Error_monad.return_unit)
  |> Node.applied

let const shape f =
  Node.diff
    ~tag:"const"
    Node.(return shape)
    Float
    (fun () -> C.float shape f |> Error_monad.return)
    (fun () -> Error_monad.return_unit)
  |> Node.applied

let var shape =
  let v = C.var shape in
  let n =
    Node.diff
      ~tag:"var"
      Node.(return shape)
      Float
      (fun () -> Error_monad.return v)
      (fun _ -> Error_monad.return_unit)
  in
  Node.set_value n v ;
  Node.applied n

let create_binop ~tag x y ret_shape elt_ty forward jacobian =
  Node.diff
    ~tag
    Node.(x @-> y @-> return ret_shape)
    elt_ty
    (fun (x, (y, ())) -> forward x y)
    (fun (x, (y, ())) -> jacobian x y)
  |> Node.applied

let create_binop_uniform ~tag x y forward jacobian =
  let open Error_monad in
  let elt_ty = Node.elt_type x in
  let+ () = C.shape_join (Node.get_res_shape x) (Node.get_res_shape y) in
  Node.diff
    ~tag
    Node.(x @-> y @-> return (Node.get_res_shape x))
    elt_ty
    (fun (x, (y, ())) -> forward x y)
    (fun (x, (y, ())) -> jacobian x y)
  |> Node.applied

let add x y =
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  create_binop_uniform ~tag:"add" x y C.add (fun _x _y ->
      Error_monad.return (identity, (identity, ())))

let sub x y =
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  create_binop_uniform ~tag:"sub" x y C.sub (fun _x _y ->
      Error_monad.return (identity, (neg_identity, ())))

let mul x y =
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  create_binop_uniform ~tag:"mul" x y C.mul (fun x y ->
      Error_monad.return (diagonal y, (diagonal x, ())))

let div x y =
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  create_binop_uniform ~tag:"div" x y C.mul (fun x y ->
      let open Error_monad in
      let* ja_x =
        let* recip_x = C.recip x in
        return (diagonal recip_x)
      in
      let* ja_y =
        let* sq = C.mul y y in
        let* j = C.div x sq in
        return (diagonal j)
      in
      Error_monad.return (ja_x, (ja_y, ())))

let and_ x y =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  Node.diff
    ~tag:"and_"
    Node.(x @-> y @-> return (Node.get_res_shape x))
    Bool
    (fun (x, (y, ())) -> C.and_ x y)
    (fun (_x, (_y, ())) ->
      return
        ( { form = (fun ~acc:_ -> Error_monad.return_none) },
          ({ form = (fun ~acc:_ -> Error_monad.return_none) }, ()) ))
  |> Node.applied |> return

let or_ x y =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  Node.diff
    ~tag:"or_"
    Node.(x @-> y @-> return (Node.get_res_shape x))
    Bool
    (fun (x, (y, ())) -> C.and_ x y)
    (fun (_x, (_y, ())) ->
      return
        ( { form = (fun ~acc:_ -> Error_monad.return_none) },
          ({ form = (fun ~acc:_ -> Error_monad.return_none) }, ()) ))
  |> Node.applied |> return

let neg (x : 'a vec Node.t) =
  let open Error_monad in
  let sh = Node.get_res_shape x in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  Node.diff
    ~tag:"neg_"
    Node.(x @-> return sh)
    Float
    (fun (x, ()) -> C.neg x)
    (fun (_x, ()) -> Error_monad.return (neg_identity, ()))
  |> Node.applied |> return

let sqrt (type a) (x : a vec Node.t) =
  let open Error_monad in
  let sh = Node.get_res_shape x in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  Node.diff
    ~tag:"sqrt_"
    Node.(x @-> return sh)
    Float
    (fun (x, ()) -> C.sqrt x)
    (fun (x, ()) ->
      let* j =
        let* sqrt = C.sqrt x in
        let* grad = C.div (C.float sh 2.) sqrt in
        return (diagonal grad)
      in
      Error_monad.return (j, ()))
  |> Node.applied |> return

let recip (type a) (x : a vec Node.t) =
  let open Error_monad in
  let sh = Node.get_res_shape x in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  Node.diff
    ~tag:"sqrt_"
    Node.(x @-> return sh)
    Float
    (fun (x, ()) -> C.recip x)
    (fun (x, ()) ->
      let* j =
        let* sqr = C.mul x x in
        let* inv_sqrt = C.recip sqr in
        let* grad = C.neg inv_sqrt in
        return (diagonal grad)
      in
      Error_monad.return (j, ()))
  |> Node.applied |> return

let get x i =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  Node.diff
    ~tag:"get"
    Node.(x @-> return Shape.scalar)
    (Node.elt_type x)
    (fun (x, ()) -> C.get x i |> Error_monad.return)
    (fun (_x, ()) ->
      let+ shape = Node.get_value_shape x in
      (delta_jacobian shape i, ()))
  |> Node.applied |> return

let cond (type a elt) (cond : (a, bool) tensor Node.t)
    (ift : (a, elt) tensor Node.t) (iff : (a, elt) tensor Node.t) =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  let ift_shape = Node.get_res_shape ift in
  let iff_shape = Node.get_res_shape iff in
  let elt_ty = Node.elt_type ift in
  let+ () = C.shape_join ift_shape iff_shape in
  Node.diff
    ~tag:"cond_"
    Node.(cond @-> ift @-> iff @-> return ift_shape)
    elt_ty
    (fun (cond, (ift, (iff, ()))) -> C.cond cond ift iff)
    (fun (cond, (_ift, (_iff, ()))) ->
      let ja_cond = { form = (fun ~acc:_ -> Error_monad.return_none) } in
      let ja_ift =
        { form =
            (fun (type output) ~(acc : (output, a) mat) ->
              let acc_shape = C.get_shape acc in
              let* v = C.broadcast cond acc_shape Shape.Lcont in
              C.cond v acc (C.zero acc_shape) |> lift_option)
        }
      in
      let ja_iff =
        { form =
            (fun (type output) ~(acc : (output, a) mat) ->
              let acc_shape = C.get_shape acc in
              let* v = C.broadcast cond acc_shape Shape.Lcont in
              C.cond v (C.zero acc_shape) acc |> lift_option)
        }
      in
      Error_monad.return (ja_cond, (ja_ift, (ja_iff, ()))))
  |> Node.applied

let true_ shape =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  return
  @@ (Node.diff
        ~tag:"true_"
        Node.(return shape)
        Bool
        (fun () -> return (C.true_ shape))
        (fun () -> return ())
     |> Node.applied)

let false_ shape =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  return
  @@ (Node.diff
        ~tag:"false_"
        Node.(return shape)
        Bool
        (fun () -> return (C.false_ shape))
        (fun () -> return ())
     |> Node.applied)

(* Jacobian of matrix multiplication.

       f : X -> Y
   J (f) : X -> (X -o Y)

   res = mat1 x mat2

   mat1 : (mid, out)
   mat2 : (in, mid)
   res  : (in, out)

   J(mm) : R^(mid x out)xR^(in x mid) -> (R^(mid x out)xR^(in x mid) -o R^(in x out))

   res(c, r) = \sum_k mat1[k;r] mat2[c;k] = \sum_{k \in mid} col(mat1, k) . row(mat2, k)

   J1 ( (c', r'), (c, r) )  (c' : mid, r' : out, c : in, r : out)
      = d (res(c, r)) / d mat1[c';r']
      = \sum_k d(mat1[k;r] mat2[c;k]) / d mat1[c';r']
      = if r = r' then mat2[c;c'] else 0

   J2 ( (c', r'), (c, r) ) (c' : in, r' : mid, c : in, r : out)
      = d (res(c, r)) / d mat2[c';r']
      = \sum_k d(mat1[k;r] mat2[c;k]) / d mat2[c';r']
      = if c = c' then mat1[r';r] else 0

   J1 : ((mid, out), (in, out))
   J1 = fun (c', r') -> fun (row : (in, out)) -> row . J1(c', r', -, -)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_(c, r) row(c, r) . J1(c', r', c, r)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_(c, r) row(c, r) . (if r = r' then mat2[c;c'] else 0)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_(c, r) (if r = r' then row(c, r) . mat2[c;c'] else 0)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_c (row(c, r') . mat2[c;c'])
      = fun (c', r') -> fun (row : (in, out)) -> row(-, r') . mat2[-; c']

   J2 : ((in, mid),  (in, out))
      = fun (c', r') -> fun (row : (in, out)) -> row . J2(c', r', -, -)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_(c, r) row(c, r) . J2(c', r', c, r)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_(c, r) row(c, r) . (if c = c' then mat1[r';r] else 0)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_(c, r) (if c = c' then row(c, r) . mat1[r';r] else 0)
      = fun (c', r') -> fun (row : (in, out)) -> \sum_r row(c', r) . mat1[r';r]
      = fun (c', r') -> fun (row : (in, out)) -> row(c', -) . mat1[r'; -]
*)

let mm (type i m o) (x : (o, m) mat Node.t) (y : (m, i) mat Node.t) =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  let x_shape = Node.get_res_shape x in
  let y_shape = Node.get_res_shape y in
  let in_shape = Shape.create () in
  let mid_shape = Shape.create () in
  let out_shape = Shape.create () in
  let* () = C.shape_join x_shape (Shape.tensor out_shape mid_shape) in
  let* () = C.shape_join y_shape (Shape.tensor mid_shape in_shape) in
  return
  @@ create_binop
       ~tag:"mm"
       x
       y
       (Shape.tensor out_shape in_shape)
       Float
       C.mm
       (fun mat1 mat2 ->
         let j1 : (o * m, o * i) jacobian =
           { form =
               (fun (type output) ~(acc : (output, o * i) mat) ->
                 let* acc = C.rassoc acc in
                 let* mat2_tr = C.transpose mat2 in
                 let* matmul = C.(mm acc mat2_tr) in
                 C.lassoc matmul |> lift_option)
           }
         in
         let j2 : (m * i, o * i) jacobian =
           { form =
               (fun (type output) ~(acc : (output, o * i) mat) ->
                 let* acc = C.iso acc Shape.Iso.(rmap tr |> rassoc) in
                 let* matmul = C.(mm acc mat1) in
                 C.iso matmul Shape.Iso.(lassoc |> rmap tr) |> lift_option)
           }
         in
         Error_monad.return (j1, (j2, ())))

let dot (type a) (x : a vec Node.t) (y : a vec Node.t) =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  create_binop ~tag:"dot" x y Shape.scalar Float C.dot (fun x y ->
      return
        ( { form =
              (fun ~acc ->
                let acc_shape = C.get_shape acc in
                let out_shape = Shape.create () in
                let y_shape = C.get_shape y in
                let* () =
                  C.shape_join acc_shape (Shape.tensor out_shape Shape.scalar)
                in
                let* v =
                  C.broadcast y (Shape.tensor Shape.scalar y_shape) Shape.Lcont
                in
                C.mm acc v |> lift_option)
          },
          ( { form =
                (fun ~acc ->
                  let acc_shape = C.get_shape acc in
                  let out_shape = Shape.create () in
                  let x_shape = C.get_shape x in
                  let* () =
                    C.shape_join acc_shape (Shape.tensor out_shape Shape.scalar)
                  in
                  let* v =
                    C.broadcast
                      x
                      (Shape.tensor Shape.scalar x_shape)
                      Shape.Lcont
                  in
                  C.mm acc v |> lift_option)
            },
            () ) ))
  |> return

let slice (type a b elt) (x : (a, elt) tensor Node.t)
    (slice : (a, b) Shape.slice) =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  let x_shape = Node.get_res_shape x in
  let* result_shape = Shape.of_slice x_shape slice |> Shape_error.lift in
  Node.diff
    ~tag:"slice"
    Node.(x @-> return result_shape)
    (Node.elt_type x)
    (fun (x, ()) -> C.slice x slice)
    (fun (_x, ()) ->
      return
        ( { form =
              (fun (type out) ~(acc : (out, b) mat) ->
                let out_shape = Shape.create () in
                let* () =
                  C.shape_join
                    (C.get_shape acc)
                    (Shape.tensor out_shape result_shape)
                in
                let zero_shape = Shape.tensor out_shape x_shape in
                let zero = C.zero zero_shape in
                C.set_slice zero acc (Shape.R slice) |> Option.some |> return)
          },
          () ))
  |> Node.applied |> return

let contract (type a b) (x : (a, float) tensor Node.t)
    (path : (a, b) Shape.path) =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  let x_shape = Node.get_res_shape x in
  let* result_shape = Shape.contract x_shape path |> Shape_error.lift in
  Node.diff
    ~tag:"contract"
    Node.(x @-> return result_shape)
    (Node.elt_type x)
    (fun (x, ()) -> C.contract x path)
    (fun (_x, ()) ->
      return
        ( { form =
              (fun (type output) ~(acc : (output, b) mat) ->
                let output_shape = Shape.create () in
                let* () =
                  C.shape_join
                    (Shape.tensor output_shape result_shape)
                    (C.get_shape acc)
                in
                C.broadcast
                  acc
                  (Shape.tensor output_shape x_shape)
                  (Shape.R path)
                |> lift_option)
          },
          () ))
  |> Node.applied |> return

let diagonal (type base diag) (x : (base, float) tensor Node.t)
    (diag : (base, diag) Shape.diagonal) =
  let open Error_monad in
  trace_generic ~loc:__FUNCTION__ @@ fun () ->
  let (_base, shape) = Shape.shape_of_diag diag in
  Node.diff
    ~tag:"diagonal"
    Node.(x @-> return shape)
    (Node.elt_type x)
    (fun (x, ()) -> C.diag x diag |> return)
    (fun (x, ()) ->
      return
        ( { form =
              (fun (type output) ~(acc : (output, diag) mat) ->
                let* diag_t = C.transpose (C.diag x (Shape.Diag_cons diag)) in
                C.mm acc diag_t |> lift_option)
          },
          () ))
  |> Node.applied |> return

let rec iso :
    type a b elt.
    (a, elt) tensor Node.t ->
    (a, b) Shape.iso ->
    (b, elt) tensor Node.t Error_monad.t =
 fun x i ->
  let open Error_monad in
  match i with
  | Shape.LAssoc ->
      let a = Shape.create () in
      let b = Shape.create () in
      let c = Shape.create () in
      let x_shape = Node.get_res_shape x in
      let* () = C.shape_join (Shape.tensor (Shape.tensor a b) c) x_shape in
      let result_shape = Shape.tensor a (Shape.tensor b c) in
      Node.diff
        ~tag:"lassoc"
        Node.(x @-> return result_shape)
        (Node.elt_type x)
        (fun (x, ()) -> C.lassoc x)
        (fun (_x, ()) ->
          return
            ( { form =
                  (fun (type out) ~(acc : (out, _) mat) ->
                    C.iso acc Shape.Iso.(rmap rassoc) |> lift_option)
              },
              () ))
      |> Node.applied |> return
  | RAssoc ->
      let a = Shape.create () in
      let b = Shape.create () in
      let c = Shape.create () in
      let x_shape = Node.get_res_shape x in
      let* () = C.shape_join (Shape.tensor a (Shape.tensor b c)) x_shape in
      let result_shape = Shape.tensor (Shape.tensor a b) c in
      Node.diff
        ~tag:"rassoc"
        Node.(x @-> return result_shape)
        (Node.elt_type x)
        (fun (x, ()) -> C.rassoc x)
        (fun (_x, ()) ->
          return
            ( { form =
                  (fun (type out) ~(acc : (out, _) mat) ->
                    C.iso acc Shape.Iso.(rmap lassoc) |> lift_option)
              },
              () ))
      |> Node.applied |> return
  | Transpose ->
      let a = Shape.create () in
      let b = Shape.create () in
      let x_shape = Node.get_res_shape x in
      let* () = C.shape_join (Shape.tensor a b) x_shape in
      let result_shape = Shape.tensor b a in
      Node.diff
        ~tag:"transpose"
        Node.(x @-> return result_shape)
        (Node.elt_type x)
        (fun (x, ()) -> C.transpose x)
        (fun (_x, ()) ->
          return
            ( { form =
                  (fun (type out) ~(acc : (out, _) mat) ->
                    C.iso acc Shape.Iso.(rmap tr) |> lift_option)
              },
              () ))
      |> Node.applied |> return
  | LMap l ->
      let a = Shape.create () in
      let b = Shape.create () in
      let x_shape = Node.get_res_shape x in
      let* () = C.shape_join (Shape.tensor a b) x_shape in
      let* left_result_shape = Shape.apply_iso a l |> Shape_error.lift in
      let result_shape = Shape.tensor left_result_shape b in
      Node.diff
        ~tag:"lmap"
        Node.(x @-> return result_shape)
        (Node.elt_type x)
        (fun (x, ()) -> C.lmap x l)
        (fun (_x, ()) ->
          return
            ( ({ form =
                   (fun (type out) ~(acc : (out, b) mat) ->
                     C.iso acc Shape.Iso.(rmap (Shape.inverse i)) |> lift_option)
               }
                : (a, b) jacobian),
              () ))
      |> Node.applied |> return
  | RMap r ->
      let a = Shape.create () in
      let b = Shape.create () in
      let x_shape = Node.get_res_shape x in
      let* () = C.shape_join (Shape.tensor a b) x_shape in
      let* right_result_shape = Shape.apply_iso b r |> Shape_error.lift in
      let result_shape = Shape.tensor a right_result_shape in
      Node.diff
        ~tag:"rmap"
        Node.(x @-> return result_shape)
        (Node.elt_type x)
        (fun (x, ()) -> C.rmap x r)
        (fun (_x, ()) ->
          return
            ( ({ form =
                   (fun (type out) ~(acc : (out, b) mat) ->
                     C.iso acc Shape.Iso.(rmap (Shape.inverse i)) |> lift_option)
               }
                : (a, b) jacobian),
              () ))
      |> Node.applied |> return
  | Circ (i1, i2) ->
      let* y = iso x i1 in
      (iso y i2 : (b, elt) tensor Node.t Error_monad.t)

let get_accu_or_zero :
    type a output elt.
    (_, (a, elt) tensor) Node.node ->
    (output * a) Shape.t ->
    (output * a, float) tensor Error_monad.t =
 fun node expected_shape ->
  let open Error_monad in
  match Node.get_accu node with
  | Zero_accu -> return (C.zero expected_shape)
  | Accu acc ->
      let acc_shape = C.get_shape acc in
      let+ Type.Equal =
        C.assert_shape_equal ~loc:__LOC__ expected_shape acc_shape
      in
      (acc : (output * a, float) tensor)

let differentiate (type input output) ?shape
    (f : input vec Node.t -> output vec Node.t Error_monad.t) =
  let open Error_monad in
  let input_shape = match shape with None -> Shape.create () | Some s -> s in
  let var = var input_shape in
  trace_generic ~loc:__LOC__ @@ fun () ->
  let* body = f var in
  let* output_shape = Node.get_value_shape body in
  let* () =
    if not (Shape.is_ground output_shape) then
      fail (Output_shape_not_ground output_shape)
    else return_unit
  in
  let accumulate :
      type b elt.
      (b, elt) tensor Node.t -> (output, b) mat -> unit Error_monad.t =
   fun node vec ->
    let* node_shape = Node.get_value_shape node in
    let (Applied node) = node in
    match Node.get_accu node with
    | Zero_accu ->
        Node.set_accu node (Accu vec) ;
        return_unit
    | Accu accu ->
        let expected = Shape.tensor output_shape node_shape in
        let actual = C.get_shape accu in
        let* () =
          if not (Shape.is_ground actual) then
            fail (Accumulator_shape_not_ground (Node.get_tag node, actual))
          else return_unit
        in
        let* Type.Equal = C.assert_shape_equal ~loc:__LOC__ actual expected in
        let* next_accu = C.add accu vec in
        Node.set_accu node (Accu next_accu) ;
        return_unit
  in
  let (Applied b) = body in
  Node.set_accu b (Accu (C.identity output_shape)) ;
  let* () =
    children_first_infix_iter_e
      (Opaque body)
      (fun (Opaque (Applied (Node _) as an)) ->
        (* This eta-expansion is there just to name a type parameter. *)
        (fun (type a elt) (an : (a, elt) tensor Node.t) ->
          let (Applied node) = an in
          match Node.get_kernel node with
          | Diff { spec; bwd; jacobian; _ } ->
              (* for (node_i, ja_i) in ancestors of node
                 node_i.accu += mm(node.accu, ja_i) *)
              let node_shape = get_res_shape_from_internal_spec spec in
              let expected_shape = Shape.tensor output_shape node_shape in
              let* node_accu = get_accu_or_zero node expected_shape in
              let args = Node.vec_get bwd in
              let* js = jacobian args in
              let rec loop :
                  type args ja_res res.
                  (args, ja_res, res) internal_spec ->
                  args Node.node_vector ->
                  ja_res ->
                  (a, res) Type.eq ->
                  unit Error_monad.t =
               fun spec vec ja eq ->
                match (eq, spec, vec, ja) with
                | (_, Returning _, [], ()) -> return_unit
                | ( Type.Equal,
                    Arg (_elt_ty, spec_tl),
                    back :: node_tl,
                    (ja, ja_tl) ) ->
                    trace_generic_msg ~msg:(fun () ->
                        Format.asprintf
                          "While differentiating %s"
                          (Node.get_tag node))
                    @@ fun () ->
                    let* delta_opt = ja.form ~acc:node_accu in
                    let* () =
                      match delta_opt with
                      | None -> return_unit
                      | Some delta -> accumulate back delta
                    in
                    loop spec_tl node_tl ja_tl Type.Equal
              in
              loop spec bwd js Type.Equal)
          an)
  in
  let (Applied var_node) = var in
  get_accu_or_zero var_node (Shape.tensor output_shape input_shape)

let binop (type a ea b eb c ec) (op : (a, ea, b, eb, c, ec) C.binop) :
    (a, ea) tensor Node.t ->
    (b, eb) tensor Node.t ->
    (c, ec) tensor Node.t Error_monad.t =
  match op with
  | C.Add -> add
  | C.Sub -> sub
  | C.Mul -> mul
  | C.Div -> div
  | C.Matmul -> mm
  | C.Dot -> fun x y -> dot x y
  | C.And -> and_
  | C.Or -> or_

let rec reflect :
    type a elt. (a, elt) tensor -> (a, elt) tensor Node.t Error_monad.t =
 fun e ->
  let open Error_monad in
  match e with
  | C.S { desc; shape; tag = _; hash = _ } -> (
      match desc with
      | C.Var -> var shape |> return
      | C.Float f -> const shape f |> return
      | C.Array ba -> array ba |> return
      | C.True -> true_ shape
      | C.False -> false_ shape
      | C.Binop (op, l, r) ->
          let* l = reflect l in
          let* r = reflect r in
          binop op l r
      | C.Unop (op, e) -> (
          match op with
          | Neg ->
              let* e = reflect e in
              neg e
          | Sqrt ->
              let* e = reflect e in
              sqrt e
          | Recip ->
              let* e = reflect e in
              recip e)
      | C.Get (e, i) ->
          let* e = reflect e in
          get e i
      | C.Cond (c, ift, iff) ->
          let* c = reflect c in
          let* ift = reflect ift in
          let* iff = reflect iff in
          cond c ift iff
      | C.Set_slice { operand; update; slice } ->
          let* operand = reflect operand in
          let* update = reflect update in
          set_slice operand update slice
      | C.Contract (e, path) ->
          let* e = reflect e in
          contract e path
      | C.Broadcast (e, path) ->
          let* e = reflect e in
          broadcast e shape path
      | Slice (e, sl) ->
          let* e = reflect e in
          slice e sl
      | Iso (e, i) ->
          let* e = reflect e in
          iso e i
      | Diagonal (e, diag) ->
          let* e = reflect e in
          diagonal e diag)

module WithExn : sig
  val ( + ) : 'a vec t -> 'a vec t -> 'a vec t

  val ( - ) : 'a vec t -> 'a vec t -> 'a vec t

  val ( * ) : 'a vec t -> 'a vec t -> 'a vec t

  val ( / ) : 'a vec t -> 'a vec t -> 'a vec t

  val array :
    (float, Bigarray.float64_elt, Bigarray.c_layout) Bigarray.Array1.t ->
    (int, float) Core.tensor Core.t t

  val zero : 'a Shape.t -> 'a vec t

  val scalar : float -> index vec t

  val and_ : 'a boolean t -> 'a boolean t -> 'a boolean t

  val or_ : 'a boolean t -> 'a boolean t -> 'a boolean t

  val true_ : 'a Shape.t -> 'a boolean t

  val false_ : 'a Shape.t -> 'a boolean t

  val ( ~- ) : 'a vec t -> 'a vec t

  val sqrt : 'a vec t -> 'a vec t

  val recip : 'a vec t -> 'a vec t

  val get : ('a, 'b) tensor t -> 'a -> (index, 'b) tensor t

  val cond :
    'a boolean t ->
    ('a, 'elt) tensor t ->
    ('a, 'elt) tensor t ->
    ('a, 'elt) tensor t

  val mm : ('o, 'm) mat t -> ('m, 'i) mat t -> ('o * 'i, float) tensor t

  val dot : 'a vec t -> 'a vec t -> index vec t

  val slice : ('a, 'elt) tensor t -> ('a, 'b) Shape.slice -> ('b, 'elt) tensor t

  val contract : 'a vec t -> ('a, 'b) Shape.path -> 'b vec t

  val diagonal : 'base vec t -> ('base, 'diag) Shape.diagonal -> 'diag vec t

  val iso : ('a, 'elt) tensor t -> ('a, 'b) Shape.iso -> ('b, 'elt) tensor t
end = struct
  let array = array

  let zero = zero

  let scalar = scalar

  let ( + ) x y = add x y |> Error_monad.run

  let ( - ) x y = sub x y |> Error_monad.run

  let ( * ) x y = mul x y |> Error_monad.run

  let ( / ) x y = div x y |> Error_monad.run

  let and_ x y = and_ x y |> Error_monad.run

  let or_ x y = or_ x y |> Error_monad.run

  let true_ shape = true_ shape |> Error_monad.run

  let false_ shape = false_ shape |> Error_monad.run

  let ( ~- ) x = neg x |> Error_monad.run

  let sqrt x = sqrt x |> Error_monad.run

  let recip x = recip x |> Error_monad.run

  let get x i = get x i |> Error_monad.run

  let cond c ift iff = cond c ift iff |> Error_monad.run

  let mm x y = mm x y |> Error_monad.run

  let dot x y = dot x y |> Error_monad.run

  let slice x sl = slice x sl |> Error_monad.run

  let contract x path = contract x path |> Error_monad.run

  let diagonal x diag = diagonal x diag |> Error_monad.run

  let iso x i = iso x i |> Error_monad.run
end
