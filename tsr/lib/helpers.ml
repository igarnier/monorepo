let opt_get ~__LOC__ = function
  | None -> Format.kasprintf failwith "opt_get: %s" __LOC__
  | Some x -> x

module Option_syntax = struct
  let ( let* ) = Option.bind

  let ( let+ ) = Option.map

  let return = Option.some
end

let pp_shape fmtr shape =
  Format.fprintf
    fmtr
    "[| %a |]"
    (Format.pp_print_array
       ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ";")
       Format.pp_print_int)
    shape

(** [pp_genarray pp_elt genarray] pretty-prints n-dimensional arrays in numpy style. *)
let pp_genarray :
    type elt kind.
    (Format.formatter -> elt -> unit) ->
    Format.formatter ->
    (elt, kind, Bigarray.c_layout) Bigarray.Genarray.t ->
    unit =
 fun pp_elt fmt arr ->
  let ndim = Bigarray.Genarray.num_dims arr in
  let dims = Bigarray.Genarray.dims arr in
  let rec pp_loop : type n. int -> int array -> bool -> unit =
   fun i idx last ->
    if i = ndim then
      Format.fprintf fmt "%a" pp_elt (Bigarray.Genarray.get arr idx)
    else (
      Format.open_vbox 0 ;
      Format.fprintf fmt "[" ;
      for j = 0 to dims.(i) - 1 do
        pp_loop (i + 1) (Array.append idx [| j |]) (j = dims.(i) - 1)
      done ;
      Format.fprintf fmt "]" ;
      if not last then Format.fprintf fmt "@," ;
      Format.close_box ())
  in
  pp_loop 0 [||] (0 = dims.(0) - 1)

let pp_float_genarray genarray = pp_genarray Format.pp_print_float genarray

let pp_bool_genarray genarray =
  pp_genarray
    (fun fmtr c ->
      if Char.code c = 0 then Format.fprintf fmtr "f"
      else Format.fprintf fmtr "t")
    genarray

module Sexp = struct
  type t = S of float | A of t array

  let scalar x = A [| S x |]

  let array a = A (Array.map (fun x -> S x) a)

  let iter_sexp sexp f =
    let rec iter_sexp : t -> int list -> (int list -> float -> unit) -> unit =
     fun sexp acc f ->
      match sexp with
      | S x -> f (List.rev acc) x
      | A sexps -> Array.iteri (fun i sexp -> iter_sexp sexp (i :: acc) f) sexps
    in
    iter_sexp sexp [] f

  let to_bigarray (sexp : t) =
    let rec rank : t -> int list = function
      | S _ -> []
      | A sexps ->
          if Array.length sexps = 0 then
            invalid_arg "bigarray_of_sexp: empty array"
          else Array.length sexps :: rank (Array.get sexps 0)
    in
    let rec check_rank : t -> int list -> bool =
     fun sexp rank ->
      match (sexp, rank) with
      | (S _, []) -> true
      | (A sexps, n :: rank) ->
          Array.length sexps = n
          && Array.for_all (fun sexp -> check_rank sexp rank) sexps
      | (A _, _) -> false
      | (S _, _) -> false
    in
    let rank = rank sexp in
    if not (check_rank sexp rank) then
      invalid_arg "bigarray_of_sexp: array shape mismatch" ;
    let dims = Array.of_list rank in
    let array =
      Bigarray.Genarray.create Bigarray.float64 Bigarray.c_layout dims
    in
    iter_sexp sexp (fun acc x ->
        let idx = Array.of_list acc in
        Bigarray.Genarray.set array idx x) ;
    array

  let%expect_test "test_iter_sexp" =
    let rank_1a = A [| S 0.0; S 1.0; S 2.0; S 3.0 |] in
    let rank_1b = A [| S 4.0; S 5.0; S 6.0; S 7.0 |] in
    let rank_2 = A [| rank_1a; rank_1b |] in
    iter_sexp rank_2 (fun acc x ->
        Format.printf
          "%a |-> %f@."
          (Format.pp_print_list
             ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",")
             Format.pp_print_int)
          acc
          x) ;
    [%expect
      {|
    0,0 |-> 0.000000
    0,1 |-> 1.000000
    0,2 |-> 2.000000
    0,3 |-> 3.000000
    1,0 |-> 4.000000
    1,1 |-> 5.000000
    1,2 |-> 6.000000
    1,3 |-> 7.000000 |}]
end
