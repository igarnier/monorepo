open Tsr_shape

type ('shape, 'elt) tensor = private Tensor_tag

type index = int

type 'elt elt_kind = Float : float elt_kind | Bool : bool elt_kind

(** The type of operation descriptors, exported for internal use only. *)
type _ desc = private
  (** [Var] corresponds to the input of the computation. *)
  | Var : ('a, float) tensor desc
  (** [Float f] is a constant tensor equal everywhere to [f]. *)
  | Float : float -> ('a, float) tensor desc
  (** [Array ba] is a tensor of rank one containing the array [ba]. *)
  | Array : Ba.t -> (index, float) tensor desc
  (** [True] is the constant boolean tensor everywhere equal to [true]. *)
  | True : ('a, bool) tensor desc
  (** [False] is the constant boolean tensor everywhere equal to [false]. *)
  | False : ('a, bool) tensor desc
  (** [Binop (op, l, r)] is the application of the binary operator [op] to the operands [l] and [r]. *)
  | Binop :
      ('a, 'ea, 'b, 'eb, 'c, 'ec) binop
      * ('a, 'ea) tensor t
      * ('b, 'eb) tensor t
      -> ('c, 'ec) tensor desc
  (** [Unop (op, x)] is the application of the unary operator [op] to the operand [t]. *)
  | Unop : ('a, 'ea, 'b, 'eb) unop * ('a, 'ea) tensor t -> ('b, 'eb) tensor desc
  (** [Get (t, i)] gets the element of the tensor [t] at the index [i] as a rank-1 tensor of length 1. *)
  | Get : ('a, 'elt) tensor t * 'a -> (index, 'elt) tensor desc
  (** [Cond (c, ift, iff)] is the pointwise conditional operator, with true branch [ift] and false branch [iff]. *)
  | Cond :
      ('a, bool) tensor t * ('a, 'elt) tensor t * ('a, 'elt) tensor t
      -> ('a, 'elt) tensor desc
  (** [Set_slice { operand; update; slice }] is the tensor equal to [operand] except on [slice] where it is equal to [update]. *)
  | Set_slice :
      { operand : ('a, float) tensor t;
        update : ('b, float) tensor t;
        slice : ('a, 'b) Shape.slice
      }
      -> ('a, float) tensor desc
  (** [Contract (t, path)] reduces the tensor [t] along the path [p]. *)
  | Contract :
      ('a, float) tensor t * ('a, 'b) Shape.path
      -> ('b, float) tensor desc
  (** [Broadcast (t, path)] is a tensor equal to [t] on all slices of the extra dimensions specified by [path]. *)
  | Broadcast :
      ('a, 'elt) tensor t * ('b, 'a) Shape.path
      -> ('b, 'elt) tensor desc
  (** [Slice (t, slice)] is the sub-tensor of [t] at [slice]. *)
  | Slice : ('a, 'elt) tensor t * ('a, 'b) Shape.slice -> ('b, 'elt) tensor desc
  (** [Iso (t, i)] applies a structural isomorphism [i] to the tensor [t]. *)
  | Iso : ('a, 'elt) tensor t * ('a, 'b) Shape.iso -> ('b, 'elt) tensor desc
  (** [Diagonal (t, diag)] is the tensor with [t] on its diagonal and [0] everywhere else.
      The rank of the tensor is specified by [diag]. *)
  | Diagonal :
      ('base, float) tensor t * ('base, 'a) Shape.diagonal
      -> ('a, float) tensor desc

and (_, _, _, _, _, _) binop =
  | Add : ('a, float, 'a, float, 'a, float) binop
  | Sub : ('a, float, 'a, float, 'a, float) binop
  | Mul : ('a, float, 'a, float, 'a, float) binop
  | Div : ('a, float, 'a, float, 'a, float) binop
  | Matmul : ('a * 'b, float, 'b * 'c, float, 'a * 'c, float) binop
  | Dot : ('a, float, 'a, float, index, float) binop
  | And : ('a, bool, 'a, bool, 'a, bool) binop
  | Or : ('a, bool, 'a, bool, 'a, bool) binop

and (_, _, _, _) unop =
  | Neg : ('a, float, 'a, float) unop
  | Sqrt : ('a, float, 'a, float) unop
  | Recip : ('a, float, 'a, float) unop

and _ t =
  | S :
      { hash : int;
        tag : ('a, 'elt) tensor Type.Id.t;
        desc : ('a, 'elt) tensor desc;
        shape : 'a Shape.t
      }
      -> ('a, 'elt) tensor t

type 'a vec = ('a, float) tensor t

type 'a boolean = ('a, bool) tensor t

val get_hash : 'a t -> int

val get_tag : ('a, 'elt) tensor t -> ('a, 'elt) tensor Type.Id.t

val get_shape : ('a, 'b) tensor t -> 'a Shape.t

val get_var : 'a t -> 'a desc

val pp : Format.formatter -> 'a t -> unit

val pp_desc : Format.formatter -> 'a Shape.t -> ('a, 'elt) tensor desc -> unit

val pp_binop : Format.formatter -> ('a, 'ea, 'b, 'eb, 'c, 'ec) binop -> unit

val pp_unop : Format.formatter -> ('a, 'ea, 'b, 'eb) unop -> unit

val binop_elt_type : ('a, 'ea, 'b, 'eb, 'c, 'ec) binop -> 'ec elt_kind

val equal_elt_kind : 'a elt_kind -> 'b elt_kind -> ('a, 'b) Type.eq option

val var : 'a Shape.t -> 'a vec

val float : 'a Shape.t -> float -> 'a vec

val zero : 'a Shape.t -> 'a vec

val one : 'a Shape.t -> 'a vec

val array : Ba.t -> (index, float) tensor t

val true_ : 'a Shape.t -> 'a boolean

val false_ : 'a Shape.t -> 'a boolean

val shape_join : 'a Shape.t -> 'a Shape.t -> unit Error_monad.t

val assert_shape_equal : ?loc:string -> 'a Shape.t -> 'b Shape.t -> ('a, 'b) Type.eq Error_monad.t

val add :
  'a vec ->
  'a vec ->
  'a vec Error_monad.t

val sub :
  'a vec ->
  'a vec ->
  'a vec Error_monad.t

val mul :
  'a vec ->
  'a vec ->
  'a vec Error_monad.t

val div :
  'a vec ->
  'a vec ->
  'a vec Error_monad.t

val mm :
  ('a * 'b) vec ->
  ('b * 'c) vec ->
  ('a * 'c) vec Error_monad.t

val dot :
  'a vec ->
  'a vec ->
  index vec Error_monad.t

val and_ :
  'a boolean ->
  'a boolean ->
  'a boolean Error_monad.t

val or_ :
  'a boolean ->
  'a boolean ->
  'a boolean Error_monad.t

val unop :
  ('a, 'ea, 'b, 'eb) unop ->
  ('a, 'ea) tensor t ->
  ('b, 'eb) tensor t Error_monad.t

val neg : 'a vec -> 'a vec Error_monad.t

val sqrt : 'a vec -> 'a vec Error_monad.t

val recip : 'a vec -> 'a vec Error_monad.t

val get : ('a, 'elt) tensor t -> 'a -> (index, 'elt) tensor t

val cond :
  'a boolean ->
  ('a, 'b) tensor t ->
  ('a, 'b) tensor t ->
  ('a, 'b) tensor t Error_monad.t

val set_slice :
  'a vec ->
  'b vec ->
  ('a, 'b) Shape.slice ->
  'a vec

val contract :
  'a vec ->
  ('a, 'b) Shape.path ->
  'b vec Error_monad.t

val broadcast :
  ('a, 'elt) tensor t ->
  'c Shape.t ->
  ('c, 'a) Shape.path ->
  ('c, 'elt) tensor t Error_monad.t

val slice :
  ('a, 'elt) tensor t ->
  ('a, 'b) Shape.slice ->
  ('b, 'elt) tensor t Error_monad.t

val iso :
  ('a, 'elt) tensor t -> ('a, 'b) Shape.iso -> ('b, 'elt) tensor t Error_monad.t

val lassoc :
  (('a * 'b) * 'c, 'elt) tensor t ->
  ('a * ('b * 'c), 'elt) tensor t Error_monad.t

val rassoc :
  ('a * ('b * 'c), 'elt) tensor t ->
  (('a * 'b) * 'c, 'elt) tensor t Error_monad.t

val transpose :
  ('a * 'b, 'elt) tensor t -> ('b * 'a, 'elt) tensor t Error_monad.t

val lmap :
  ('a * 'b, 'elt) tensor t ->
  ('a, 'c) Shape.iso ->
  ('c * 'b, 'elt) tensor t Error_monad.t

val rmap :
  ('a * 'b, 'elt) tensor t ->
  ('b, 'c) Shape.iso ->
  ('a * 'c, 'elt) tensor t Error_monad.t

val diag :
  'base vec -> ('base, 'a) Shape.diagonal -> 'a vec

val identity : 'a Shape.t -> ('a * 'a) vec

val subst_var : 'v vec -> 'a t -> 'a t Error_monad.t

module Dot : sig
  type graph =
    { nodes : (int, string) Hashtbl.t;
      edges : (int, (int * string) list) Hashtbl.t
    }

  val to_graph : ('a, 'elt) tensor t -> graph

  val to_dot : out_channel -> graph -> unit
end
