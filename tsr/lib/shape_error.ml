open Tsr_shape

type Error_monad.error +=
  | Shape_error of { loc : string option; err : Shape.error }

let () =
  Error_monad.register
    (function Shape_error { loc; err } -> Some (loc, err) | _ -> None)
    (fun fmtr (loc, err) ->
      Format.fprintf
        fmtr
        "Shape error at %a: %a"
        (Format.pp_print_option
           ~none:(fun fmtr () -> Format.pp_print_string fmtr "<unknown>")
           Format.pp_print_string)
        loc
        Shape.pp_error
        err)

let lift ?loc res =
  Error_monad.map_error (fun err -> [Shape_error { loc; err }]) res
