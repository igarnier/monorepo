open Core_diff.WithExn
open Tsr_shape
module Fmt = Format

let f x = scalar 10.0 + (x * x)

let%expect_test "diff_sq" =
  let grad = Error_monad.run @@ Core_diff.differentiate (fun x -> Ok (f x)) in
  Format.printf "result: %a@." Core.pp grad ;
  [%expect
    {| result: ((diag(1.000000, 1^2) * broadcast(var, lcont, [ 1 1 ])) + (diag(1.000000, 1^2) * broadcast(var, lcont, [ 1 1 ]))) |}]

let rosenbrock vec =
  let x = get vec 0 in
  let y = get vec 1 in
  let term1 = scalar 1. - x in
  let term2 = y - (x * x) in
  (term1 * term1) + (scalar 100. * term2 * term2)

let rosenbrock_grad pos =
  let x = pos.(0) in
  let y = pos.(1) in
  [| 2. *. ((200. *. (x ** 3.)) -. (200. *. x *. y) +. x -. 1.);
     200. *. (y -. (x ** 2.))
  |]

let%expect_test "diff_rosenbrock" =
  let grad =
    Core_diff.differentiate ~shape:(Shape.rank_one 2) (fun x ->
        Ok (rosenbrock x))
    |> Error_monad.run
  in
  Format.printf "result shape: %a@." Shape.pp (Core.get_shape grad) ;
  let resx = Core.get grad (0, 0) in
  let resy = Core.get grad (0, 1) in
  Format.printf "dx= %a@." Core.pp resx ;
  Format.printf "dy= %a@." Core.pp resx ;
  let resx = Flat.flatten resx |> Error_monad.run in
  let resy = Flat.flatten resy |> Error_monad.run in
  let inputs =
    let args = [0.; 5.; 10.] |> List.to_seq in
    Seq.product args args |> List.of_seq
  in
  List.iter
    (fun (x, y) ->
      let arr = [| x; y |] in
      let vec = Ba.of_array arr |> Interp.tensor_of_bigarray in
      let (Float dx) =
        Interp.interp resx vec |> Error_monad.run |> Interp.rematerialize
      in
      let (Float dy) =
        Interp.interp resy vec |> Error_monad.run |> Interp.rematerialize
      in
      Fmt.printf
        "ours:  %f %f |=> @[<v>%a%a@]@."
        x
        y
        Helpers.pp_float_genarray
        dx
        Helpers.pp_float_genarray
        dy ;
      let real_grad = rosenbrock_grad arr in
      Fmt.printf "truth: %f %f |=> %f %f@." x y real_grad.(0) real_grad.(1))
    inputs;
  [%expect {|
    result shape: [ 1 2 ]
    dx= ((set_slice(0.000000, slice(((diag(1.000000, 1^2) * broadcast((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))), lcont, [ 1 1 ])) + ((diag(1.000000, 1^2) * broadcast(((var)[1] - ((var)[0] * (var)[0])), lcont, [ 1 1 ])) * broadcast(100.000000, lcont, [ 1 1 ]))), rset(0)), rset(1)) + set_slice(0.000000, slice(((neg(((diag(1.000000, 1^2) * broadcast((1.000000 - (var)[0]), lcont, [ 1 1 ])) + (diag(1.000000, 1^2) * broadcast((1.000000 - (var)[0]), lcont, [ 1 1 ])))) + (neg(((diag(1.000000, 1^2) * broadcast((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))), lcont, [ 1 1 ])) + ((diag(1.000000, 1^2) * broadcast(((var)[1] - ((var)[0] * (var)[0])), lcont, [ 1 1 ])) * broadcast(100.000000, lcont, [ 1 1 ])))) * broadcast((var)[0], lcont, [ 1 1 ]))) + (neg(((diag(1.000000, 1^2) * broadcast((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))), lcont, [ 1 1 ])) + ((diag(1.000000, 1^2) * broadcast(((var)[1] - ((var)[0] * (var)[0])), lcont, [ 1 1 ])) * broadcast(100.000000, lcont, [ 1 1 ])))) * broadcast((var)[0], lcont, [ 1 1 ]))), rset(0)), rset(0))))[[ 0 0 ]]
    dy= ((set_slice(0.000000, slice(((diag(1.000000, 1^2) * broadcast((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))), lcont, [ 1 1 ])) + ((diag(1.000000, 1^2) * broadcast(((var)[1] - ((var)[0] * (var)[0])), lcont, [ 1 1 ])) * broadcast(100.000000, lcont, [ 1 1 ]))), rset(0)), rset(1)) + set_slice(0.000000, slice(((neg(((diag(1.000000, 1^2) * broadcast((1.000000 - (var)[0]), lcont, [ 1 1 ])) + (diag(1.000000, 1^2) * broadcast((1.000000 - (var)[0]), lcont, [ 1 1 ])))) + (neg(((diag(1.000000, 1^2) * broadcast((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))), lcont, [ 1 1 ])) + ((diag(1.000000, 1^2) * broadcast(((var)[1] - ((var)[0] * (var)[0])), lcont, [ 1 1 ])) * broadcast(100.000000, lcont, [ 1 1 ])))) * broadcast((var)[0], lcont, [ 1 1 ]))) + (neg(((diag(1.000000, 1^2) * broadcast((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))), lcont, [ 1 1 ])) + ((diag(1.000000, 1^2) * broadcast(((var)[1] - ((var)[0] * (var)[0])), lcont, [ 1 1 ])) * broadcast(100.000000, lcont, [ 1 1 ])))) * broadcast((var)[0], lcont, [ 1 1 ]))), rset(0)), rset(0))))[[ 0 0 ]]
    ours:  0.000000 0.000000 |=> [-2.][0.]
    truth: 0.000000 0.000000 |=> -2.000000 0.000000
    ours:  5.000000 0.000000 |=> [50008.][-5000.]
    truth: 5.000000 0.000000 |=> 50008.000000 -5000.000000
    ours:  0.000000 5.000000 |=> [-2.][1000.]
    truth: 0.000000 5.000000 |=> -2.000000 1000.000000
    ours:  10.000000 0.000000 |=> [400018.][-20000.]
    truth: 10.000000 0.000000 |=> 400018.000000 -20000.000000
    ours:  5.000000 5.000000 |=> [40008.][-4000.]
    truth: 5.000000 5.000000 |=> 40008.000000 -4000.000000
    ours:  0.000000 10.000000 |=> [-2.][2000.]
    truth: 0.000000 10.000000 |=> -2.000000 2000.000000
    ours:  10.000000 5.000000 |=> [380018.][-19000.]
    truth: 10.000000 5.000000 |=> 380018.000000 -19000.000000
    ours:  5.000000 10.000000 |=> [30008.][-3000.]
    truth: 5.000000 10.000000 |=> 30008.000000 -3000.000000
    ours:  10.000000 10.000000 |=> [360018.][-18000.]
    truth: 10.000000 10.000000 |=> 360018.000000 -18000.000000 |}]

(*;
    let result = C.Vec.get grad Core.(i 0, i 0) in
    Format.printf "%a@." Core.pp res ;
    List.iter
      (fun x ->
        let y =
          Interp.interp [] res (Interp.Const.scalar x) |> Interp.get_scalar_exn
        in
        Fmt.printf "%f |=> %f@." x y)
      [0.0; 5.0; 10.0] ;
    [%expect
      {|
      var shape: 1
      result shape: [ 1 1 ]
      (((var)[0] * (0 = 0)?(1.000000):(0.000000)) + ((var)[0] * (0 = 0)?(1.000000):(0.000000)))
      0.000000 |=> 0.000000
      5.000000 |=> 10.000000
      10.000000 |=> 20.000000 |}]

  let f x = x * x * x

  let%expect_test "diff_cube" =
    let* grad = differentiate ~shape:Shape.scalar f in
    Format.printf "result shape: %a@." Shape.pp (C.Vec.shape grad) ;
    let res = C.Vec.get grad Core.(i 0, i 0) in
    Format.printf "%a@." Core.pp res ;
    List.iter
      (fun x ->
        let y =
          Interp.interp [] res (Interp.Const.scalar x) |> Interp.get_scalar_exn
        in
        Fmt.printf "%f |=> %f@." x y)
      [0.0; 5.0; 10.0] ;
    [%expect
      {|
      var shape: 1
      result shape: [ 1 1 ]
      (((((var * var))[0] * (0 = 0)?(1.000000):(0.000000)) + ((var)[0] * ((var)[0] * (0 = 0)?(1.000000):(0.000000)))) + ((var)[0] * ((var)[0] * (0 = 0)?(1.000000):(0.000000))))
      0.000000 |=> 0.000000
      5.000000 |=> 75.000000
      10.000000 |=> 300.000000 |}]

  let rosenbrock vec =
    let x = get vec Core.(i 0) in
    let y = get vec Core.(i 1) in
    let term1 = scalar 1. - x in
    let term2 = y - (x * x) in
    (term1 * term1) + (scalar 100. * term2 * term2)

  let rosenbrock_grad pos =
    let x = pos.(0) in
    let y = pos.(1) in
    [| 2. *. ((200. *. (x ** 3.)) -. (200. *. x *. y) +. x -. 1.);
       200. *. (y -. (x ** 2.))
    |]

  let of_array (a : float array) : _ Interp.Const.t =
    let len = Array.length a in
    let shape = Core.Shape.rank_one len in
    ( shape,
      fun (Core.I { desc; _ }) ->
        match desc with Index i -> a.(i) | Index_var _ -> assert false )

  let%expect_test "diff_rosenbrock" =
    let* grad = differentiate ~shape:(Shape.rank_one 2) rosenbrock in
    Format.printf "result shape: %a@." Shape.pp (C.Vec.shape grad) ;
    let resx = C.Vec.get grad Core.(i 0, i 0) in
    let resy = C.Vec.get grad Core.(i 1, i 0) in
    Format.printf "dx= %a@." Core.pp resx ;
    Format.printf "dy= %a@." Core.pp resx ;
    let inputs =
      let args = [0.; 5.; 10.] |> List.to_seq in
      Seq.product args args |> List.of_seq
    in
    List.iter
      (fun (x, y) ->
        let arr = [| x; y |] in
        let vec = of_array arr in
        let dx = Interp.interp [] resx vec |> Interp.get_scalar_exn in
        let dy = Interp.interp [] resy vec |> Interp.get_scalar_exn in
        Fmt.printf "ours:  %f %f |=> %f %f@." x y dx dy ;
        let real_grad = rosenbrock_grad arr in
        Fmt.printf "truth: %f %f |=> %f %f@." x y real_grad.(0) real_grad.(1))
      inputs ;
    [%expect
      {|
      var shape: 2
      result shape: [ 2 1 ]
      dx= ((1 = 0)?(((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))):(0.000000) + (0 = 0)?(((-((((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000)) + (((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000)))))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))))):(0.000000))
      dy= ((1 = 0)?(((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))):(0.000000) + (0 = 0)?(((-((((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000)) + (((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000)))))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))))):(0.000000))
      ours:  0.000000 0.000000 |=> -2.000000 0.000000
      truth: 0.000000 0.000000 |=> -2.000000 0.000000
      ours:  5.000000 0.000000 |=> 50008.000000 -5000.000000
      truth: 5.000000 0.000000 |=> 50008.000000 -5000.000000
      ours:  0.000000 5.000000 |=> -2.000000 1000.000000
      truth: 0.000000 5.000000 |=> -2.000000 1000.000000
      ours:  10.000000 0.000000 |=> 400018.000000 -20000.000000
      truth: 10.000000 0.000000 |=> 400018.000000 -20000.000000
      ours:  5.000000 5.000000 |=> 40008.000000 -4000.000000
      truth: 5.000000 5.000000 |=> 40008.000000 -4000.000000
      ours:  0.000000 10.000000 |=> -2.000000 2000.000000
      truth: 0.000000 10.000000 |=> -2.000000 2000.000000
      ours:  10.000000 5.000000 |=> 380018.000000 -19000.000000
      truth: 10.000000 5.000000 |=> 380018.000000 -19000.000000
      ours:  5.000000 10.000000 |=> 30008.000000 -3000.000000
      truth: 5.000000 10.000000 |=> 30008.000000 -3000.000000
      ours:  10.000000 10.000000 |=> 360018.000000 -18000.000000
      truth: 10.000000 10.000000 |=> 360018.000000 -18000.000000 |}]

  let%expect_test "diff_rosenbrock_hashconsed" =
    let* grad = differentiate ~shape:(Shape.rank_one 2) rosenbrock in
    Format.printf "result shape: %a@." Shape.pp (C.Vec.shape grad) ;
    let resx = C.Vec.get grad Core.(i 0, i 0) |> Core.hash_cons in
    let resy = C.Vec.get grad Core.(i 1, i 0) |> Core.hash_cons in
    Format.printf "dx= %a@." Core.pp resx ;
    Format.printf "dy= %a@." Core.pp resx ;
    let inputs =
      let args = [0.; 5.; 10.] |> List.to_seq in
      Seq.product args args |> List.of_seq
    in
    List.iter
      (fun (x, y) ->
        let arr = [| x; y |] in
        let vec = of_array arr in
        let dx = Interp.interp [] resx vec |> Interp.get_scalar_exn in
        let dy = Interp.interp [] resy vec |> Interp.get_scalar_exn in
        Fmt.printf "ours:  %f %f |=> %f %f@." x y dx dy ;
        let real_grad = rosenbrock_grad arr in
        Fmt.printf "truth: %f %f |=> %f %f@." x y real_grad.(0) real_grad.(1))
      inputs ;
    [%expect
      {|
      var shape: 2
      result shape: [ 2 1 ]
      dx= ((1 = 0)?(((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))):(0.000000) + (0 = 0)?(((-((((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000)) + (((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000)))))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))))):(0.000000))
      dy= ((1 = 0)?(((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))):(0.000000) + (0 = 0)?(((-((((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000)) + (((1.000000 - (var)[0]))[0] * (0 = 0)?(1.000000):(0.000000))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000)))))) + (((var)[0])[0] * -((((100.000000 * ((var)[1] - ((var)[0] * (var)[0]))))[0] * (0 = 0)?(1.000000):(0.000000)) + ((100.000000)[0] * ((((var)[1] - ((var)[0] * (var)[0])))[0] * (0 = 0)?(1.000000):(0.000000))))))):(0.000000))
      ours:  0.000000 0.000000 |=> -2.000000 0.000000
      truth: 0.000000 0.000000 |=> -2.000000 0.000000
      ours:  5.000000 0.000000 |=> 50008.000000 -5000.000000
      truth: 5.000000 0.000000 |=> 50008.000000 -5000.000000
      ours:  0.000000 5.000000 |=> -2.000000 1000.000000
      truth: 0.000000 5.000000 |=> -2.000000 1000.000000
      ours:  10.000000 0.000000 |=> 400018.000000 -20000.000000
      truth: 10.000000 0.000000 |=> 400018.000000 -20000.000000
      ours:  5.000000 5.000000 |=> 40008.000000 -4000.000000
      truth: 5.000000 5.000000 |=> 40008.000000 -4000.000000
      ours:  0.000000 10.000000 |=> -2.000000 2000.000000
      truth: 0.000000 10.000000 |=> -2.000000 2000.000000
      ours:  10.000000 5.000000 |=> 380018.000000 -19000.000000
      truth: 10.000000 5.000000 |=> 380018.000000 -19000.000000
      ours:  5.000000 10.000000 |=> 30008.000000 -3000.000000
      truth: 5.000000 10.000000 |=> 30008.000000 -3000.000000
      ours:  10.000000 10.000000 |=> 360018.000000 -18000.000000
      truth: 10.000000 10.000000 |=> 360018.000000 -18000.000000 |}] *)
