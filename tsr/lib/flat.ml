open Tsr_shape

type shape = int array

type index = int array

type slice = int option array

type path = bool array

type diagonal = shape * int

(* A permutation of indices, mapping dimension [i] to [iso.(i)]*)
type iso = int array

type _ desc =
  (* Vector values *)
  | Var : float desc
  | Float : float -> float desc
  | Array : Ba.t -> float desc
  (* Boolean values *)
  | True : bool desc
  | False : bool desc
  (* Vector expressions *)
  | Binop : ('a, 'b, 'c) binop * 'a t * 'b t -> 'c desc
  | Unop : ('a, 'b) unop * 'a t -> 'b desc
  | Get : 'a t * index -> 'a desc
  | Cond : bool t * 'a t * 'a t -> 'a desc
  | Set_slice :
      { operand : float t; update : float t; slice : slice }
      -> float desc
  | Contract : float t * path -> float desc
  | Broadcast : 'a t * path -> 'a desc
  | Slice : 'a t * slice -> 'a desc
  | Iso : 'a t * iso -> 'a desc
  | Diagonal : float t * diagonal -> float desc

and (_, _, _) binop =
  | Add : (float, float, float) binop
  | Sub : (float, float, float) binop
  | Mul : (float, float, float) binop
  | Div : (float, float, float) binop
  | Matmul : { mid : int } -> (float, float, float) binop
  | Dot : (float, float, float) binop
  | And : (bool, bool, bool) binop
  | Or : (bool, bool, bool) binop

and (_, _) unop =
  | Neg : (float, float) unop
  | Sqrt : (float, float) unop
  | Recip : (float, float) unop

and _ t = Flat : { tag : 'e Type.Id.t; desc : 'e desc; shape : shape } -> 'e t

module Errors = struct
  type ex_shape = Ex_shape : 'a Shape.t -> ex_shape

  type ex_shape_and_index =
    | Ex_shape_and_index : 'a Shape.t * 'a -> ex_shape_and_index

  type ex_shape_and_slice =
    | Ex_shape_and_slice :
        'a Shape.t * ('a, 'b) Shape.slice
        -> ex_shape_and_slice

  type ex_path = Ex_path : ('a, 'b) Shape.path -> ex_path

  let pp_ex_shape fmtr (Ex_shape sh) = Shape.pp fmtr sh

  let pp_ex_shape_and_index fmtr (Ex_shape_and_index (sh, i)) =
    Format.fprintf fmtr "%a, %a" Shape.pp sh (Shape.pp_index ~shape:sh ()) i

  let pp_ex_shape_and_slice fmtr (Ex_shape_and_slice (sh, sl)) =
    Format.fprintf fmtr "%a, %a" Shape.pp sh (Shape.pp_slice ~shape:sh ()) sl

  let pp_ex_path fmtr (Ex_path path) =
    Format.fprintf fmtr "%a" Shape.pp_path path

  let ex_shape sh = Ex_shape sh

  let ex_shape_index sh i = Ex_shape_and_index (sh, i)

  let ex_shape_and_slice sh sl = Ex_shape_and_slice (sh, sl)

  let ex_path path = Ex_path path

  type location = string

  type Error_monad.error +=
    | Flatten_shape_error of (location * ex_shape)
    | Flatten_index_error of (location * ex_shape_and_index)
    | Destruct_shape_error of (location * ex_shape)
    | Flatten_slice_error of (location * ex_shape_and_slice)
    | Flatten_path_error of (location * ex_path)

  let () =
    Error_monad.register
      (function Flatten_shape_error payload -> Some payload | _ -> None)
      (fun fmtr (location, shape) ->
        Format.fprintf
          fmtr
          "Flatten_shape_error(%s, %a)"
          location
          pp_ex_shape
          shape)

  let () =
    Error_monad.register
      (function Flatten_index_error payload -> Some payload | _ -> None)
      (fun fmtr (location, ex) ->
        Format.fprintf
          fmtr
          "Flatten_index_error(%s, %a)"
          location
          pp_ex_shape_and_index
          ex)

  let () =
    Error_monad.register
      (function Destruct_shape_error payload -> Some payload | _ -> None)
      (fun fmtr (location, shape) ->
        Format.fprintf
          fmtr
          "Destruct_shape_error(%s, %a)"
          location
          pp_ex_shape
          shape)

  let trace_flatten_shape ~loc shape k =
    Error_monad.trace (fun () -> Flatten_shape_error (loc, ex_shape shape)) k

  let trace_flatten_index ~loc shape index k =
    Error_monad.trace
      (fun () -> Flatten_index_error (loc, ex_shape_index shape index))
      k

  let trace_destruct_shape ~loc shape k =
    Error_monad.trace (fun () -> Destruct_shape_error (loc, ex_shape shape)) k

  let trace_flatten_slice ~loc shape slice k =
    Error_monad.trace
      (fun () -> Flatten_slice_error (loc, ex_shape_and_slice shape slice))
      k

  let trace_flatten_path ~loc path k =
    Error_monad.trace (fun () -> Flatten_path_error (loc, ex_path path)) k
end

(* Getters *)

let get_shape : type a. a t -> shape = function Flat { shape; _ } -> shape

let get_tag : type a. a t -> a Type.Id.t = function Flat { tag; _ } -> tag

(* Equality checking *)

let array_equal eq a1 a2 =
  Array.length a1 = Array.length a2 && Array.for_all2 eq a1 a2
[@@ocaml.inline]

let equal_shape (s1 : shape) (s2 : shape) = array_equal Int.equal s1 s2

let equal_index (i1 : index) (i2 : index) = array_equal Int.equal i1 i2

let equal_slice (sl1 : slice) (sl2 : slice) =
  array_equal (Option.equal Int.equal) sl1 sl2

let equal_path (p1 : path) (p2 : path) = array_equal Bool.equal p1 p2

let equal_diagonal (d1 : diagonal) (d2 : diagonal) =
  equal_shape (fst d1) (fst d2) && Int.equal (snd d1) (snd d2)

let equal_iso (i1 : iso) (i2 : iso) = array_equal Int.equal i1 i2

let equal_binop :
    type a b c d e f.
    (a, b, c) binop ->
    (d, e, f) binop ->
    ((a, d) Type.eq * (b, e) Type.eq * (c, f) Type.eq) option =
 fun op1 op2 ->
  match (op1, op2) with
  | (Add, Add) -> Some (Type.Equal, Type.Equal, Type.Equal)
  | (Sub, Sub) -> Some (Type.Equal, Type.Equal, Type.Equal)
  | (Mul, Mul) -> Some (Type.Equal, Type.Equal, Type.Equal)
  | (Div, Div) -> Some (Type.Equal, Type.Equal, Type.Equal)
  | (Matmul { mid = m1 }, Matmul { mid = m2 }) ->
      if m1 = m2 then Some (Type.Equal, Type.Equal, Type.Equal) else None
  | (Dot, Dot) -> Some (Type.Equal, Type.Equal, Type.Equal)
  | (And, And) -> Some (Type.Equal, Type.Equal, Type.Equal)
  | (Or, Or) -> Some (Type.Equal, Type.Equal, Type.Equal)
  | (Add, _) -> None
  | (Sub, _) -> None
  | (Mul, _) -> None
  | (Div, _) -> None
  | (Matmul _, _) -> None
  | (Dot, _) -> None
  | (And, _) -> None
  | (Or, _) -> None

let equal_unop :
    type a b c d. (a, b) unop -> (c, d) unop -> (b, d) Type.eq option =
 fun op1 op2 ->
  match (op1, op2) with
  | (Neg, Neg) -> Some Type.Equal
  | (Sqrt, Sqrt) -> Some Type.Equal
  | (Recip, Recip) -> Some Type.Equal
  | (Neg, _) -> None
  | (Sqrt, _) -> None
  | (Recip, _) -> None

let equal_desc : type a b. a desc -> b desc -> (a, b) Type.eq option =
 fun desc1 desc2 ->
  match (desc1, desc2) with
  | (Var, Var) -> Some (Type.Equal : (a, b) Type.eq)
  | (Float f1, Float f2) -> if f1 = f2 then Some Type.Equal else None
  | (Array ba1, Array ba2) -> if Ba.equal ba1 ba2 then Some Type.Equal else None
  | (True, True) -> Some Type.Equal
  | (False, False) -> Some Type.Equal
  | (Binop (op1, l1, r1), Binop (op2, l2, r2)) -> (
      match
        ( Type.Id.provably_equal (get_tag l1) (get_tag l2),
          Type.Id.provably_equal (get_tag r1) (get_tag r2) )
      with
      | (Some Type.Equal, Some Type.Equal) -> (
          match equal_binop op1 op2 with
          | Some (Type.Equal, Type.Equal, Type.Equal) -> Some Type.Equal
          | None -> None)
      | (None, _) | (_, None) -> None)
  | (Unop (op1, t1), Unop (op2, t2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> (
          match equal_unop op1 op2 with
          | Some Type.Equal -> Some Type.Equal
          | None -> None)
      | None -> None)
  | (Get (t1, i1), Get (t2, i2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> if i1 = i2 then Some Type.Equal else None
      | None -> None)
  | (Cond (cond1, ift1, iff1), Cond (cond2, ift2, iff2)) -> (
      match
        ( Type.Id.provably_equal (get_tag cond1) (get_tag cond2),
          Type.Id.provably_equal (get_tag ift1) (get_tag ift2),
          Type.Id.provably_equal (get_tag iff1) (get_tag iff2) )
      with
      | (Some Type.Equal, Some Type.Equal, Some Type.Equal) -> Some Type.Equal
      | _ -> None)
  | ( Set_slice { operand = op1; update = up1; slice = sl1 },
      Set_slice { operand = op2; update = up2; slice = sl2 } ) -> (
      match
        ( Type.Id.provably_equal (get_tag op1) (get_tag op2),
          Type.Id.provably_equal (get_tag up1) (get_tag up2) )
      with
      | (Some Type.Equal, Some Type.Equal) ->
          if equal_slice sl1 sl2 then Some Type.Equal else None
      | _ -> None)
  | (Contract (t1, p1), Contract (t2, p2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> if equal_path p1 p2 then Some Type.Equal else None
      | None -> None)
  | (Broadcast (t1, p1), Broadcast (t2, p2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> if equal_path p1 p2 then Some Type.Equal else None
      | None -> None)
  | (Slice (t1, sl1), Slice (t2, sl2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> if equal_slice sl1 sl2 then Some Type.Equal else None
      | None -> None)
  | (Iso (t1, iso1), Iso (t2, iso2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> if equal_iso iso1 iso2 then Some Type.Equal else None
      | None -> None)
  | (Diagonal (t1, diag1), Diagonal (t2, diag2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal ->
          if equal_diagonal diag1 diag2 then Some Type.Equal else None
      | None -> None)
  | (Var, _)
  | (Float _, _)
  | (Array _, _)
  | (True, _)
  | (False, _)
  | (Binop _, _)
  | (Unop _, _)
  | (Get _, _)
  | (Cond _, _)
  | (Set_slice _, _)
  | (Contract _, _)
  | (Broadcast _, _)
  | (Slice _, _)
  | (Iso _, _)
  | (Diagonal _, _) ->
      None

(* Conversion from [Core.t] *)

let shape_destruct shape = Shape.destruct shape |> Shape_error.lift

let shape_flatten shape = Shape.flatten shape |> Shape_error.lift

let shape_flatten_index shape index =
  Shape.flatten_index shape index |> Shape_error.lift

let of_binop :
    type a ea b eb c ec.
    a Shape.t ->
    b Shape.t ->
    (a, ea, b, eb, c, ec) Core.binop ->
    (ea, eb, ec) binop Error_monad.t =
 fun shape1 shape2 op ->
  let open Error_monad in
  match op with
  | Core.Add -> return Add
  | Core.Sub -> return Sub
  | Core.Mul -> return Mul
  | Core.Div -> return Div
  | Core.Matmul ->
      let* (_in1, mid1) = shape_destruct shape1 in
      let* (mid2, _out2) = shape_destruct shape2 in
      let* Type.Equal = Shape.assert_equal mid1 mid2 |> Shape_error.lift in
      let* mid_flat = shape_flatten mid1 in
      return (Matmul { mid = Array.length mid_flat })
  | Core.Dot -> return Dot
  | Core.And -> return And
  | Core.Or -> return Or

let of_unop : type a ea b eb. (a, ea, b, eb) Core.unop -> (ea, eb) unop =
 fun op ->
  match op with Core.Neg -> Neg | Core.Sqrt -> Sqrt | Core.Recip -> Recip

let rec of_slice :
    type a b. a Shape.t -> (a, b) Shape.slice -> int option array Error_monad.t
    =
 fun shape slice ->
  let open Error_monad in
  match slice with
  | Lset index ->
      let* (l, r) = shape_destruct shape in
      Errors.trace_flatten_index ~loc:__LOC__ l index @@ fun () ->
      let* lis = shape_flatten_index l index |> map (Array.map Option.some) in
      let* ris = shape_flatten r |> map (Array.map (Fun.const Option.none)) in
      return (Array.concat [lis; ris])
  | Rset index ->
      let* (l, r) = shape_destruct shape in
      let* lis = shape_flatten l |> map (Array.map (Fun.const Option.none)) in
      Errors.trace_flatten_index ~loc:__LOC__ r index @@ fun () ->
      let* ris = shape_flatten_index r index |> map (Array.map Option.some) in
      return (Array.concat [lis; ris])
  | L slice ->
      let* (l, r) = shape_destruct shape in
      let* lis = of_slice l slice in
      let* ris = shape_flatten r |> map (Array.map (Fun.const Option.none)) in
      return (Array.concat [lis; ris])
  | R slice ->
      let* (l, r) = shape_destruct shape in
      let* ris = of_slice r slice in
      let* lis = shape_flatten l |> map (Array.map (Fun.const Option.none)) in
      return (Array.concat [lis; ris])

let rec of_path :
    type a b. a Shape.t -> (a, b) Shape.path -> bool array Error_monad.t =
 fun shape path ->
  let open Error_monad in
  match path with
  | Lcont ->
      let* (l, r) = shape_destruct shape in
      Errors.trace_flatten_shape ~loc:__LOC__ l @@ fun () ->
      let* l = shape_flatten l |> map (Array.map (Fun.const true)) in
      Errors.trace_flatten_shape ~loc:__LOC__ r @@ fun () ->
      let* r = shape_flatten r |> map (Array.map (Fun.const false)) in
      return (Array.concat [l; r])
  | Rcont ->
      let* (l, r) = shape_destruct shape in
      Errors.trace_flatten_shape ~loc:__LOC__ l @@ fun () ->
      let* l = shape_flatten l |> map (Array.map (Fun.const false)) in
      Errors.trace_flatten_shape ~loc:__LOC__ r @@ fun () ->
      let* r = shape_flatten r |> map (Array.map (Fun.const true)) in
      return (Array.concat [l; r])
  | L path ->
      let* (l, r) = shape_destruct shape in
      let* l = of_path l path in
      Errors.trace_flatten_shape ~loc:__LOC__ r @@ fun () ->
      let* r = shape_flatten r |> map (Array.map (Fun.const false)) in
      return (Array.concat [l; r])
  | R path ->
      let* (l, r) = shape_destruct shape in
      let* r = of_path r path in
      Errors.trace_flatten_shape ~loc:__LOC__ l @@ fun () ->
      let* l = shape_flatten l |> map (Array.map (Fun.const false)) in
      return (Array.concat [l; r])

let rec of_iso : type a b. a Shape.t -> (a, b) Shape.iso -> iso Error_monad.t =
 fun shape iso ->
  let open Error_monad in
  Errors.trace_flatten_shape ~loc:__LOC__ shape @@ fun () ->
  let* flattened = shape_flatten shape in
  let numel = Array.length flattened in
  match iso with
  | LAssoc -> return (Array.init numel Fun.id)
  | RAssoc -> return (Array.init numel Fun.id)
  | Transpose ->
      let* (l, r) = shape_destruct shape in
      let* l = shape_flatten l in
      let* r = shape_flatten r in
      let liso = Array.init (Array.length l) Fun.id in
      let riso = Array.init (Array.length r) (fun i -> i + Array.length l) in
      return (Array.concat [riso; liso])
  | LMap iso ->
      let* (l, r) = shape_destruct shape in
      let* liso = of_iso l iso in
      let* r = shape_flatten r in
      let riso = Array.init (Array.length r) (fun i -> i + Array.length liso) in
      return (Array.concat [liso; riso])
  | RMap iso ->
      let* (l, r) = shape_destruct shape in
      let* riso = of_iso r iso in
      let* l = shape_flatten l in
      let liso = Array.init (Array.length l) (fun i -> i + Array.length riso) in
      return (Array.concat [liso; riso])
  | Circ (i1, i2) ->
      let* arr1 = of_iso shape i1 in
      let* shape2 = Shape.apply_iso shape i1 |> Shape_error.lift in
      let* arr2 = of_iso shape2 i2 in
      return (Array.init (Array.length arr2) (fun i -> arr2.(arr1.(i))))

let of_diag :
    type base total. (base, total) Shape.diagonal -> (shape * int) Error_monad.t
    =
 fun diagonal ->
  let open Error_monad in
  let rec loop : type base total. (base, total) Shape.diagonal -> _ =
   fun diagonal acc ->
    match diagonal with
    | Shape.Diag_base sh ->
        let* sh = shape_flatten sh in
        return (sh, acc)
    | Diag_cons diag -> loop diag (acc + 1)
  in
  loop diagonal 1

(* We want to preserve sharing and need to store terms in a heterogeneous hash table. *)
type elt = Key : ('a, 'elt) Core.tensor Type.Id.t * 'elt t -> elt

let flatten core =
  let table = Hashtbl.create 101 in
  let rec flatten :
      type a elt. (a, elt) Core.tensor Core.t -> elt t Error_monad.t =
   fun (Core.S { hash = _; tag; desc; shape }) ->
    let open Error_monad in
    match Hashtbl.find_opt table (Type.Id.uid tag) with
    | Some (Key (tag', elt)) -> (
        match Type.Id.provably_equal tag tag' with
        | Some Type.Equal -> return elt
        | None -> assert false)
    | None ->
        let* desc = flatten_desc shape desc in
        let* shape = shape_flatten shape in
        let elt = Flat { tag = Type.Id.make (); desc; shape } in
        Hashtbl.add table (Type.Id.uid tag) (Key (tag, elt)) ;
        return @@ elt
  and flatten_desc :
      type a elt.
      a Shape.t -> (a, elt) Core.tensor Core.desc -> elt desc Error_monad.t =
   fun shape desc ->
    let open Error_monad in
    match desc with
    | Core.Var -> return Var
    | Core.Float f -> return (Float f)
    | Core.Array ba -> return (Array ba)
    | Core.True -> return True
    | Core.False -> return False
    | Core.Binop (op, l, r) ->
        let* op = of_binop (Core.get_shape l) (Core.get_shape r) op in
        let* l = flatten l in
        let* r = flatten r in
        return (Binop (op, l, r))
    | Core.Unop (op, t) ->
        let* t = flatten t in
        return (Unop (of_unop op, t))
    | Core.Get (t, i) ->
        let ts = Core.get_shape t in
        let* t = flatten t in
        Errors.trace_flatten_index ~loc:__LOC__ ts i @@ fun () ->
        let* i = shape_flatten_index ts i in
        return (Get (t, i))
    | Core.Cond (cond, ift, iff) ->
        let* cond = flatten cond in
        let* ift = flatten ift in
        let* iff = flatten iff in
        return (Cond (cond, ift, iff))
    | Core.Set_slice { operand; update; slice } ->
        Errors.trace_flatten_slice ~loc:__LOC__ (Core.get_shape operand) slice
        @@ fun () ->
        let* slice = of_slice (Core.get_shape operand) slice in
        let* operand = flatten operand in
        let* update = flatten update in
        return (Set_slice { operand; update; slice })
    | Core.Contract (t, path) ->
        Errors.trace_flatten_path ~loc:__LOC__ path @@ fun () ->
        let* path = of_path (Core.get_shape t) path in
        let* t = flatten t in
        return (Contract (t, path))
    | Core.Broadcast (t, path) ->
        Errors.trace_flatten_path ~loc:__LOC__ path @@ fun () ->
        let* path = of_path shape path in
        let* t = flatten t in
        return (Broadcast (t, path))
    | Core.Slice (t, sl) ->
        Errors.trace_flatten_slice ~loc:__LOC__ (Core.get_shape t) sl
        @@ fun () ->
        let* sl = of_slice (Core.get_shape t) sl in
        let* t = flatten t in
        return (Slice (t, sl))
    | Core.Iso (t, iso) ->
        let* iso = of_iso (Core.get_shape t) iso in
        let* t = flatten t in
        return (Iso (t, iso))
    | Core.Diagonal (t, diag) ->
        let* t = flatten t in
        let* (base_shape, dim) = of_diag diag in
        return (Diagonal (t, (base_shape, dim)))
  in
  flatten core

let pp_array pp_elt fmtr arr =
  Format.pp_print_array
    ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",")
    pp_elt
    fmtr
    arr

let shape_of_bigarray (ba : Ba.t) : shape = [| Ba.length ba |]

let%expect_test "flattened_shape" =
  let open Error_monad in
  run
    (let shape =
       Shape.tensor
         (Shape.rank_one 12)
         (Shape.tensor (Shape.rank_one 6) (Shape.rank_one 3))
     in
     let* flattened = shape_flatten shape in
     Format.printf
       "shape = %a, flattened = %a@."
       Shape.pp
       shape
       (pp_array Format.pp_print_int)
       flattened ;
     return_unit) ;
  [%expect {| shape = [ 12 [ 6 3 ] ], flattened = 12,6,3 |}]

let%expect_test "flattened_index" =
  let open Error_monad in
  run
    (let shape =
       Shape.tensor
         (Shape.rank_one 12)
         (Shape.tensor (Shape.rank_one 6) (Shape.rank_one 3))
     in
     let index = (0, (1, 2)) in
     let* flattened_index = shape_flatten_index shape index in
     Format.printf
       "shape = %a, flattened_index = %a@."
       Shape.pp
       shape
       (pp_array Format.pp_print_int)
       flattened_index ;
     return_unit) ;
  [%expect {| shape = [ 12 [ 6 3 ] ], flattened_index = 0,1,2 |}]

let%expect_test "flattened_slice" =
  let open Error_monad in
  let pp_slice =
    pp_array
      (Format.pp_print_option
         ~none:(fun fmtr () -> Format.fprintf fmtr "?")
         Format.pp_print_int)
  in
  run
    (let shape =
       Shape.tensor
         (Shape.rank_one 12)
         (Shape.tensor (Shape.rank_one 6) (Shape.rank_one 3))
     in
     let slice = Shape.(R (Lset 1)) in
     let* flattened_slice = of_slice shape slice in
     let slice2 = Shape.(Rset (1, 2)) in
     let* flattened_slice2 = of_slice shape slice2 in
     Format.printf
       "shape = %a, slice1 = %a, flattened_slice1 = %a slice2 = %a, \
        flattened_slice2 = %a@."
       Shape.pp
       shape
       (Shape.pp_slice ~shape ())
       slice
       pp_slice
       flattened_slice
       (Shape.pp_slice ~shape ())
       slice2
       pp_slice
       flattened_slice2 ;
     return_unit) ;
  [%expect
    {| shape = [ 12 [ 6 3 ] ], slice1 = R(lset(1)), flattened_slice1 = ?,1,? slice2 = rset([ 1 2 ]), flattened_slice2 = ?,1,2 |}]

let%expect_test "flattened_path" =
  let open Error_monad in
  let pp_path = pp_array Format.pp_print_bool in
  run
    (let shape =
       Shape.tensor
         (Shape.rank_one 12)
         (Shape.tensor (Shape.rank_one 6) (Shape.rank_one 3))
     in
     let path : (_, _) Shape.path = Shape.R Shape.Lcont in
     let* contracted = Shape.contract shape path |> Shape_error.lift in
     let* flattened = of_path shape path in
     Format.printf
       "shape = %a, path = %a, contracted = %a@."
       Shape.pp
       shape
       Shape.pp_path
       path
       Shape.pp
       contracted ;
     Format.printf "flattened path = %a@." pp_path flattened ;
     return_unit) ;
  [%expect
    {|
    shape = [ 12 [ 6 3 ] ], path = R(lcont), contracted = [ 12 3 ]
    flattened path = false,true,false |}]

let%expect_test "flattened_isos" =
  let open Error_monad in
  let pp_flattened = pp_array Format.pp_print_int in
  run
    (let shape =
       Shape.tensor
         (Shape.rank_one 12)
         (Shape.tensor (Shape.rank_one 6) (Shape.rank_one 3))
     in
     let iso = Shape.Iso.(rassoc |> lmap tr) in
     let* shape' = Shape.apply_iso shape iso |> Shape_error.lift in
     let () =
       Format.printf
         "shape = %a, iso = %a, shape' = %a@."
         Shape.pp
         shape
         Shape.pp_iso
         iso
         Shape.pp
         shape'
     in
     let iso_inv = Shape.inverse iso in
     let comp = Shape.Iso.(iso_inv |> iso) in
     let* iso = of_iso shape iso in
     let* iso_inv = of_iso shape' iso_inv in
     let* iso_comp = of_iso shape' comp in
     Format.printf
       "iso = %a, iso_inv = %a, composition = %a@."
       pp_flattened
       iso
       pp_flattened
       iso_inv
       pp_flattened
       iso_comp ;
     return_unit) ;
  [%expect
    {|
    shape = [ 12 [ 6 3 ] ], iso = (rassoc;lmap(transpose)), shape' = [ [ 6 12 ] 3 ]
    iso = 1,0,2, iso_inv = 1,0,2, composition = 0,1,2 |}]

let%expect_test "flattened_isos_2" =
  let open Error_monad in
  let pp_flattened = pp_array Format.pp_print_int in
  run
    (let shape =
       Shape.tensor
         (Shape.rank_one 12)
         (Shape.tensor (Shape.rank_one 6) (Shape.rank_one 3))
     in
     let iso1 = Shape.Iso.(rmap tr) in
     let iso2 = Shape.Iso.rassoc in
     let iso3 = Shape.Iso.(lmap tr) in
     let iso = Shape.Iso.(iso1 |> iso2 |> iso3) in
     let* shape' = Shape.apply_iso shape iso |> Shape_error.lift in
     let () =
       Format.printf
         "shape = %a, iso = %a, shape' = %a@."
         Shape.pp
         shape
         Shape.pp_iso
         iso
         Shape.pp
         shape'
     in
     let iso_inv = Shape.inverse iso in
     let comp = Shape.Iso.(iso_inv |> iso) in
     let* iso = of_iso shape iso in
     let* iso_inv = of_iso shape' iso_inv in
     let* iso_comp = of_iso shape' comp in
     Format.printf
       "iso = %a, iso_inv = %a, composition = %a@."
       pp_flattened
       iso
       pp_flattened
       iso_inv
       pp_flattened
       iso_comp ;
     return_unit) ;
  [%expect
    {|
    shape = [ 12 [ 6 3 ] ], iso = ((rmap(transpose);rassoc);lmap(transpose)), shape' = [ [ 3 12 ] 6 ]
    iso = 2,0,1, iso_inv = 1,2,0, composition = 0,1,2 |}]
