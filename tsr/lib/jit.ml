(*module L = Suplex.Llvm_impl
  module R = L.F64
  module BA = L.Exec.F64_ba
  module T = Linalg.Llvm.Tensor
  module Shape = Core.Shape

  module BA_rel = struct
    type elt = float

    type kind = Bigarray.float64_elt

    let rel = L.Exec.bigarray_f64
  end

  (* Vector-shaped overlays over generated program arrays *)
  module Vec = struct
    include Linalg.Vec.Make (L) (T) (R)
    include Linalg.Llvm.Array_backed (L) (T) (R)
  end

  (* Matrix-shaped overlays over generated program arrays *)
  module Mat = struct
    include
      Linalg.Mat.Make (L) (T)
        (struct
          include L.I64

          let foldi = L.foldi
        end)
        (R)

    include Linalg.Llvm.Array_backed_column_major (L) (T) (R)
  end

  (* The ring of tensor sizes. Sizes are statically known, hence we use [int]. *)
  module Size : T.Ring with type t = int = struct
    include Int

    let to_pos i = L.I64.v (Int64.of_int i)
  end

  let size : int T.ring = (module Size)

  type 'a k = 'a L.K.t

  type 'a m = 'a L.Types.m

  type index = T.pos

  type ('a, 'b, 'sa, 'sb) morphism = ('a, 'b, 'sa, 'sb) T.Morphism.t

  type 'a view = ('a, index m, int, int) morphism

  type 'a vec = ('a, int) Vec.t

  type 'a ovec = ('a, int) Vec.out

  type ('a, 'b) mat = ('a, 'b, int) Mat.mat

  module View = struct
    include T.Morphism

    let scalar : T.pos m view = identity (T.scalar size)

    let rank_one s : T.pos m view = identity (T.rank_one size s)

    let rank_two s1 s2 : (T.pos m * T.pos m) view =
      column_major (T.rank_two size s1 s2)
  end

  module Rel = struct
    (* Relating [Shape.t] and [View.t] *)
    type (_, _) t =
      | Rank_one : int -> (Core.index Core.t, T.pos m) t
      | Tensor : ('a, 'b) t * ('c, 'd) t -> ('a * 'c, 'b * 'd) t

    type _ wrapped = Rel : ('shape, 'view) t -> 'shape wrapped [@@ocaml.unboxed]

    type _ wrapped_vec =
      | Vec :
          { rel : ('shape, 'view) t; shape : 'shape Core.shape; vec : 'view vec }
          -> 'shape Core.vector wrapped_vec
      | Bool : { vec : index m vec } -> Core.boolean wrapped_vec
      | Index : { idx : index m } -> Core.index wrapped_vec

    let rec get_tensor_shape : type a b. (a, b) t -> (b, Size.t) T.t =
     fun rel ->
      match rel with
      | Rank_one d -> T.rank_one size d
      | Tensor (l, r) -> T.tensor (get_tensor_shape l) (get_tensor_shape r)

    let rec get_core_shape : type a b. (a, b) t -> a Core.shape =
     fun rel ->
      match rel with
      | Rank_one d -> Shape.rank_one d
      | Tensor (l, r) -> Shape.tensor (get_core_shape l) (get_core_shape r)

    let rec of_core_shape : type a. a Core.shape -> a wrapped =
     fun shape ->
      match Core.Shape.get_var shape with
      | Unknown -> failwith "of_core_shape"
      | Desc desc -> (
          match desc with
          | Core.Dim d -> Rel (Rank_one d)
          | Core.Tensor (l, r) ->
              let (Rel vl) = of_core_shape l in
              let (Rel vr) = of_core_shape r in
              Rel (Tensor (vl, vr)))

    let rec elim :
        type shape v1 v2. (shape, v1) t -> (shape, v2) t -> (v1, v2) Refl.eq =
     fun rel1 rel2 ->
      match (rel1, rel2) with
      | (Rank_one d1, Rank_one d2) ->
          (* TODO: make this into a proper fatal error. *)
          assert (d1 = d2) ;
          Refl.Refl
      | (Tensor (l1, r1), Tensor (l2, r2)) -> (
          match (elim l1 l2, elim r1 r2) with (Refl, Refl) -> Refl)
  end

  type env = (Core.index_var * T.pos m) list

  let rec extend_env :
      type a b. env -> (a, b) Rel.t -> a Core.index_tuple -> b -> env =
   fun env rel indexing_var index ->
    match rel with
    | Rel.Rank_one _dim ->
        let (Core.Index_one v) = indexing_var in
        (v, index) :: env
    | Rel.Tensor (lrel, rrel) ->
        let (Core.Index_tensor (vl, vr)) = indexing_var in
        let (il, ir) = index in
        let env = extend_env env lrel vl il in
        let env = extend_env env rrel vr ir in
        env

  let env_assoc_desc : env -> Core.index Core.desc -> index m =
   fun env (desc : Core.index Core.desc) ->
    match desc with
    | Core.Index i -> Size.to_pos i
    | Core.Index_var v -> (
        match List.find_opt (fun (a, _b) -> v = a) env with
        | None -> Format.kasprintf failwith "env_assoc: index %d not found" v.id
        | Some (_a, b) -> b)

  let env_assoc env (Core.I { desc; _ }) = env_assoc_desc env desc

  let rec get_suplex_index : type a b. env -> (a, b) Rel.t -> a -> b =
   fun env rel i ->
    match rel with
    | Rel.Rank_one _ -> env_assoc env i
    | Rel.Tensor (lrel, rrel) ->
        let (il, ir) = i in
        let l = get_suplex_index env lrel il in
        let r = get_suplex_index env rrel ir in
        (l, r)

  let rec is_index_in_slice :
      type a b.
      env ->
      shape:a Core.shape ->
      rel:(a, b) Rel.t ->
      sub:a Core.shape ->
      at:a ->
      b ->
      bool m =
   fun env ~shape ~rel ~sub ~at index ->
    match (Shape.get_var shape, Shape.get_var sub) with
    | (Unknown, _) | (_, Unknown) -> failwith "is_index_in_slice: undefined shape"
    | (Desc shape, Desc sub) -> (
        let open L in
        match (shape, sub, rel) with
        | (Dim _shape_size, Dim sub_size, Rel.Rank_one _) ->
            (* shape: 0; ...; shape_size - 1
               sub:   at; ...; at + sub_size - 1 *)
            (* assert (at >= 0) ; *)
            (* assert (at < shape_size) ; *)
            (* assert (index >= 0) ; *)
            (* assert (index < shape_size) ; *)
            let at = env_assoc env at in
            I64.(le at index && lt index (add at (Size.to_pos sub_size)))
            (* (function true -> I64.sub index at | false -> I64.neg I64.one) *)
        | (Tensor (l1, r1), Tensor (l2, r2), Rel.Tensor (lrel, rrel)) ->
            is_index_in_slice
              env
              ~shape:l1
              ~rel:lrel
              ~sub:l2
              ~at:(fst at)
              (fst index)
            && is_index_in_slice
                 env
                 ~shape:r1
                 ~rel:rrel
                 ~sub:r2
                 ~at:(snd at)
                 (snd index))

  let rec index_to_slice :
      type a b b'.
      env ->
      shape:a Core.shape ->
      rel:(a, b) Rel.t ->
      sub:a Core.shape ->
      sub_rel:(a, b') Rel.t ->
      at:a ->
      b ->
      b' =
   fun env ~shape ~rel ~sub ~sub_rel ~at index ->
    match (Shape.get_var shape, Shape.get_var sub) with
    | (Unknown, _) | (_, Unknown) -> failwith "is_index_in_slice: undefined shape"
    | (Desc shape, Desc sub) -> (
        let open L in
        match (shape, sub, rel, sub_rel) with
        | (Dim _shape_size, Dim _sub_size, Rel.Rank_one _, Rel.Rank_one _) ->
            (* shape: 0; ...; shape_size - 1
               sub:   at; ...; at + sub_size - 1 *)
            (* assert (at >= 0) ; *)
            (* assert (at < shape_size) ; *)
            (* assert (index >= 0) ; *)
            (* assert (index < shape_size) ; *)
            let at = env_assoc env at in
            I64.sub index at
        | ( Tensor (l1, r1),
            Tensor (l2, r2),
            Rel.Tensor (lrel, rrel),
            Rel.Tensor (lrel', rrel') ) ->
            ( index_to_slice
                env
                ~shape:l1
                ~rel:lrel
                ~sub:l2
                ~sub_rel:lrel'
                ~at:(fst at)
                (fst index),
              index_to_slice
                env
                ~shape:r1
                ~rel:rrel
                ~sub:r2
                ~sub_rel:rrel'
                ~at:(snd at)
                (snd index) ))

  let rec index_equal : type a b. (a, b) Rel.t -> b -> b -> bool m =
   fun rel i1 i2 ->
    let open L in
    match rel with
    | Rel.Rank_one _ -> I64.eq i1 i2
    | Rel.Tensor (l, r) ->
        index_equal l (fst i1) (fst i2) && index_equal r (snd i1) (snd i2)

  module Workspace = struct
    (* TODO: static data should be an array of bigarrays.
       This would avoid the need to copy data from bigarrays to the workspace.
       OTOH, it requires to add dummy pointers to these bigarrays so that the runtime
       doesn't free the bigarrays.
    *)
    type shared =
      { mutable max_allocated : int; mutable currently_allocated : int }

    type static_data = { ba : Ba.t; offset : int }

    type split =
      { mutable total_allocated : int; mutable static : static_data list }

    type allocation_state = { shared : shared; split : split }

    type t =
      { data : (R.t, [ `unk ]) L.arr m;
        static : (R.t, [ `unk ]) L.arr m;
        state : allocation_state
      }

    let create ~data ~static =
      { data;
        static;
        state =
          { shared = { max_allocated = 0; currently_allocated = 0 };
            split = { total_allocated = 0; static = [] }
          }
      }

    let get { data; static = _; state } (shape : ('a, int) T.t) k =
      let numel = T.numel shape in
      let shared = state.shared in
      let offset =
        let ofs = state.shared.currently_allocated in
        shared.currently_allocated <- shared.currently_allocated + numel ;
        shared.max_allocated <-
          Int.max shared.currently_allocated shared.max_allocated ;
        ofs
      in
      let (vec, ovec) =
        if offset = 0 then Vec.of_array size numel data
        else
          let ofs = Size.to_pos offset in
          let shape = T.rank_one size numel in
          let vec = Vec.make shape L.(fun i -> data.%[I64.add ofs i]) in
          let ovec =
            Vec.make_out shape L.(fun i v -> data.%[I64.add ofs i] <- v)
          in
          (vec, ovec)
      in
      let open L.K in
      let* vec = Vec.pullback (T.Morphism.flatten_column_major shape) vec in
      let* ovec = Vec.pullback_o (T.Morphism.flatten_column_major shape) ovec in
      let result = k (vec, ovec) in
      shared.currently_allocated <- shared.currently_allocated - numel ;
      result

    let static { data = _; static; state } ba k =
      let split = state.split in
      let offset = split.total_allocated in
      let numel = Ba.length ba in
      split.total_allocated <- split.total_allocated + numel ;
      let ofs = Size.to_pos offset in
      let shape = T.rank_one size numel in
      let vec = Vec.make shape L.(fun i -> static.%[I64.add ofs i]) in
      let ovec = Vec.make_out shape L.(fun i v -> static.%[I64.add ofs i] <- v) in
      k (vec, ovec)

    (* TODO: get vec for static data

       - then remove Const stuff
       - then add reshaping primitives to Core
    *)
  end

  let jit_binop :
      type a b c.
      (a, b, c) Core.binop ->
      a Core.vector Rel.wrapped_vec ->
      b Core.vector Rel.wrapped_vec ->
      c Core.vector Rel.wrapped_vec k =
   fun op l r ->
    let open L.K in
    let (Vec l) = l in
    let (Vec r) = r in
    match op with
    | Core.Add ->
        let Refl = Rel.elim l.rel r.rel in
        let* vec = Vec.add l.vec r.vec in
        return (Rel.Vec { rel = l.rel; shape = l.shape; vec })
    | Core.Sub ->
        let Refl = Rel.elim l.rel r.rel in
        let* vec = Vec.sub l.vec r.vec in
        return (Rel.Vec { rel = l.rel; shape = l.shape; vec })
    | Core.Mul ->
        let Refl = Rel.elim l.rel r.rel in
        let* vec = Vec.mul l.vec r.vec in
        return (Rel.Vec { rel = l.rel; shape = l.shape; vec })
    | Core.Matmul ->
        let (Tensor (l1, r1)) = l.rel in
        let (Tensor (l2, r2)) = r.rel in
        let Refl = Rel.elim l1 r2 in
        let* vec = Mat.mm l.vec r.vec in
        let rel = Rel.Tensor (l2, r1) in
        let shape = Rel.get_core_shape rel in
        return (Rel.Vec { rel; shape; vec })
    | Core.Dot ->
        let open L in
        let Refl = Rel.elim l.rel r.rel in
        let*! res = Vec.dot l.vec r.vec in
        let vec = Vec.const (T.scalar size) res in
        let rel = Rel.Rank_one 1 in
        let shape = Rel.get_core_shape rel in
        return (Rel.Vec { rel; shape; vec })

  let jit_main :
      type input output.
      Workspace.t ->
      (input Core.vector Core.t -> output Core.vector Core.t) ->
      input Core.vector Rel.wrapped_vec ->
      output Core.vector Rel.wrapped_vec k =
   fun workspace f input ->
    let open L.K in
    let (Rel.Vec i) = input in
    let core_in_shape = Rel.get_core_shape i.rel in

    let var = Core.Hash_consing.var core_in_shape in

    let body = f var in

    let () =
      match Core.Shape.get_var core_in_shape with
      | Unknown ->
          Format.kasprintf failwith "Jit: could not determine input shape."
      | Desc _ -> ()
    in

    let (Rel in_rel) = Rel.of_core_shape core_in_shape in

    let rec of_core : type a. env -> a Core.t -> a Rel.wrapped_vec k =
     fun env term ->
      match term with
      | Core.V { desc; shape; _ } -> of_core_vec_desc env shape desc
      | Core.B { desc; _ } -> of_core_bool_desc env desc
      | Core.I { desc; _ } -> of_core_index_desc env desc
    and of_core_vec_desc :
        type a.
        env ->
        a Core.shape ->
        a Core.vector Core.desc ->
        a Core.vector Rel.wrapped_vec k =
     fun env shape desc ->
      match desc with
      | Core.Var -> (
          match Shape.equal shape core_in_shape with
          | None ->
              Format.kasprintf
                failwith
                "Jit.of_core_vec_desc: unexpected variable shape %a (expected %a)"
                Core.pp_shape
                shape
                Core.pp_shape
                core_in_shape
          | Some Refl.Refl ->
              let (Rel.Vec i) = input in
              let Refl = Rel.elim i.rel in_rel in
              return input)
      | Core.Float f ->
          let (Rel rel) = Rel.of_core_shape shape in
          let vec = Vec.const (Rel.get_tensor_shape rel) (R.v f) in
          return @@ Rel.Vec { rel; shape; vec }
      | Core.Array ba ->
          Workspace.static workspace ba @@ fun (vec, _) ->
          let (Rel rel) = Rel.of_core_shape shape in
          let Refl = Rel.elim rel (Rel.Rank_one (Ba.length ba)) in
          return @@ Rel.Vec { rel; shape; vec }
      | Core.Binop (op, l, r) ->
          let* l = of_core env l in
          let* r = of_core env r in
          jit_binop op l r
      | Core.Unop (Neg, e) ->
          let* (Vec e) = of_core env e in
          let vec = Vec.neg e.vec in
          return (Rel.Vec { rel = e.rel; shape; vec })
      | Core.Get (e, i) ->
          let open L in
          let* (Vec e) = of_core env e in
          let*! xi = Vec.get e.vec (get_suplex_index env e.rel i) in
          let vec = Vec.const (T.scalar size) xi in
          return (Rel.Vec { rel = Rel.Rank_one 1; shape; vec })
      | Core.Cond (c, ift, iff) ->
          let* (Bool c) = of_core env c in
          let* (Vec ift) = of_core env ift in
          let* (Vec iff) = of_core env iff in
          let Refl = Rel.elim ift.rel iff.rel in
          let res_shape = Vec.idim ift.vec in
          let vec =
            Vec.make res_shape (fun i ->
                let open L in
                let*! c = Vec.get c.vec I64.zero in
                cond F64.(eq zero c) @@ function
                | true ->
                    (* c = zero -> c is false *)
                    Vec.get iff.vec i
                | false -> Vec.get ift.vec i)
          in
          return (Rel.Vec { rel = ift.rel; shape; vec })
      | Core.Fold (binop, indexing_shape, indexing_variables, f, init) -> (
          let open L in
          let (Rel indexing_rel) = Rel.of_core_shape indexing_shape in
          let (Rel rel) = Rel.of_core_shape shape in
          let indexing_shape = Rel.get_tensor_shape indexing_rel in
          let* (Vec init) = of_core env init in
          let Refl = Rel.elim rel init.rel in
          (* Some binary operations don't self-alias, they need less storage and less copy *)
          match binop with
          | Add | Sub | Mul | Dot ->
              Workspace.get workspace (Rel.get_tensor_shape rel)
              @@ fun (wrk, owrk) ->
              let*! _ = Vec.(owrk := init.vec) in
              let*! _ =
                T.fold
                  (fun index _ ->
                    let env =
                      extend_env env indexing_rel indexing_variables index
                    in
                    let* f_index = of_core env f in
                    let* (Vec res) =
                      jit_binop
                        binop
                        (Rel.Vec { rel = init.rel; shape = init.shape; vec = wrk })
                        f_index
                    in
                    let Refl = Rel.elim res.rel init.rel in
                    Vec.(owrk := res.vec))
                  indexing_shape
                  unit
              in
              return (Rel.Vec { rel = init.rel; shape; vec = wrk })
          | Matmul ->
              Workspace.get workspace (Rel.get_tensor_shape rel)
              @@ fun (wrk0, owrk0) ->
              Workspace.get workspace (Rel.get_tensor_shape rel)
              @@ fun (wrk1, owrk1) ->
              Workspace.get workspace (Rel.get_tensor_shape rel)
              @@ fun (wrk2, owrk2) ->
              let*! _ = Vec.(owrk0 := init.vec) in
              let*! _ =
                T.fold
                  (fun index _ ->
                    let env =
                      extend_env env indexing_rel indexing_variables index
                    in
                    let* (Vec f_index) = of_core env f in
                    let Refl = Rel.elim f_index.rel init.rel in
                    let*! _ = Vec.(owrk1 := f_index.vec) in
                    let* (Vec res) =
                      jit_binop
                        binop
                        (Rel.Vec
                           { rel = init.rel; shape = init.shape; vec = wrk0 })
                        (Rel.Vec
                           { rel = init.rel; shape = init.shape; vec = wrk1 })
                    in
                    let Refl = Rel.elim res.rel init.rel in
                    let*! _ = Vec.(owrk2 := res.vec) in
                    Vec.(owrk0 := wrk2))
                  indexing_shape
                  unit
              in
              return (Rel.Vec { rel = init.rel; shape; vec = wrk0 }))
      | Set_slice { operand; update; indices } ->
          let* (Vec operand) = of_core env operand in
          let* (Vec update) = of_core env update in
          let vec =
            Vec.make (Vec.idim operand.vec) @@ fun i ->
            let open L in
            cond
              (is_index_in_slice
                 env
                 ~shape:operand.shape
                 ~rel:operand.rel
                 ~sub:update.shape
                 ~at:indices
                 i)
            @@ function
            | true ->
                let i =
                  index_to_slice
                    env
                    ~shape:operand.shape
                    ~rel:operand.rel
                    ~sub:update.shape
                    ~sub_rel:update.rel
                    ~at:indices
                    i
                in
                Vec.get update.vec i
            | false -> Vec.get operand.vec i
          in
          return (Rel.Vec { rel = operand.rel; shape = operand.shape; vec })
      | Broadcast (v, _sh) ->
          let* (Vec v) = of_core env v in
          let (Rel (Tensor (l, _) as rel)) = Rel.of_core_shape shape in
          let Refl = Rel.elim v.rel l in
          let vec =
            Vec.make (Rel.get_tensor_shape rel) @@ fun (i, _) -> Vec.get v.vec i
          in
          return (Rel.Vec { rel; shape; vec })
      | Delta (sh, pos, v) ->
          let (Rel (Tensor (sh_rel, v_rel) as rel)) = Rel.of_core_shape shape in
          let (Rel rel') = Rel.of_core_shape sh in
          let Refl = Rel.elim sh_rel rel' in
          let* (Vec v) = of_core env v in
          let Refl = Rel.elim v.rel v_rel in
          let pos = get_suplex_index env sh_rel pos in
          let vec =
            Vec.make (Rel.get_tensor_shape rel) (fun (i, j) ->
                let open L in
                cond (index_equal sh_rel pos i) @@ function
                | true -> Vec.get v.vec j
                | false -> F64.zero)
          in
          return (Rel.Vec { rel; shape; vec })
    and of_core_bool_desc env (desc : Core.boolean Core.desc) :
        Core.boolean Rel.wrapped_vec k =
      let shape = T.scalar size in
      match desc with
      | Core.True ->
          let vec = Vec.make shape (fun _ -> L.F64.one) in
          return (Rel.Bool { vec })
      | Core.False ->
          let vec = Vec.make shape (fun _ -> L.F64.zero) in
          return (Rel.Bool { vec })
      | Core.Index_eq (i1, i2) ->
          let* (Index i1) = of_core env i1 in
          let* (Index i2) = of_core env i2 in
          let vec =
            Vec.make shape (fun _ ->
                let open L in
                F64.select (I64.eq i1.idx i2.idx) F64.one F64.zero)
          in
          return (Rel.Bool { vec })
      | Core.And (l, r) ->
          let* (Bool { vec = l }) = of_core env l in
          let* (Bool { vec = r }) = of_core env r in
          let vec = Vec.unsafe_map2 L.F64.mul shape l r in
          return (Rel.Bool { vec })
      | Core.Or (l, r) ->
          let* (Bool { vec = l }) = of_core env l in
          let* (Bool { vec = r }) = of_core env r in
          let vec = Vec.unsafe_map2 L.F64.max shape l r in
          return (Rel.Bool { vec })
    and of_core_index_desc env (desc : Core.index Core.desc) :
        Core.index Rel.wrapped_vec k =
      return @@ Rel.Index { idx = env_assoc_desc env desc }
    in
    of_core [] body
*)
