open Tsr_shape

type ('shape, 'elt) tensor = Tensor_tag [@@ocaml.warning "-37"]

type index = int

type 'a shape = 'a Shape.t

(** A language sufficiently expressive to express its own gradients. *)

type 'elt elt_kind = Float : float elt_kind | Bool : bool elt_kind

type _ desc =
  (* Vector values *)
  | Var : ('a, float) tensor desc
  | Float : float -> ('a, float) tensor desc
  | Array : Ba.t -> (index, float) tensor desc
  (* Boolean values *)
  | True : ('a, bool) tensor desc
  | False : ('a, bool) tensor desc
  (* Vector expressions *)
  | Binop :
      ('a, 'ea, 'b, 'eb, 'c, 'ec) binop
      * ('a, 'ea) tensor t
      * ('b, 'eb) tensor t
      -> ('c, 'ec) tensor desc
  | Unop : ('a, 'ea, 'b, 'eb) unop * ('a, 'ea) tensor t -> ('b, 'eb) tensor desc
  | Get : ('a, 'elt) tensor t * 'a -> (index, 'elt) tensor desc
  | Cond :
      ('a, bool) tensor t * ('a, 'elt) tensor t * ('a, 'elt) tensor t
      -> ('a, 'elt) tensor desc
  | Set_slice :
      { operand : ('a, float) tensor t;
        update : ('b, float) tensor t;
        slice : ('a, 'b) Shape.slice
      }
      -> ('a, float) tensor desc
  | Contract :
      ('a, float) tensor t * ('a, 'b) Shape.path
      -> ('b, float) tensor desc
  | Broadcast :
      ('a, 'elt) tensor t * ('b, 'a) Shape.path
      -> ('b, 'elt) tensor desc
  | Slice : ('a, 'elt) tensor t * ('a, 'b) Shape.slice -> ('b, 'elt) tensor desc
  | Iso : ('a, 'elt) tensor t * ('a, 'b) Shape.iso -> ('b, 'elt) tensor desc
  | Diagonal :
      ('base, float) tensor t * ('base, 'a) Shape.diagonal
      -> ('a, float) tensor desc

and (_, _, _, _, _, _) binop =
  | Add : ('a, float, 'a, float, 'a, float) binop
  | Sub : ('a, float, 'a, float, 'a, float) binop
  | Mul : ('a, float, 'a, float, 'a, float) binop
  | Div : ('a, float, 'a, float, 'a, float) binop
  | Matmul : ('a * 'b, float, 'b * 'c, float, 'a * 'c, float) binop
  | Dot : ('a, float, 'a, float, index, float) binop
  | And : ('a, bool, 'a, bool, 'a, bool) binop
  | Or : ('a, bool, 'a, bool, 'a, bool) binop

and (_, _, _, _) unop =
  | Neg : ('a, float, 'a, float) unop
  | Sqrt : ('a, float, 'a, float) unop
  | Recip : ('a, float, 'a, float) unop

and _ t =
  | S :
      { hash : int;
        tag : ('a, 'elt) tensor Type.Id.t;
        desc : ('a, 'elt) tensor desc;
        shape : 'a shape
      }
      -> ('a, 'elt) tensor t

type hashcons_key = Key : 'a t -> hashcons_key [@@ocaml.unboxed]

type packed = Packed : (_, _) tensor desc -> packed [@@ocaml.unboxed]

type 'a vec = ('a, float) tensor t

type 'a boolean = ('a, bool) tensor t

(* Getters *)

let get_hash : type a. a t -> int = fun (S { hash; _ }) -> hash

let get_tag : type a elt. (a, elt) tensor t -> (a, elt) tensor Type.Id.t =
 fun (S { tag; _ }) -> tag

let get_shape : type a b. (a, b) tensor t -> a shape =
 fun (S { shape; _ }) -> shape

let get_var : type a. a t -> a desc = fun (S { desc; _ }) -> desc

(* Pretty-printers *)

let rec pp : type a. Format.formatter -> a t -> unit =
 fun fmtr (expr : a t) ->
  match expr with
  | S { desc; shape; tag = _; hash = _ } -> pp_desc fmtr shape desc

and pp_desc :
    type a elt. Format.formatter -> a shape -> (a, elt) tensor desc -> unit =
 fun fmtr shape desc ->
  let open Format in
  match desc with
  | Var -> fprintf fmtr "var"
  | Float f -> fprintf fmtr "%f" f
  | Array ba -> fprintf fmtr "array[%d]" (Ba.length ba)
  | True -> fprintf fmtr "true"
  | False -> fprintf fmtr "false"
  | Binop (op, l, r) -> fprintf fmtr "(%a %a %a)" pp l pp_binop op pp r
  | Unop (op, e) -> fprintf fmtr "%a(%a)" pp_unop op pp e
  | Get (e, i) -> fprintf fmtr "(%a)[%a]" pp e (pp_pos (get_shape e)) i
  | Cond (c, ift, iff) -> fprintf fmtr "(%a)?(%a):(%a)" pp c pp ift pp iff
  | Set_slice { operand; update; slice } ->
      fprintf
        fmtr
        "set_slice(%a, %a, %a)"
        pp
        operand
        pp
        update
        (Shape.pp_slice ~shape:(get_shape operand) ())
        slice
  | Contract (tensor, path) ->
      fprintf fmtr "contract(%a,%a)" pp tensor Shape.pp_path path
  | Broadcast (vec, path) ->
      fprintf
        fmtr
        "broadcast(%a, %a, %a)"
        pp
        vec
        Shape.pp_path
        path
        Shape.pp
        shape
  | Slice (vec, slice) ->
      fprintf
        fmtr
        "slice(%a, %a)"
        pp
        vec
        (Shape.pp_slice ~shape:(get_shape vec) ())
        slice
  | Iso (vec, iso) -> fprintf fmtr "iso(%a, %a)" pp vec Shape.pp_iso iso
  | Diagonal (vec, d) -> fprintf fmtr "diag(%a, %a)" pp vec (Shape.pp_diag 1) d

and pp_binop :
    type a b c ea eb ec. Format.formatter -> (a, ea, b, eb, c, ec) binop -> unit
    =
 fun fmtr op ->
  let open Format in
  match op with
  | Add -> pp_print_string fmtr "+"
  | Sub -> pp_print_string fmtr "-"
  | Mul -> pp_print_string fmtr "*"
  | Div -> pp_print_string fmtr "/"
  | Matmul -> pp_print_string fmtr "@"
  | Dot -> pp_print_string fmtr "."
  | And -> pp_print_string fmtr "&&"
  | Or -> pp_print_string fmtr "||"

and pp_unop : type a ea b eb. Format.formatter -> (a, ea, b, eb) unop -> unit =
 fun fmtr op ->
  let open Format in
  match op with
  | Neg -> pp_print_string fmtr "neg"
  | Sqrt -> pp_print_string fmtr "sqrt"
  | Recip -> pp_print_string fmtr "recip"

and pp_pos : type a. a shape -> Format.formatter -> a -> unit =
 fun shape fmtr index -> Shape.pp_index ~shape () fmtr index

(* Helpers *)

let binop_elt_type :
    type a ea b eb c ec. (a, ea, b, eb, c, ec) binop -> ec elt_kind =
 fun op ->
  match op with
  | Add -> Float
  | Sub -> Float
  | Mul -> Float
  | Div -> Float
  | Matmul -> Float
  | Dot -> Float
  | And -> Bool
  | Or -> Bool

(* Errors *)

type Error_monad.error += Construction_error of packed

let () =
  Error_monad.register
    (function Construction_error node -> Some node | _ -> None)
    (fun fmtr (Packed node) -> pp_desc fmtr (Shape.create ()) node)

let trace_construction_error desc k =
  Error_monad.trace (fun () -> Construction_error (Packed desc)) k

(*  Misc *)

let equal_elt_kind : type a b. a elt_kind -> b elt_kind -> (a, b) Type.eq option
    =
 fun k1 k2 ->
  match (k1, k2) with
  | (Float, Float) -> Some Type.Equal
  | (Bool, Bool) -> Some Type.Equal
  | (Float, _) -> None
  | (Bool, _) -> None

(* Hashing  *)

let hash = Hashtbl.hash

let hash_binop : type a ea b eb c ec. (a, ea, b, eb, c, ec) binop -> int =
 fun binop -> hash binop

let hash_unop : type a ea b ec. (a, ea, b, ec) unop -> int =
 fun unop -> hash unop

let hash_index : type a. a Shape.t -> a -> int =
 fun _shape i ->
  (* The index is a tuple of integers and can be hashed safely, even if the shape is undefined. *)
  hash i

let hash_slice : type a b. (a, b) Shape.slice -> int = fun slice -> hash slice

let hash_path : type a b. (a, b) Shape.path -> int = fun path -> hash path

let hash_iso : type a b. (a, b) Shape.iso -> int = fun iso -> hash iso

let hash_diagonal diag =
  let rec loop : type a b. int -> (a, b) Shape.diagonal -> int =
   fun depth diag ->
    match diag with
    | Diag_base shape -> Shape.uid shape |> Type.Id.uid
    | Diag_cons diag -> hash (depth, loop (depth + 1) diag)
  in
  loop 0 diag

let hash_desc : type a. a desc -> int =
 fun desc ->
  match desc with
  | Var -> hash desc
  | Float f -> Float.hash f
  | Array ba -> Ba.hash ba
  | True -> hash desc
  | False -> hash desc
  | Binop (op, l, r) -> hash (hash_binop op, get_hash l, get_hash r)
  | Unop (op, t) -> hash (hash_unop op, get_hash t)
  | Get (t, i) -> hash (get_hash t, hash_index (get_shape t) i)
  | Cond (cond, ift, iff) -> hash (get_hash cond, get_hash ift, get_hash iff)
  | Set_slice { operand; update; slice } ->
      hash (get_hash operand, get_hash update, hash_slice slice)
  | Contract (t, p) -> hash (get_hash t, hash_path p)
  | Broadcast (t, p) -> hash (get_hash t, hash_path p)
  | Slice (t, s) -> hash (get_hash t, hash_slice s)
  | Iso (t, i) -> hash (get_hash t, hash_iso i)
  | Diagonal (t, diag) -> hash (get_hash t, hash_diagonal diag)

let equal_binop :
    type a ea b eb c elt1 elt2.
    (a, ea, b, eb, c, elt1) binop ->
    (a, ea, b, eb, c, elt2) binop ->
    (elt1, elt2) Type.eq option =
 fun op1 op2 ->
  match (op1, op2) with
  | (Add, Add) -> Some Type.Equal
  | (Sub, Sub) -> Some Type.Equal
  | (Mul, Mul) -> Some Type.Equal
  | (Div, Div) -> Some Type.Equal
  | (Matmul, Matmul) -> Some Type.Equal
  | (Dot, Dot) -> Some Type.Equal
  | (And, And) -> Some Type.Equal
  | (Or, Or) -> Some Type.Equal
  | (Add, _) -> None
  | (Sub, _) -> None
  | (Mul, _) -> None
  | (Div, _) -> None
  | (Matmul, _) -> None
  | (Dot, _) -> None
  | (And, _) -> None
  | (Or, _) -> None

let equal_unop :
    type a ea b elt1 elt2.
    (a, ea, b, elt1) unop ->
    (a, ea, b, elt2) unop ->
    (elt1, elt2) Type.eq option =
 fun op1 op2 ->
  match (op1, op2) with
  | (Neg, Neg) -> Some Type.Equal
  | (Sqrt, Sqrt) -> Some Type.Equal
  | (Recip, Recip) -> Some Type.Equal
  | (Neg, _) -> None
  | (Sqrt, _) -> None
  | (Recip, _) -> None

let equal_desc :
    type a elt1 elt2.
    (a, elt1) tensor desc ->
    (a, elt2) tensor desc ->
    ((a, elt1) tensor, (a, elt2) tensor) Type.eq option =
 fun desc1 desc2 ->
  match (desc1, desc2) with
  | (Var, Var) -> Some Type.Equal
  | (Float f1, Float f2) -> if Float.equal f1 f2 then Some Type.Equal else None
  | (Array ba1, Array ba2) -> if Ba.equal ba1 ba2 then Some Type.Equal else None
  | (True, True) -> Some Type.Equal
  | (False, False) -> Some Type.Equal
  | (Binop (op1, l1, r1), Binop (op2, l2, r2)) -> (
      match
        ( Type.Id.provably_equal (get_tag l1) (get_tag l2),
          Type.Id.provably_equal (get_tag r1) (get_tag r2) )
      with
      | (Some Type.Equal, Some Type.Equal) -> (
          match equal_binop op1 op2 with
          | Some Type.Equal -> Some Type.Equal
          | None -> None)
      | (None, _) | (_, None) -> None)
  | (Unop (op1, t1), Unop (op2, t2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> (
          match equal_unop op1 op2 with
          | Some Type.Equal -> Some Type.Equal
          | None -> None)
      | None -> None)
  | (Get (t1, i1), Get (t2, i2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal -> if i1 = i2 then Some Type.Equal else None
      | None -> None)
  | (Cond (cond1, ift1, iff1), Cond (cond2, ift2, iff2)) -> (
      match
        ( Type.Id.provably_equal (get_tag cond1) (get_tag cond2),
          Type.Id.provably_equal (get_tag ift1) (get_tag ift2),
          Type.Id.provably_equal (get_tag iff1) (get_tag iff2) )
      with
      | (Some Type.Equal, Some Type.Equal, Some Type.Equal) -> Some Type.Equal
      | _ -> None)
  | ( Set_slice { operand = op1; update = up1; slice = sl1 },
      Set_slice { operand = op2; update = up2; slice = sl2 } ) -> (
      match
        ( Type.Id.provably_equal (get_tag op1) (get_tag op2),
          Type.Id.provably_equal (get_tag up1) (get_tag up2) )
      with
      | (Some Type.Equal, Some Type.Equal) ->
          (* FIXME: we could avoid using polymorphic equality if we had the guarantee that shapes are fully defined,
             or if we made that assumption. Same for `Get` and `Slice` nodes. *)
          if Shape.equal_slice sl1 sl2 then Some Type.Equal else None
      | _ -> None)
  | (Contract (t1, p1), Contract (t2, p2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal ->
          if Shape.equal_path p1 p2 then Some Type.Equal else None
      | None -> None)
  | (Broadcast (t1, p1), Broadcast (t2, p2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal ->
          if Shape.equal_path p1 p2 then Some Type.Equal else None
      | None -> None)
  | (Slice (t1, sl1), Slice (t2, sl2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal ->
          if Shape.equal_slice sl1 sl2 then Some Type.Equal else None
      | None -> None)
  | (Iso (t1, iso1), Iso (t2, iso2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal ->
          if Shape.equal_iso iso1 iso2 then Some Type.Equal else None
      | None -> None)
  | (Diagonal (t1, diag1), Diagonal (t2, diag2)) -> (
      match Type.Id.provably_equal (get_tag t1) (get_tag t2) with
      | Some Type.Equal ->
          if Shape.equal_diag diag1 diag2 then Some Type.Equal else None
      | None -> None)
  | (Var, _)
  | (Float _, _)
  | (Array _, _)
  | (True, _)
  | (False, _)
  | (Binop _, _)
  | (Unop _, _)
  | (Get _, _)
  | (Cond _, _)
  | (Set_slice _, _)
  | (Contract _, _)
  | (Broadcast _, _)
  | (Slice _, _)
  | (Iso _, _)
  | (Diagonal _, _) ->
      None

let equal_key (Key (S { shape = shape1; desc = desc1; _ }))
    (Key (S { shape = shape2; desc = desc2; _ })) =
  match Type.Id.provably_equal (Shape.uid shape1) (Shape.uid shape2) with
  | Some Type.Equal -> (
      match equal_desc desc1 desc2 with
      | Some Type.Equal -> true
      | None -> false)
  | None -> false

let hash_key (Key (S { shape; desc; _ })) =
  hash (Shape.uid shape, hash_desc desc)

(* Smart constructors *)

module Table = Weak.Make (struct
  type t = hashcons_key

  let equal = equal_key

  let hash = hash_key
end)

let table = Table.create 1023

let make : type a elt. a Shape.t -> (a, elt) tensor desc -> (a, elt) tensor t =
 fun shape desc ->
  let hash = hash (Shape.uid shape, hash_desc desc) in
  let node = S { shape; desc; tag = Type.Id.make (); hash } in
  let key = Key node in
  match Table.find_opt table key with
  | None ->
      Table.add table key ;
      node
  | Some (Key (S { shape = shape'; desc = desc'; _ } as node)) -> (
      match Type.Id.provably_equal (Shape.uid shape) (Shape.uid shape') with
      | Some Type.Equal -> (
          match equal_desc desc desc' with
          | Some Type.Equal -> node
          | None -> assert false)
      | None -> assert false)

let var shape = make shape Var

let float shape f = make shape (Float f)

let zero shape = make shape (Float 0.0)

let one shape = make shape (Float 1.0)

let array ba =
  let shape = Shape.rank_one (Ba.length ba) in
  make shape (Array ba)

let true_ shape = make shape True

let false_ shape = make shape False

(* TODO: would be nice to be able to express that a shape
   is rank one with unknown dimension *)

let shape_join x y = Shape.join x y |> Shape_error.lift

let assert_shape_equal ?loc shape1 shape2 =
  Shape.assert_equal shape1 shape2
  |> Error_monad.map_error (fun err -> [Shape_error.Shape_error { loc; err }])

let shape_constraints (type a ea b eb c ec) ~__LOC__
    (op : (a, ea, b, eb, c, ec) binop) (x : (a, ea) tensor t)
    (y : (b, eb) tensor t) : c shape Error_monad.t =
  let open Error_monad in
  match op with
  | Add ->
      let xs = get_shape x in
      let+ () = shape_join xs (get_shape y) in
      xs
  | Sub ->
      let xs = get_shape x in
      let+ () = shape_join xs (get_shape y) in
      xs
  | Mul ->
      let xs = get_shape x in
      let+ () = shape_join xs (get_shape y) in
      xs
  | Div ->
      let xs = get_shape x in
      let+ () = shape_join xs (get_shape y) in
      xs
  | Matmul ->
      let x_shape = get_shape x in
      let y_shape = get_shape y in
      let input_shape = Shape.create () in
      let mid_shape = Shape.create () in
      let output_shape = Shape.create () in
      let* () = shape_join y_shape (Shape.tensor mid_shape input_shape) in
      let+ () = shape_join x_shape (Shape.tensor output_shape mid_shape) in
      Shape.tensor output_shape input_shape
  | Dot ->
      let+ () = shape_join (get_shape x) (get_shape y) in
      Shape.scalar
  | And ->
      let xs = get_shape x in
      let+ () = shape_join xs (get_shape y) in
      xs
  | Or ->
      let xs = get_shape x in
      let+ () = shape_join xs (get_shape y) in
      xs

let unop_shape_constraints (type a ea b elt) (op : (a, ea, b, elt) unop)
    (x : (a, ea) tensor t) : b shape Error_monad.t =
  let open Error_monad in
  match op with
  | Neg -> return (get_shape x)
  | Sqrt -> return (get_shape x)
  | Recip -> return (get_shape x)

let binop ~__LOC__ op x y =
  let open Error_monad in
  let desc = Binop (op, x, y) in
  trace_construction_error desc @@ fun () ->
  let+ shape = shape_constraints ~__LOC__ op x y in
  make shape desc

let add x y = binop ~__LOC__ Add x y

let sub x y = binop ~__LOC__ Sub x y

let mul x y = binop ~__LOC__ Mul x y

let div x y = binop ~__LOC__ Div x y

let mm x y = binop ~__LOC__ Matmul x y

let dot x y = binop ~__LOC__ Dot x y

let and_ x y = binop ~__LOC__ And x y

let or_ x y = binop ~__LOC__ Or x y

let unop :
    type a ea b eb.
    (a, ea, b, eb) unop -> (a, ea) tensor t -> (b, eb) tensor t Error_monad.t =
 fun op x ->
  let open Error_monad in
  let+ shape = unop_shape_constraints op x in
  make shape (Unop (op, x))

let neg x = unop Neg x

let sqrt x = unop Sqrt x

let recip x = unop Recip x

let get x i =
  (* TODO: i \in (shape x) *)
  make Shape.scalar (Get (x, i))

let cond c ift iff =
  let open Error_monad in
  let desc = Cond (c, ift, iff) in
  trace_construction_error desc @@ fun () ->
  let+ () = shape_join (get_shape ift) (get_shape iff) in
  make (get_shape ift) desc

let set_slice operand update slice =
  (* TODO: check [update] is smaller than [operand],
     check [indices] are in-shape for [operand] (same problem with [Dim]) *)
  make (get_shape operand) (Set_slice { operand; update; slice })

let contract v path =
  let open Error_monad in
  let desc = Contract (v, path) in
  trace_construction_error desc @@ fun () ->
  let+ shape = Shape.contract (get_shape v) path |> Shape_error.lift in
  make shape desc

let broadcast v shape path =
  let open Error_monad in
  let desc = Broadcast (v, path) in
  trace_construction_error desc @@ fun () ->
  let* contracted = Shape.contract shape path |> Shape_error.lift in
  let+ () = shape_join contracted (get_shape v) in
  make shape desc

let slice v slice =
  let open Error_monad in
  let desc = Slice (v, slice) in
  trace_construction_error desc @@ fun () ->
  let+ shape = Shape.of_slice (get_shape v) slice |> Shape_error.lift in
  make shape desc

let iso v iso =
  let open Error_monad in
  let desc = Iso (v, iso) in
  trace_construction_error desc @@ fun () ->
  let+ shape = Shape.apply_iso (get_shape v) iso |> Shape_error.lift in
  make shape desc

let lassoc v = iso v Shape.LAssoc

let rassoc v = iso v Shape.RAssoc

let transpose v = iso v Shape.Transpose

let lmap v i = iso v (Shape.Iso.lmap i)

let rmap v i = iso v (Shape.Iso.rmap i)

let diag v d =
  let (_, shape) = Shape.shape_of_diag d in
  make shape (Diagonal (v, d))

let identity shape = diag (float shape 1.0) (Diag_cons (Diag_base shape))

let rec subst_var : type a v. (v, float) tensor t -> a t -> a t Error_monad.t =
 fun e term ->
  let open Error_monad in
  match term with
  | S { desc; shape; _ } -> (
      match desc with
      | Var -> (
          let shape' = get_shape e in
          match Shape.assert_equal shape shape' with
          | Error err ->
              fail (Shape_error.Shape_error { loc = Some __LOC__; err })
          | Ok Type.Equal -> return e)
      | Float _ -> return term
      | Array _ -> return term
      | Binop (op, l, r) ->
          let* l = subst_var e l in
          let* r = subst_var e r in
          binop ~__LOC__ op l r
      | Unop (op, term) ->
          let* term = subst_var e term in
          unop op term
      | Get (term, i) ->
          (* TODO: subst_var in indices (useless at the time of writing but more
             consistent) *)
          Result.map (fun term -> get term i) (subst_var e term)
      | Cond (c, ift, iff) ->
          let* c = subst_var e c in
          let* ift = subst_var e ift in
          let* iff = subst_var e iff in
          cond c ift iff
      | Set_slice { operand; update; slice } ->
          (* TODO: subst_var in indices (useless at the time of writing but more
             consistent) *)
          let* operand = subst_var e operand in
          let+ update = subst_var e update in
          set_slice operand update slice
      | Contract (v, path) ->
          let* v = subst_var e v in
          contract v path
      | Broadcast (v, path) ->
          Result.bind (subst_var e v) @@ fun v -> broadcast v shape path
      | Slice (v, sl) ->
          let* v = subst_var e v in
          let* slice = slice v sl in
          return slice
      | Iso (v, i) ->
          let* v = subst_var e v in
          iso v i
      | Diagonal (v, d) ->
          let* v = subst_var e v in
          return (diag v d)
      | True -> return term
      | False -> return term)

module Dot = struct
  type graph =
    { nodes : (int, string) Hashtbl.t;
      edges : (int, (int * string) list) Hashtbl.t
    }

  let to_graph : type a elt. (a, elt) tensor t -> graph =
   fun node ->
    let open Format in
    let nodes = Hashtbl.create 101 in
    let edges = Hashtbl.create 101 in
    let rec loop : type a elt. (a, elt) tensor t -> unit =
     fun (S { hash = _; tag; desc; shape }) ->
      let tag = Type.Id.uid tag in
      let node s = Hashtbl.add nodes tag s in
      let edge_from : type a elt. ?label:string -> (a, elt) tensor t -> unit =
       fun ?(label = "") (S payload) ->
        let payload_tag = Type.Id.uid payload.tag in
        let neighbours =
          Hashtbl.find_opt edges payload_tag |> Option.value ~default:[]
        in
        Hashtbl.replace edges payload_tag ((tag, label) :: neighbours)
      in
      if Hashtbl.mem nodes tag then ()
      else
        match desc with
        | Var -> node (asprintf "var")
        | Float f -> node (asprintf "float(%f, %a)" f Shape.pp shape)
        | Array _ba -> node "array"
        | True -> node "true"
        | False -> node "false"
        | Binop (op, lhs, rhs) ->
            edge_from ~label:"lhs" lhs ;
            edge_from ~label:"rhs" rhs ;
            node (asprintf "%a" pp_binop op) ;
            loop lhs ;
            loop rhs
        | Unop (op, t) ->
            edge_from t ;
            node (asprintf "%a" pp_unop op) ;
            loop t
        | Get (t, i) ->
            edge_from t ;
            node (asprintf "get(%a)" (Shape.pp_index ~shape:(get_shape t) ()) i) ;
            loop t
        | Cond (cond, ift, iff) ->
            edge_from ~label:"cond" cond ;
            edge_from ~label:"ift" ift ;
            edge_from ~label:"iff" iff ;
            node "cond" ;
            loop cond ;
            loop ift ;
            loop iff
        | Set_slice { operand; update; slice } ->
            edge_from ~label:"operand" operand ;
            edge_from ~label:"update" update ;
            node (asprintf "set_slice(%a)" (Shape.pp_slice ~shape ()) slice) ;
            loop operand ;
            loop update
        | Contract (t, p) ->
            edge_from t ;
            node (asprintf "contract(%a)" Shape.pp_path p) ;
            loop t
        | Broadcast (t, p) ->
            edge_from t ;
            node (asprintf "broadcast(%a)" Shape.pp_path p) ;
            loop t
        | Slice (t, sl) ->
            edge_from t ;
            node
              (asprintf "slice(%a)" (Shape.pp_slice ~shape:(get_shape t) ()) sl) ;
            loop t
        | Iso (t, is) ->
            edge_from t ;
            node (asprintf "iso(%a)" Shape.pp_iso is) ;
            loop t
        | Diagonal (t, diag) ->
            edge_from t ;
            node (asprintf "diagonal(%a)" (Shape.pp_diag 0) diag) ;
            loop t
    in
    loop node ;
    { edges; nodes }

  let to_dot oc graph =
    let fmtr = Format.formatter_of_out_channel oc in
    let pp_nodes fmtr nodes =
      Hashtbl.iter
        (fun tag name ->
          Format.fprintf fmtr "%d [label=\"%s\"; shape=box]\n" tag name)
        nodes
    in
    let pp_edges fmtr edges =
      Hashtbl.iter
        (fun tag neighbours ->
          List.iter
            (fun (tag', label) ->
              Format.fprintf fmtr "%d -> %d [label = \"%s\"]\n" tag tag' label)
            neighbours)
        edges
    in
    Format.fprintf
      fmtr
      "digraph { @.%a @. %a @.}%!"
      pp_nodes
      graph.nodes
      pp_edges
      graph.edges
end
