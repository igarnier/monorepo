open Bigarray
open Array1

type nonrec t = (float, float64_elt, c_layout) t

let create dim : t = create float64 c_layout dim

let make dim x : t = init float64 c_layout dim (fun _ -> x)

let length (arr : t) = dim arr

let of_array (arr : float array) : t = of_array float64 c_layout arr

let to_array (ba : t) : float array = Array.init (dim ba) (unsafe_get ba)

let hash = Hashtbl.hash

let equal (arr1 : t) (arr2 : t) =
  let dim1 = dim arr1 in
  let dim2 = dim arr2 in
  if dim1 <> dim2 then false
  else
    let exception Exit in
    try
      for i = 0 to dim1 - 1 do
        let x = unsafe_get arr1 i in
        let y = unsafe_get arr2 i in
        if x <> y then raise Exit
      done ;
      true
    with Exit -> false

let map f (arr : t) =
  let dim = dim arr in
  let res = create dim in
  for i = 0 to dim - 1 do
    unsafe_set res i (f (unsafe_get arr i))
  done ;
  res

let map2 f (arr1 : t) (arr2 : t) =
  let dim1 = dim arr1 in
  let dim2 = dim arr2 in
  if dim1 <> dim2 then invalid_arg "Ba.map2" ;
  let res = create dim1 in
  for i = 0 to dim1 - 1 do
    unsafe_set res i (f (unsafe_get arr1 i) (unsafe_get arr2 i))
  done ;
  res

let add x y = map2 ( +. ) x y

let sub x y = map2 ( -. ) x y

let mul x y = map2 ( *. ) x y

let neg x = map Float.neg x
