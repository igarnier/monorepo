open Interp
open Bigarray

let%expect_test "iter_sub_shape" =
  let shape = [| `loop 4; `loop 3; `fixed 42; `loop 2 |] in
  iter_sub_shape shape (fun i ->
      Format.printf
        "[%a]@."
        (Format.pp_print_list
           ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "; ")
           Format.pp_print_int)
        (Array.to_list i)) ;
  [%expect
    {|
    [0; 0; 42; 0]
    [0; 0; 42; 1]
    [0; 1; 42; 0]
    [0; 1; 42; 1]
    [0; 2; 42; 0]
    [0; 2; 42; 1]
    [1; 0; 42; 0]
    [1; 0; 42; 1]
    [1; 1; 42; 0]
    [1; 1; 42; 1]
    [1; 2; 42; 0]
    [1; 2; 42; 1]
    [2; 0; 42; 0]
    [2; 0; 42; 1]
    [2; 1; 42; 0]
    [2; 1; 42; 1]
    [2; 2; 42; 0]
    [2; 2; 42; 1]
    [3; 0; 42; 0]
    [3; 0; 42; 1]
    [3; 1; 42; 0]
    [3; 1; 42; 1]
    [3; 2; 42; 0]
    [3; 2; 42; 1] |}]

let%expect_test "iter_shape" =
  let shape = [| 4; 3; 2 |] in
  iter_shape shape (fun i ->
      Format.printf
        "[%a]@."
        (Format.pp_print_list
           ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "; ")
           Format.pp_print_int)
        (Array.to_list i)) ;
  [%expect
    {|
    [0; 0; 0]
    [0; 0; 1]
    [0; 1; 0]
    [0; 1; 1]
    [0; 2; 0]
    [0; 2; 1]
    [1; 0; 0]
    [1; 0; 1]
    [1; 1; 0]
    [1; 1; 1]
    [1; 2; 0]
    [1; 2; 1]
    [2; 0; 0]
    [2; 0; 1]
    [2; 1; 0]
    [2; 1; 1]
    [2; 2; 0]
    [2; 2; 1]
    [3; 0; 0]
    [3; 0; 1]
    [3; 1; 0]
    [3; 1; 1]
    [3; 2; 0]
    [3; 2; 1] |}]

let eval_term term input =
  let open Error_monad in
  let input = dematerialize (Float input) in
  let* flat = Flat.flatten term in
  let* output = interp flat input in
  let (Float result) = rematerialize output in
  return result

let check term inputs =
  List.iter
    (fun i ->
      let i = Helpers.Sexp.to_bigarray i in
      let o_eval = eval_term term i |> Error_monad.run in
      Format.printf
        "@[<v>input=  %a@,output= %a@,@]@."
        Helpers.pp_float_genarray
        i
        Helpers.pp_float_genarray
        o_eval)
    inputs

let check_against_oracle oracle term inputs =
  List.iter
    (fun i ->
      let i = Helpers.Sexp.to_bigarray i in
      let o_oracle = oracle i in
      let o_eval = eval_term term i |> Error_monad.run in
      Format.printf
        "@[<v>input=  %a@,output= %a@,expect= %a@,@]@."
        Helpers.pp_float_genarray
        i
        Helpers.pp_float_genarray
        o_eval
        Helpers.pp_float_genarray
        o_oracle)
    inputs

let%expect_test "test_identity" =
  let open Tsr_shape in
  let term = Core.var Shape.scalar in
  check_against_oracle
    (fun x -> x)
    term
    Helpers.Sexp.[scalar ~-.1.0; scalar 0.0; scalar 1.0] ;
  [%expect
    {|
        input=  [-1.]
        output= [-1.]
        expect= [-1.]

        input=  [0.]
        output= [0.]
        expect= [0.]

        input=  [1.]
        output= [1.]
        expect= [1.] |}]

let%expect_test "test_array" =
  let term = Core.array (Ba.of_array [| 1.0; 2.0; 3.0 |]) in
  check_against_oracle
    (fun _x ->
      let open Helpers.Sexp in
      array [| 1.0; 2.0; 3.0 |] |> to_bigarray)
    term
    (List.map Helpers.Sexp.scalar [0.0]) ;
  [%expect {|
    input=  [0.]
    output= [1.2.3.]

    expect= [1.2.3.] |}]

let%expect_test "test_incr" =
  let open Tsr_shape in
  let term =
    let var = Core.var Shape.scalar in
    Core.add var (Core.float Shape.scalar 1.0) |> Error_monad.run
  in
  check_against_oracle
    (fun x ->
      let open Helpers.Sexp in
      scalar (Genarray.get x [| 0 |] +. 1.0) |> to_bigarray)
    term
    (List.map Helpers.Sexp.scalar [1.0; 2.0; 3.0; 10.0]) ;
  [%expect
    {|
       input=  [1.]
       output= [2.]
       expect= [2.]

       input=  [2.]
       output= [3.]
       expect= [3.]

       input=  [3.]
       output= [4.]
       expect= [4.]

       input=  [10.]
       output= [11.]
       expect= [11.] |}]

let%expect_test "test_square" =
  let open Tsr_shape in
  let term =
    let var = Core.var Shape.scalar in
    Core.mul var var |> Error_monad.run
  in
  check_against_oracle
    (fun x ->
      let open Helpers.Sexp in
      scalar (Genarray.get x [| 0 |] *. Genarray.get x [| 0 |]) |> to_bigarray)
    term
    (List.map Helpers.Sexp.scalar [1.0; 2.0; 3.0; 10.0]) ;
  [%expect
    {|
       input=  [1.]
       output= [1.]
       expect= [1.]

       input=  [2.]
       output= [4.]
       expect= [4.]

       input=  [3.]
       output= [9.]
       expect= [9.]

       input=  [10.]
       output= [100.]
       expect= [100.] |}]

let%expect_test "test_dot" =
  let open Tsr_shape in
  let term =
    let var = Core.var Shape.scalar in
    Core.dot var var |> Error_monad.run
  in
  let array f = Helpers.Sexp.array (Array.make 3 f) in
  check term [array 0.0; array 1.0; array 2.0] ;
  [%expect
    {|
    input=  [0.0.0.]

    output= [0.]

    input=  [1.1.1.]

    output= [3.]

    input=  [2.2.2.]

    output= [12.] |}]

let const_square_matrix dim f =
  let open Helpers in
  let a = Sexp.array (Array.make dim f) in
  Sexp.A (Array.make dim a)

let const_diag_matrix dim f =
  let open Helpers in
  let a j = Sexp.array (Array.init dim (fun i -> if i <= j then f else 0.0)) in
  Sexp.A (Array.init dim a)

let%expect_test "test_mm_sq" =
  let open Tsr_shape in
  let open Helpers in
  let term =
    let var = Core.var Shape.(tensor (rank_one 3) (rank_one 3)) in
    Core.mm var var |> Error_monad.run
  in
  let matrix f =
    let a = Sexp.array (Array.make 3 f) in
    Sexp.A [| a; a; a |]
  in
  check term [matrix 0.0; matrix 1.0; matrix 2.0] ;
  [%expect
    {|
    input=  [[0.0.0.]
             [0.0.0.]
             [0.0.0.]]

    output= [[0.0.0.]
             [0.0.0.]
             [0.0.0.]]


    input=  [[1.1.1.]
             [1.1.1.]
             [1.1.1.]]

    output= [[3.3.3.]
             [3.3.3.]
             [3.3.3.]]


    input=  [[2.2.2.]
             [2.2.2.]
             [2.2.2.]]

    output= [[12.12.12.]
             [12.12.12.]
             [12.12.12.]] |}]

let%expect_test "test_mat_slice" =
  let open Tsr_shape in
  let open Helpers in
  let open Error_monad in
  let term =
    let var =
      Core.var Shape.(tensor (rank_one 2) (tensor (rank_one 3) (rank_one 3)))
    in
    let* mat1 = Core.slice var (Shape.Lset 0) in
    let* mat2 = Core.slice var (Shape.Lset 1) in
    Core.add mat1 mat2
  in
  let term = run term in
  let matrix f1 f2 =
    Sexp.A [| const_diag_matrix 3 f1; const_diag_matrix 3 f2 |]
  in
  check term [matrix 1.0 ~-.1.0; matrix 3.0 3.0] ;
  [%expect
    {|
    input=  [[[1.0.0.]
              [1.1.0.]
              [1.1.1.]]
             [[-1.0.0.]
              [-1.-1.0.]
              [-1.-1.-1.]]]

    output= [[0.0.0.]
             [0.0.0.]
             [0.0.0.]]


    input=  [[[3.0.0.]
              [3.3.0.]
              [3.3.3.]]
             [[3.0.0.]
              [3.3.0.]
              [3.3.3.]]]

    output= [[6.0.0.]
             [6.6.0.]
             [6.6.6.]] |}]

let%expect_test "test_contract" =
  let open Tsr_shape in
  let open Error_monad in
  let term =
    let var = Core.var Shape.(tensor (rank_one 3) (rank_one 3)) in
    Core.contract var Shape.Rcont
  in
  let term = run term in
  let matrix f = const_diag_matrix 3 f in
  check term [matrix 3.0] ;
  [%expect
    {|
    input=  [[3.0.0.]
             [3.3.0.]
             [3.3.3.]]

    output= [3.6.9.] |}]

let%expect_test "test_broadcast" =
  let open Tsr_shape in
  let open Error_monad in
  let term =
    let var = Core.var Shape.(rank_one 3) in
    Core.broadcast var Shape.(tensor (rank_one 3) (rank_one 3)) Shape.Lcont
  in
  let term = run term in
  check term [Helpers.Sexp.array [| 1.0; 2.0; 3.0 |]] ;
  [%expect
    {|
    input=  [1.2.3.]

    output= [[1.1.1.]
             [2.2.2.]
             [3.3.3.]] |}]
