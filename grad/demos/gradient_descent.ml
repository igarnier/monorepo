open Grad

let rec gradient_descent_s grad v0 eps rate =
  let gradient = grad v0 in
  let norm = abs_float gradient in
  Format.eprintf "at %f, gradient = %f, norm = %f@." v0 gradient norm ;
  if norm < eps then v0
  else
    let v0' = v0 -. (rate *. gradient) in
    gradient_descent_s grad v0' eps rate

let expr : Float.Scalar.t -> Float.Scalar.t =
 fun x ->
  let open Float.Scalar in
  let e = sub x (inj 10.) in
  mul e e

(* [fn] is the forward function corresponding to [expr], [diff] its gradient *)
let (fn, diff) : (float -> float) * (float -> float) = Float.generate_ss expr

let () = Format.printf "minimizing f(x) = (x-10)^2@."

let minimizer = gradient_descent_s diff 0.0 0.01 0.1

let () = Format.printf "approximate minimizer found at x = %f@." minimizer
