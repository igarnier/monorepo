open Linalg
module G = Grad.Float

(* ------------------------------------------------------------------------- *)
(* Identity *)
let id x = x

let (gen_id, did) = G.generate_ss id

let inputs = Array.to_list @@ Array.init 100 (fun i -> 0.1 *. float_of_int i)

let outputs_oracle = List.map (fun x -> x) inputs

let outputs_graph = List.map gen_id inputs

let outputs_oracle_deriv = List.map (fun _x -> 1.0) inputs

let outputs_deriv = List.map did inputs

let data x y =
  Plot.Data.of_list (List.map (fun (x, y) -> Plot.r2 x y) (List.combine x y))

let () =
  let open Plot in
  let target = png ~png_file:"basic1.png" () in
  run
    ~target
    exec
    (plot2
       ~xaxis:"x"
       ~yaxis:"y"
       ~title:"basic"
       [ Line.line_2d
           ~legend:"id-oracle"
           ~with_points:true
           ~points:(data inputs outputs_oracle)
           ();
         Line.line_2d ~legend:"id-graph" ~points:(data inputs outputs_graph) ();
         Line.line_2d
           ~legend:"d(id-oracle)"
           ~with_points:true
           ~points:(data inputs outputs_oracle_deriv)
           ();
         Line.line_2d ~legend:"id-graph" ~points:(data inputs outputs_deriv) ()
       ])

(* ------------------------------------------------------------------------- *)
(* x^3 *)

let cube x = G.Scalar.(mul x (mul x x))

let (gen_cube, dcube) = G.generate_ss cube

let inputs = Array.to_list @@ Array.init 100 (fun i -> 0.1 *. float_of_int i)

let outputs_oracle = List.map (fun x -> x *. x *. x) inputs

let outputs_graph = List.map gen_cube inputs

let outputs_oracle_deriv = List.map (fun x -> 3. *. x *. x) inputs

let outputs_deriv = List.map dcube inputs

let () =
  let open Plot in
  let target = png ~png_file:"basic2.png" () in
  run
    ~target
    exec
    (plot2
       ~xaxis:"x"
       ~yaxis:"y"
       ~title:"basic"
       [ Line.line_2d
           ~legend:"oracle"
           ~with_points:true
           ~points:(data inputs outputs_oracle)
           ();
         Line.line_2d ~legend:"graph" ~points:(data inputs outputs_graph) ();
         Line.line_2d
           ~legend:"d(oracle)"
           ~with_points:true
           ~points:(data inputs outputs_oracle_deriv)
           ();
         Line.line_2d ~legend:"id-graph" ~points:(data inputs outputs_deriv) ()
       ])

(* ------------------------------------------------------------------------- *)
(* sin/cos *)

let sin x =
  G.Scalar.of_generic
  @@ G.create
       ~tag:"sin"
       ~backward:[| G.Scalar.to_generic x |]
       ~transfer:(fun v -> Vec.Float.map sin v.(0))
       ~jacobian:(fun v ->
         let m = Mat.Float.of_row v.(0) in
         Mat.Float.map cos m)
       ~output_dim:G.Output_dim.one

let (forward, backward) = G.generate_ss sin

let inputs = Array.to_list @@ Array.init 100 (fun i -> 0.1 *. float_of_int i)

let outputs = List.map forward inputs

let outputs' = List.map backward inputs

let () =
  let open Plot in
  let target = png ~png_file:"basic3.png" () in
  run
    ~target
    exec
    (plot2
       ~xaxis:"x"
       ~yaxis:"y"
       ~title:"basic"
       [ Line.line_2d ~legend:"sin" ~points:(data inputs outputs) ();
         Line.line_2d ~legend:"cos" ~points:(data inputs outputs') () ])
