open Linalg
module T = Proptest

(* ------------------------------------------------------------------------- *)
(* Instantiate graph functor *)

module Native = Basic_impl.Lang
module State = Grad.Native_impl.Node_state
module State_storage = Grad.Native_impl.Node_state_storage
module A = Native.Make_array (Native.Rational)
module V = Linalg.Vec.Float
module G =
  Grad.Native_impl.Make
    (Native.Rational)
    (Native.Make_storage (Native.Rational))
    (A)

(* ------------------------------------------------------------------------- *)
(* Test scalar ring properties *)

module Scalar_ring_gen = struct
  include T.Generators.Make_ring_gen (G.Scalar)

  let equal (x : G.Scalar.t) (y : G.Scalar.t) =
    Q.equal (G.Scalar.prj x) (G.Scalar.prj y)

  let ( = ) = equal
end

module Scalar_ring_test =
  T.Algebra.Ring
    (struct
      let name = "Scalar ring"
    end)
    (Scalar_ring_gen)
    (G.Scalar)

module Vector_ring = struct
  type 'a m = 'a

  type t = G.Vector.t

  let zero = G.Vector.zero 10

  let one = G.Vector.one 10

  let add = G.Vector.add

  let sub = G.Vector.sub

  let mul = G.Vector.mul

  let neg = G.Vector.neg

  let of_int i =
    G.Vector.const
      (Vec.Rational.const (Linalg.Tensor.Int.rank_one 10) (Q.of_int i))
end

(* ------------------------------------------------------------------------- *)
(* Test vector ring properties *)

module Vector_ring_gen = struct
  include T.Generators.Make_ring_gen (Vector_ring)

  let equal (x : G.Vector.t) (y : G.Vector.t) =
    let x = G.Vector.prj x in
    let y = G.Vector.prj y in
    let exception Not_equal in
    try
      if Array.length x <> Array.length y then raise Not_equal
      else
        for i = 0 to Array.length x - 1 do
          if not (Q.equal x.(i) y.(i)) then raise Not_equal else ()
        done ;
      true
    with Not_equal -> false

  let ( = ) = equal
end

module Vector_ring_test =
  T.Algebra.Ring
    (struct
      let name = "Vector ring"
    end)
    (Vector_ring_gen)
    (Vector_ring)

(* ------------------------------------------------------------------------- *)
(* Test some simple derivatives *)

let square (x : G.Scalar.t) = G.Scalar.mul x x

let (_, square') = G.generate_ss square

let () =
  T.Generic.add_test
    "d(x^2) = 2x"
    Crowbar.(
      map [T.Generators.Q.gen] (fun x ->
          let open Q in
          equal (square' x) (mul (of_int 2) x)))

let cube (x : G.Scalar.t) = G.Scalar.(mul x (mul x x))

let (graph_cube, cube') = G.generate_ss cube

let () =
  T.Generic.add_test
    "x^3 = generated x^3"
    Crowbar.(
      map [T.Generators.Q.gen] (fun x ->
          Q.equal (graph_cube x) (Q.mul x (Q.mul x x))))

let () =
  T.Generic.add_test
    "d(x^3) = 3x^2"
    Crowbar.(
      map [T.Generators.Q.gen] (fun x ->
          let open Q in
          equal (cube' x) (mul (of_int 3) (mul x x))))

(* ------------------------------------------------------------------------- *)
(* Test some simple gradients *)

(* multiply components of 2d vector *)
let mul vec =
  let x = G.Vector.get vec 0 in
  let y = G.Vector.get vec 1 in
  G.Scalar.mul x y

let (mul_f, mul') = G.generate_vs ~input_dim:2 mul

let array_equal eq a1 a2 =
  Array.length a1 = Array.length a2 && Array.for_all2 eq a1 a2

let () =
  T.Generic.add_test
    "d(v[0] * v[1]) = (v[1], v[0])"
    Crowbar.(
      map [T.Generators.Q.gen; T.Generators.Q.gen] @@ fun x y ->
      let storage = [| Q.zero; Q.zero |] in
      mul' [| x; y |] storage ;
      array_equal Q.equal storage [| y; x |])

(* ------------------------------------------------------------------------- *)
(* Algebraic properties of derivatives *)

(* Cook a random function by sampling a free ring term and replacing 0 by
   the given variable during interpretation *)

let randf : (G.Scalar.t -> G.Scalar.t) Crowbar.gen =
  Crowbar.(
    map [T.Generators.Free_ring.gen] (fun term x ->
        Scalar_ring_gen.interpret ~zero:x term))

(* Linearity *)

let () =
  T.Generic.add_test
    "d(f+g) = df + dg"
    Crowbar.(
      dynamic_bind randf @@ fun f ->
      dynamic_bind randf @@ fun g ->
      let (_, df) = G.generate_ss f in
      let (_, dg) = G.generate_ss g in
      let f_plus_g x = G.Scalar.add (f x) (g x) in
      let (_, dfpg) = G.generate_ss f_plus_g in
      map [T.Generators.Q.gen] (fun x -> Q.equal (Q.add (df x) (dg x)) (dfpg x)))

(* Leibniz *)

let () =
  T.Generic.add_test
    "d(fg) = df g + f dg"
    Crowbar.(
      dynamic_bind randf @@ fun f ->
      dynamic_bind randf @@ fun g ->
      let (f_, df) = G.generate_ss f in
      let (g_, dg) = G.generate_ss g in
      let fg x = G.Scalar.mul (f x) (g x) in
      let (_, dfg) = G.generate_ss fg in
      map [T.Generators.Q.gen] (fun x ->
          let open Q in
          equal (add (mul (df x) (g_ x)) (mul (f_ x) (dg x))) (dfg x)))
