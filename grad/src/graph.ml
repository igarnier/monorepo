(** Core graph construction.

    This modules allows to construct so-called "computational graphs".
    Nodes of these graphs encode differentiable functions of type
    [R]{^ [m1]} [x] [R]{^ [m2]} ... [x] [R]{^ [mk]} [->] [R]{^ [n]}.
    The graphs themselves represent composition of these functions.

    The library allows to efficently compute the jacobian associated to
    such graphs using backward-mode algorithmic differentiation (AD).

    A specificity of this module is that the implementation is parameterized
    by the target language, i.e. it is possible to not only {i run} code
    but also to {i generate} code, provided a suitable implementation
    for the (many) parameters of the [Make] functor is provided. *)

(** Nodes are evaluated on a call-by-need basis: each node stores are
    flag of type [state] indicating whether its current value has been
    invalidated or not. *)
type state = Invalid | Valid

(** [Invalid_output_dimension_specification] is raised when the specification of the
    output dimension of a node is invalid. *)
exception Invalid_output_dimension_specification

(** Generic backward-mode jacobian computation. *)
module Make
    (Repr : Basic_intf.Lang.Empty)
    (Monad : Basic_intf.Codegen_monad with type 'a m = 'a Repr.m)
    (T : Linalg.Intf.Tensor
           with type 'a m = 'a Repr.m
            and type 'a k = 'a Monad.t)
    (B : Basic_intf.Lang.Bool with type 'a m = 'a Repr.m)
    (R : Basic_intf.Lang.Ring with type 'a m = 'a Repr.m)
    (R_storage : Basic_intf.Lang.Storage
                   with type 'a m = 'a Repr.m
                    and type elt = R.t)
    (S_enum : Basic_intf.Lang.Enum with type 'a m = 'a Repr.m and type t = state)
    (S_st : Basic_intf.Lang.Storage
              with type 'a m = 'a Repr.m
               and type elt = state)
    (I_ring : Basic_intf.Lang.Ring with type 'a m = 'a Repr.m and type t = T.pos)
    (I_ord : Basic_intf.Lang.Infix_order
               with type 'a m = 'a Repr.m
                and type t = T.pos)
    (I_cst : Basic_intf.Lang.Const with type 'a m = 'a Repr.m and type t = T.pos)
    (E : Basic_intf.Lang.Exn with type 'a m = 'a Repr.m)
    (M : Basic_intf.Lang.Sequencing with type 'a m = 'a Repr.m)
    (L : Basic_intf.Lang.Lambda with type 'a m = 'a Repr.m)
    (P : Basic_intf.Lang.Product with type 'a m = 'a Repr.m)
    (A : Basic_intf.Lang.Array
           with type 'a m = 'a Repr.m
            and type index = T.pos
            and type elt = R.t) :
  Grad_intf.S
    with type 'a k = 'a Monad.t
     and type 'a m = 'a Repr.m
     and type 'a shape = 'a T.t
     and type elt = R.t
     and type index = T.pos
     and type arr = A.t = struct
  type 'a k = 'a Monad.t

  type 'a m = 'a Repr.m

  type 'a shape = 'a T.t

  type elt = R.t

  type index = T.pos

  type arr = A.t

  open Linalg

  module Vec = struct
    include Vec.Make (Repr) (Monad) (T) (B) (R) (R_storage) (E) (M)
    include Linalg.Vec.Array_backed (Repr) (Monad) (T) (A)
  end

  module Mat = struct
    include Mat.Make (Repr) (Monad) (T) (B) (R) (R_storage) (I_ring) (E) (M) (P)
    include
      Linalg.Mat.Array_backed_column_major (Repr) (Monad) (T) (B) (I_ring)
        (I_ord)
        (A)
        (E)
        (P)
        (M)
  end

  type vec = index Vec.t

  type ovec = index Vec.out

  type mat = (index * index) Mat.t

  type omat = (index * index) Mat.out

  open Monad.Infix

  module Node_kernel : sig
    type 'a t =
      | Vec_kernel :
          { transfer : vec array -> vec Monad.t;
            jacobian : vec array -> mat Monad.t;
            output_dim : index m;
            uid : int
          }
          -> vec t
      | Arr_kernel :
          { transfer : inp:arr m array -> out:arr m -> unit m Monad.t;
            jacobian : arr m array -> mat Monad.t;
            output_dim : index m;
            uid : int
          }
          -> arr m t

    val create_vec :
      transfer:(vec array -> vec Monad.t) ->
      jacobian:(vec array -> mat Monad.t) ->
      output_dim:index m ->
      vec t

    val create_arr :
      transfer:(inp:arr m array -> out:arr m -> unit m Monad.t) ->
      jacobian:(arr m array -> mat Monad.t) ->
      output_dim:index m ->
      arr m t

    val output_dim : _ t -> index m
  end = struct
    type 'a t =
      | Vec_kernel :
          { transfer : vec array -> vec Monad.t;
            jacobian : vec array -> mat Monad.t;
            output_dim : index m;
            uid : int
          }
          -> vec t
      | Arr_kernel :
          { transfer : inp:arr m array -> out:arr m -> unit m Monad.t;
            jacobian : arr m array -> mat Monad.t;
            output_dim : index m;
            uid : int
          }
          -> arr m t

    let fresh =
      let x = ref 0 in
      fun () ->
        let v = !x in
        incr x ;
        v

    let create_vec ~transfer ~jacobian ~output_dim =
      Vec_kernel { transfer; jacobian; output_dim; uid = fresh () }

    let create_arr ~transfer ~jacobian ~output_dim =
      Arr_kernel { transfer; jacobian; output_dim; uid = fresh () }

    let output_dim : type a. a t -> index m = function
      | Vec_kernel { output_dim; _ } -> output_dim
      | Arr_kernel { output_dim; _ } -> output_dim
  end

  let iter_array (array : 'a array) (body : 'a -> unit Repr.m) =
    Array.fold_left (fun acc x -> M.seq acc @@ fun () -> body x) M.unit array

  let state_dispatch st (f : state -> 'a m Monad.t) =
    Monad.return (S_enum.dispatch st (fun st -> Monad.run (f st)))

  let lambda code = L.lam (fun x -> Monad.run (code x))

  module Globals : sig
    val register_invalidate_value : (S_st.t -> unit) m -> unit

    val invalidate_value : unit -> (S_st.t -> unit) m
  end = struct
    let invalidate_value = ref None

    let register_invalidate_value f = invalidate_value := Some f

    let invalidate_value () =
      match !invalidate_value with None -> assert false | Some f -> f
  end

  module Node : sig
    type t

    val create_vec :
      tag:string -> backward:t array -> kernel:vec Node_kernel.t -> t

    val create_arr :
      tag:string -> backward:t array -> kernel:arr m Node_kernel.t -> t

    val is_initialized : t -> bool

    val is_accumulator_allocated : t -> bool

    val output_dim : t -> index m

    val state : t -> S_st.t m

    val value_storage : t -> arr m

    val value_i : t -> vec

    val value_o : t -> ovec

    val update_value : t -> (unit -> unit) m

    val id : t -> int

    val tag : t -> string

    val back : t -> t array

    val fwd : t -> t list

    val set_accumulator : t -> arr m * mat * omat -> unit

    val accumulator : t -> arr m * mat * omat

    val counter : t -> int

    val set_counter : t -> int -> unit

    val backprop : t -> (unit -> unit) m

    val construct_backprop : t -> unit

    val initialize : t -> unit Monad.t
  end = struct
    let id_gen =
      let x = ref 0 in
      fun () ->
        let v = !x in
        incr x ;
        v

    type kernel = Ex_kernel : 'a Node_kernel.t -> kernel [@@unboxed]

    type t =
      { mutable state : S_st.t m option;
        mutable value_storage : A.t m option;
        mutable value_i : vec option;
        mutable value_o : ovec option;
        mutable update_value : (unit -> unit) m option;
        mutable backprop : (unit -> unit) m option;
        mutable accumulator : (A.t Repr.m * mat * omat) option;
        back : t array;
        mutable fwd : t list;
        kernel : kernel;
        tag : string;
        id : int;
        mutable counter : int
      }

    let create ~tag ~backward ~kernel =
      let node =
        { state = None;
          value_storage = None;
          value_i = None;
          value_o = None;
          update_value = None;
          backprop = None;
          accumulator = None;
          back = backward;
          fwd = [];
          kernel;
          tag;
          id = id_gen ();
          counter = 0
        }
      in
      Array.iter (fun back -> back.fwd <- node :: back.fwd) backward ;
      node

    let create ~tag ~backward ~kernel =
      create ~tag ~backward ~kernel:(Ex_kernel kernel)

    let create_vec = create

    let create_arr = create

    let is_initialized node = Option.is_some node.state

    let is_accumulator_allocated node = Option.is_some node.accumulator

    let state node =
      match node.state with
      | None ->
          Format.kasprintf
            invalid_arg
            "state (%s, %d): not set"
            node.tag
            node.id
      | Some state -> state

    let value_storage node =
      match node.value_storage with
      | None ->
          Format.kasprintf
            invalid_arg
            "value_storage (%s, %d): not set"
            node.tag
            node.id
      | Some value_storage -> value_storage

    let value_i node =
      match node.value_i with
      | None -> invalid_arg "value_i: not set"
      | Some value_i -> value_i

    let value_o node =
      match node.value_o with
      | None -> invalid_arg "value_o: not set"
      | Some value_o -> value_o

    let update_value node =
      match node.update_value with
      | None -> invalid_arg "update_value: not set"
      | Some update_value -> update_value

    let id node = node.id

    let tag node = node.tag

    let output_dim node =
      match node.kernel with Ex_kernel k -> Node_kernel.output_dim k

    let back node = node.back

    let fwd node = node.fwd

    let set_accumulator node accumulator =
      match node.accumulator with
      | None -> node.accumulator <- Some accumulator
      | Some _ -> invalid_arg "set_accumulator: already set"

    let accumulator node =
      match node.accumulator with
      | None -> invalid_arg "accumulator: not set"
      | Some accumulator -> accumulator

    let counter node = node.counter

    let set_counter node c = node.counter <- c

    let backprop node =
      match node.backprop with
      | None -> invalid_arg "backprop: not set"
      | Some backprop -> backprop

    let update_value_generic state back do_transfer =
      lambda (fun _ ->
          let*! st = S_st.get state in
          state_dispatch st @@ function
          | Valid -> Monad.return M.unit
          | Invalid ->
              iter_array back (fun node -> L.app (update_value node) M.unit)
              >>! fun () ->
              do_transfer back >> fun () ->
              Monad.return (S_st.set state (S_enum.const Valid)))
      [@@inline]

    let update_value_vec state back value_o transfer =
      update_value_generic state back (fun back ->
          let* v = transfer (Array.map (fun node -> value_i node) back) in
          Vec.(value_o := v))
      [@@inline]

    let update_value_arr state back vstorage transfer =
      update_value_generic state back (fun back ->
          transfer ~inp:(Array.map value_storage back) ~out:vstorage)
      [@@inline]

    let initialize node =
      let*! state =
        let initial = S_enum.const Invalid in
        S_st.create initial
      in
      let*! value_storage = A.make (output_dim node) R.zero in
      let (value_i, value_o) = Vec.of_array value_storage in
      assert (Option.is_none node.state) ;
      node.value_i <- Some value_i ;
      node.value_o <- Some value_o ;
      node.value_storage <- Some value_storage ;
      node.state <- Some state ;
      let*! update_value =
        match node.kernel with
        | Ex_kernel (Node_kernel.Vec_kernel { transfer; _ }) ->
            update_value_vec state node.back value_o transfer
        | Ex_kernel (Node_kernel.Arr_kernel { transfer; _ }) ->
            update_value_arr state node.back value_storage transfer
      in
      node.update_value <- Some update_value ;
      Monad.return ()

    (* Restrict a matrix column dimension to the interval
       (inclusive) [c1;c2]. *)
    let col_restrict c1 c2 (Linalg.Intf.Vec (s, m) : mat) =
      let num_cols = I_ring.(add (sub c2 c1) one) in
      let shape = T.tensor (T.rank_one num_cols) (T.snd s) in
      let m index =
        let c = I_ring.add c1 (P.fst index) in
        let r = P.snd index in
        m (P.prod c r)
      in
      Linalg.Intf.Vec (shape, m)

    let create_backprop_generic (node_accumulator : mat) (back : t array)
        node_jacobian =
      L.lam (fun _ ->
          Monad.run
          @@ let* ja = node_jacobian back in
             let rec loop i start_col =
               if i >= Array.length back then Monad.return M.unit
               else
                 let node_i = back.(i) in
                 let dim = output_dim node_i in
                 let*! end_col = I_ring.(sub (add start_col dim) one) in
                 let ja_i = col_restrict start_col end_col ja in
                 let (_, target_i, target_o) = accumulator node_i in
                 let* transf_ja = Mat.mm node_accumulator ja_i in
                 let* updated_acc = Mat.add target_i transf_ja in
                 Mat.(target_o := updated_acc) >> fun () ->
                 let*! start_col = I_ring.add end_col I_ring.one in
                 loop (i + 1) start_col
             in
             loop 0 I_ring.zero)
      [@@inline]

    let create_backprop_vec (node_jacobian : vec array -> mat k)
        (node_accumulator : mat) (back : t array) =
      create_backprop_generic node_accumulator back (fun back ->
          node_jacobian (Array.map value_i back))
      [@@inline]

    let create_backprop_arr (node_jacobian : arr m array -> mat k)
        (node_accumulator : mat) (back : t array) =
      create_backprop_generic node_accumulator back (fun back ->
          node_jacobian (Array.map value_storage back))
      [@@inline]

    let construct_backprop node =
      match node.accumulator with
      | None -> assert false
      | Some (_, mat_in, _) ->
          node.backprop <-
            Some
              (match node.kernel with
              | Ex_kernel (Node_kernel.Vec_kernel { jacobian; _ }) ->
                  create_backprop_vec jacobian mat_in node.back
              | Ex_kernel (Node_kernel.Arr_kernel { jacobian; _ }) ->
                  create_backprop_arr jacobian mat_in node.back)
  end

  type node = Node.t

  module Int_op = struct
    type t = int

    let compare (x : int) (y : int) =
      if x < y then 1 else if y < x then -1 else 0
  end

  module PQueue =
    Psq.Make
      (Int_op)
      (struct
        type t = Node.t

        let compare node1 node2 = Int_op.compare (Node.id node1) (Node.id node2)
      end)

  let counter_gen =
    let x = ref 0 in
    fun () ->
      let v = !x in
      incr x ;
      v

  (* This version is correct when we interpret the program dynamically
     but not when we generate code! *)
  (*
  let rec wrong_invalidate_value : t -> unit Repr.m =
   fun node ->
    let open Object_language_syntax in
    let* state = S_st.get node.state in
    S_enum.dispatch state @@ function
    | Invalid -> M.unit
    | Valid ->
        S_st.set node.state (S_enum.const Invalid) >> fun () ->
        iter_list node.fwd wrong_invalidate_value
   *)

  (* This implementation is correct but dumb: it is possible
     to run through subgraphs many times. *)
  (*
  let rec invalidate_value : t -> unit Repr.m =
   fun node ->
    let open Object_language_syntax in
    let* state = S_st.get node.state in
    S_enum.dispatch state @@ function
    | Invalid -> M.unit
    | Valid ->
        S_st.set node.state (S_enum.const Invalid) >> fun () ->
        iter_list node.fwd

  and iter_list (list : t list) =
    let open Object_language_syntax in
    List.fold_left
      (fun acc x -> acc >> fun () -> invalidate_value x)
      M.unit
      list
   *)

  (* [invalidate_value] runs through all nodes downstream of its
     argument and set its state as invalid. Nodes are visited
     only once. *)
  let rec invalidate_value : Node.t -> int -> unit Repr.m Monad.t =
   fun node fresh_counter ->
    if not (Node.is_initialized node) then
      (* A graph may contain uninitialized nodes, if those nodes are not in the
         transitive dependency cone of the node being differentiated.
         We just ignore those. *)
      Monad.return M.unit
    else if fresh_counter = Node.counter node then Monad.return M.unit
    else (
      Node.set_counter node fresh_counter ;
      L.app (Globals.invalidate_value ()) (Node.state node) >>! fun () ->
      iter_list (Node.fwd node) fresh_counter)

  and iter_list (list : Node.t list) fresh_counter =
    List.fold_left
      (fun acc x -> acc >> fun () -> invalidate_value x fresh_counter)
      (Monad.return M.unit)
      list

  (* precondition: [allocate_accumulators] has been called on the
     last node of the graph. *)
  let set : Node.t -> vec -> unit Repr.m Monad.t =
   fun node v ->
    (* invalid downstream nodes, including [node] *)
    invalidate_value node (counter_gen ()) >> fun () ->
    (* set and make [node] valid *)
    Vec.(Node.value_o node := v) >> fun () ->
    Monad.return (S_st.set (Node.state node) (S_enum.const Valid))

  let rec initialize node =
    if Node.is_initialized node then Monad.return ()
    else
      let* () = initialize_list (Array.to_list (Node.back node)) in
      Node.initialize node

  and initialize_list nodes =
    match nodes with
    | [] -> Monad.return ()
    | node :: tl ->
        let* () = initialize node in
        initialize_list tl

  let rec allocate_accumulators output_dimension node =
    if Node.is_accumulator_allocated node then Monad.return ()
    else
      let*! cols = Node.output_dim node in
      let*! rows = output_dimension in
      let*! numel = I_ring.mul cols rows in
      let shape = T.rank_two cols rows in
      let*! storage = A.make numel R.zero in
      let* (mat_in, mat_out) = Mat.of_array shape storage in
      Node.set_accumulator node (storage, mat_in, mat_out) ;
      let back = Node.back node in
      let* () =
        allocate_accumulators_list output_dimension (Array.to_list back)
      in
      Node.construct_backprop node ;
      Monad.return ()

  and allocate_accumulators_list output_dimension nodes =
    match nodes with
    | [] -> Monad.return ()
    | node :: tl ->
        let* () = allocate_accumulators output_dimension node in
        allocate_accumulators_list output_dimension tl

  let rec reset_accumulators node fresh_counter =
    if fresh_counter = Node.counter node then Monad.return ()
    else (
      Node.set_counter node fresh_counter ;
      let (_, _, acc_out) = Node.accumulator node in
      Mat.(acc_out := const (odim acc_out) R.zero) >> fun () ->
      reset_accumulators_list (Array.to_list (Node.back node)) fresh_counter)

  and reset_accumulators_list nodes visited =
    match nodes with
    | [] -> Monad.return ()
    | node :: tl ->
        let* () = reset_accumulators node visited in
        reset_accumulators_list tl visited

  let rec backprop node queue =
    let backprop = Node.backprop node in
    M.seq (L.app backprop M.unit) @@ fun () ->
    let queue = ref queue in
    Array.iter
      (fun node_i ->
        let id = Node.id node_i in
        if PQueue.mem id !queue then ()
        else queue := PQueue.add id node_i !queue)
      (Node.back node) ;
    backprop_queue !queue

  and backprop_queue queue =
    match PQueue.min queue with
    | None -> M.unit
    | Some (_, node) -> (
        match PQueue.rest queue with
        | None -> M.unit
        | Some queue -> backprop node queue)

  let backprop node =
    let psq = PQueue.empty in
    let output_dimension = T.rank_one (Node.output_dim node) in
    L.app (Node.update_value node) M.unit >>! fun () ->
    let* () = reset_accumulators node (counter_gen ()) in
    let (_, _, accu_o) = Node.accumulator node in
    Mat.(accu_o := identity output_dimension) >> fun () ->
    Monad.return (backprop node psq)

  module Generic_dimension = struct
    let make_binop ?(tag = "") ~output_dim ~op ~j x y =
      let kernel =
        Node_kernel.create_vec
          ~transfer:(fun inputs -> op inputs.(0) inputs.(1))
          ~jacobian:(fun inputs -> j inputs.(0) inputs.(1))
          ~output_dim
      in
      Node.create_vec ~tag ~backward:[| x; y |] ~kernel

    let const ?(tag = "const") const : node =
      Node.create_vec
        ~tag
        ~backward:[||]
        ~kernel:
          (Node_kernel.create_vec
             ~transfer:(fun _inputs -> Monad.return const)
             ~jacobian:(fun _inputs ->
               Monad.return
                 (Mat.const
                    (T.tensor (T.rank_one I_ring.zero) (Vec.idim const))
                    R.zero))
             ~output_dim:(T.dim (Vec.idim const) T.Path.empty))

    let var ~tag ~dimension : node =
      let rec node =
        lazy
          (Node.create_vec
             ~tag
             ~backward:[||]
             ~kernel:
               (Node_kernel.create_vec
                  ~transfer:(fun _inputs ->
                    let node = Lazy.force node in
                    (* Note that this is ill-dimensioned: executing this function
                       will fail. This code should stay unreachable. *)
                    Monad.return (Node.value_i node))
                  ~jacobian:(fun _inputs ->
                    Monad.return
                      (Mat.const
                         (T.tensor T.empty (T.rank_one dimension))
                         R.zero))
                  ~output_dim:dimension))
      in
      Lazy.force node

    let add ?(tag = "add") x y =
      (* concat_horiz will fail if dimensions of x and y are not equal *)
      make_binop
        ~tag
        ~output_dim:(Node.output_dim x)
        ~op:Vec.add
        ~j:(fun v1 v2 ->
          let open Mat in
          concat_horiz (identity (Vec.idim v1)) (identity (Vec.idim v2)))
        x
        y

    let sub ?(tag = "sub") x y =
      (* concat_horiz will fail if dimensions of x and y are not equal *)
      make_binop
        ~tag
        ~output_dim:(Node.output_dim x)
        ~op:Vec.sub
        ~j:(fun v1 v2 ->
          let open Mat in
          concat_horiz (identity (Vec.idim v1)) (neg (identity (Vec.idim v2))))
        x
        y

    let neg ?(tag = "x") x =
      Node.create_vec
        ~tag
        ~backward:[| x |]
        ~kernel:
          (Node_kernel.create_vec
             ~transfer:(fun inputs -> Monad.return (Vec.neg inputs.(0)))
             ~jacobian:(fun inputs ->
               Monad.return
                 (Mat.smul R.(neg one) (Mat.identity (Vec.idim inputs.(0)))))
             ~output_dim:(Node.output_dim x))

    let mul ?(tag = "mul") x y =
      (* concat_horiz will fail if dimensions of x and y are not equal *)
      make_binop
        ~tag
        ~output_dim:(Node.output_dim x)
        ~op:Vec.mul
        ~j:(fun v1 v2 ->
          let open Mat in
          concat_horiz (diagonal v2) (diagonal v1))
        x
        y

    let proj ?(tag = "proj") x i =
      Node.create_vec
        ~tag
        ~backward:[| x |]
        ~kernel:
          (Node_kernel.create_vec
             ~transfer:(fun inputs ->
               let* xi = Vec.get inputs.(0) i in
               Monad.return (Vec.const T.scalar xi))
             ~jacobian:(fun inputs ->
               let (Intf.Vec (cols, _)) = inputs.(0) in
               let shape = T.tensor cols (T.rank_one I_ring.one) in
               let*! index = P.prod i I_ring.zero in
               Mat.basis shape index R.one)
             ~output_dim:I_ring.one)

    let generate ~input_dim (f : node -> node k) =
      let var = var ~tag:"generated" ~dimension:input_dim in
      let* body = f var in
      (* [var] and [body] might end up in disjoint connected components.
         (This means that the user provided a constant function,
         which is her right). We manually initialize the [var] node
         so that the code below doesn't fail. *)
      let*! invalidate_value =
        let open M in
        L.lam (fun state ->
            let* st = S_st.get state in
            S_enum.dispatch st @@ function
            | Invalid -> M.unit
            | Valid -> S_st.set state (S_enum.const Invalid))
      in
      Globals.register_invalidate_value invalidate_value ;
      let* () = Node.initialize var in
      let* () = initialize body in
      let*! forward =
        L.lam (fun (input : A.t Repr.m) ->
            L.lam (fun (output : A.t Repr.m) ->
                Monad.run
                  (let v_in = Vec.in_of_array input in
                   set var v_in >> fun () ->
                   L.app (Node.update_value body) M.unit >>! fun () ->
                   let value_storage = Node.value_storage body in
                   Monad.return
                   @@ A.blit
                        value_storage
                        I_ring.zero
                        output
                        I_ring.zero
                        (A.length value_storage))))
      in
      let output_dim = Node.output_dim body in
      let* () = allocate_accumulators output_dim body in
      let*! derivative =
        L.lam (fun (input : A.t Repr.m) ->
            L.lam (fun (output : A.t Repr.m) ->
                Monad.run
                @@
                let v_in = Vec.in_of_array input in
                set var v_in >> fun () ->
                backprop body >> fun () ->
                if not (Node.is_accumulator_allocated var) then
                  (* If [body] does not depend on the input,
                     [reset_accumulator] is never called on [var].
                     We return a 0 accumulator. *)
                  let output_dim = Node.output_dim body in
                  let*! numel = I_ring.mul input_dim output_dim in
                  let dst = Vec.out_of_array output in
                  Vec.(dst := const (T.rank_one numel) R.zero)
                else
                  let (storage, _, _) = Node.accumulator var in
                  Monad.return
                  @@ A.blit
                       storage
                       I_ring.zero
                       output
                       I_ring.zero
                       (A.length storage)))
      in
      Monad.return (P.prod forward derivative)
  end

  module Output_dim = struct
    type t =
      | Same_as of { index : int }
      | Sum
      | One
      | Dim of index m
      | Fun of (index m array -> index m)

    let same_as index = Same_as { index }

    let sum = Sum

    let one = One

    let dim index = Dim index

    let custom f = Fun f

    let eval output_dim backward =
      match output_dim with
      | Same_as { index } ->
          if index < 0 || index >= Array.length backward then
            raise Invalid_output_dimension_specification
          else Node.output_dim backward.(index)
      | Sum ->
          let dims = Array.map Node.output_dim backward in
          Array.fold_left I_ring.add I_ring.zero dims
      | One -> I_ring.one
      | Dim d -> d
      | Fun f -> f (Array.map Node.output_dim backward)
  end

  module Scalar : sig
    include Basic_intf.Lang.Ring with type 'a m = 'a and type t = node

    val inj : R.t Repr.m -> t

    val prj : t -> R.t Repr.m

    val to_generic : t -> node

    val of_generic : node -> t
  end = struct
    type 'a m = 'a

    type t = node

    let inj c = Generic_dimension.const (Vec.const (T.rank_one I_ring.one) c)

    let prj x =
      if Node.is_initialized x then A.get (Node.value_storage x) I_ring.zero
      else
        Monad.run
          (let* () = Node.initialize x in
           Monad.return (A.get (Node.value_storage x) I_ring.zero))

    let zero =
      Generic_dimension.const
        ~tag:"zero"
        (Vec.const (T.rank_one I_ring.one) R.zero)

    let one =
      Generic_dimension.const
        ~tag:"one"
        (Vec.const (T.rank_one I_ring.one) R.one)

    let add x y =
      if Node.id x = Node.id zero then y
      else if Node.id y = Node.id zero then x
      else Generic_dimension.add x y

    let sub x y = Generic_dimension.sub x y

    let neg x = Generic_dimension.neg x

    let mul x y =
      if Node.id x = Node.id one then y
      else if Node.id y = Node.id one then x
      else if Node.id x = Node.id zero then zero
      else if Node.id y = Node.id zero then zero
      else Generic_dimension.mul x y

    let of_int i =
      Generic_dimension.const
        ~tag:"one"
        (Vec.const (T.rank_one I_ring.one) (R.of_int i))

    let to_generic x = x

    let of_generic x = x
  end

  module Vector : sig
    type 'a m = 'a

    type t = node

    val inj : A.t Repr.m -> t

    val prj : t -> A.t Repr.m

    val const : vec -> t

    val zero : index Repr.m -> t

    val one : index Repr.m -> t

    val add : t -> t -> t

    val sub : t -> t -> t

    val neg : t -> t

    val mul : t -> t -> t

    val get : t -> index Repr.m -> Scalar.t

    val concat : t -> t -> t

    val restrict : ofs:index Repr.m -> len:index Repr.m -> t -> t

    val to_generic : t -> node

    val of_generic : node -> t
  end = struct
    type 'a m = 'a

    type t = node

    let inj a =
      let v = Vec.in_of_array a in
      Generic_dimension.const v

    let prj x =
      if Node.is_initialized x then Node.value_storage x
      else
        Monad.run
          (let* () = Node.initialize x in
           Monad.return (Node.value_storage x))

    let const v = Generic_dimension.const v

    let zero dim = Generic_dimension.const (Vec.const (T.rank_one dim) R.zero)

    let one dim = Generic_dimension.const (Vec.const (T.rank_one dim) R.one)

    let add x y = Generic_dimension.add x y

    let sub x y = Generic_dimension.sub x y

    let neg x = Generic_dimension.neg x

    let mul x y = Generic_dimension.mul x y

    let get x i = Generic_dimension.proj x i

    (* TODO: export this into {!Linalg} *)
    let vec_concat (Vec (s1, m1)) (Vec (s2, m2)) =
      let* shape = T.concat s1 s2 T.Path.empty in
      let*! s1_dim = T.dim s1 T.Path.empty in
      let f index =
        let open M in
        B.dispatch (T.mem s1 index) @@ function
        | true -> m1 index
        | false ->
            let* index = I_ring.(sub index s1_dim) in
            m2 index
      in
      Monad.return (Vec (shape, f))

    (* TODO: test this *)
    let concat x y =
      Generic_dimension.make_binop
        ~tag:"concat"
        ~output_dim:Output_dim.(eval Sum [| x; y |])
        ~op:(fun v1 v2 -> vec_concat v1 v2)
        ~j:(fun v1 v2 ->
          let dim1 = Vec.idim v1 in
          let dim2 = Vec.idim v2 in
          let* shape = T.concat dim1 dim2 T.Path.empty in
          Monad.return (Mat.identity shape))
        x
        y

    (* TODO: test this *)
    let restrict ~ofs ~len x =
      let dom = T.rank_one len in
      Node.create_vec
        ~tag:"restrict"
        ~backward:[| x |]
        ~kernel:
          (Node_kernel.create_vec
             ~transfer:(fun inputs ->
               let v = inputs.(0) in
               let* restriction = T.Morphism.sub ~ofs dom (Vec.idim v) in
               let* result = Vec.pullback restriction v in
               Monad.return result)
             ~jacobian:(fun inputs ->
               let v = inputs.(0) in
               let dim = T.numel (Vec.idim v) in
               let shape1 = T.rank_two ofs len in
               let shape2 = dom in
               let shape3 = T.rank_two I_ring.(sub dim (add ofs len)) len in
               let mat1 = Mat.zero shape1 in
               let mat2 = Mat.identity shape2 in
               let mat3 = Mat.zero shape3 in
               let* tmp = Mat.concat_horiz mat1 mat2 in
               let* res = Mat.concat_horiz tmp mat3 in
               Monad.return res)
             ~output_dim:len)

    let to_generic x = x

    let of_generic x = x
  end

  let create :
      tag:string ->
      backward:node array ->
      transfer:(vec array -> vec Monad.t) ->
      jacobian:(vec array -> mat Monad.t) ->
      output_dim:Output_dim.t ->
      node =
   fun ~tag ~backward ~transfer ~jacobian ~output_dim ->
    let output_dim = Output_dim.eval output_dim backward in
    Node.create_vec
      ~tag
      ~backward
      ~kernel:(Node_kernel.create_vec ~transfer ~jacobian ~output_dim)

  let unsafe_create :
      tag:string ->
      backward:node array ->
      transfer:(inp:arr m array -> out:arr m -> unit m Monad.t) ->
      jacobian:(arr m array -> mat Monad.t) ->
      output_dim:Output_dim.t ->
      node =
   fun ~tag ~backward ~transfer ~jacobian ~output_dim ->
    let output_dim = Output_dim.eval output_dim backward in
    Node.create_arr
      ~tag
      ~backward
      ~kernel:(Node_kernel.create_arr ~transfer ~jacobian ~output_dim)

  let generate_ss (f : Scalar.t -> Scalar.t k) :
      ((R.t -> R.t) * (R.t -> R.t)) Repr.m k =
    let* code = Generic_dimension.generate ~input_dim:I_ring.one f in
    let*! f = P.fst code in
    let*! df = P.snd code in
    let*! f =
      L.lam (fun (x : R.t Repr.m) ->
          Monad.run
          @@ let*! inp = A.make I_ring.one x in
             let*! out = A.make I_ring.one R.zero in
             L.(app (app f inp) out) >>! fun () ->
             Monad.return (A.get out I_ring.zero))
    in
    let*! df =
      L.lam (fun (x : R.t Repr.m) ->
          Monad.run
          @@ let*! inp = A.make I_ring.one x in
             let*! out = A.make I_ring.one R.zero in
             L.(app (app df inp) out) >>! fun () ->
             Monad.return (A.get out I_ring.zero))
    in
    Monad.return (P.prod f df)

  let generate_sv (f : Scalar.t -> Vector.t k) :
      ((R.t -> A.t -> unit) * (R.t -> A.t -> unit)) Repr.m k =
    let* code = Generic_dimension.generate ~input_dim:I_ring.one f in
    let*! f = P.fst code in
    let*! df = P.snd code in
    let*! f =
      L.lam (fun (x : R.t Repr.m) ->
          Monad.run
          @@ let*! inp = A.make I_ring.one x in
             Monad.return (L.app f inp))
    in
    let*! df =
      L.lam (fun (x : R.t Repr.m) ->
          Monad.run
          @@ let*! inp = A.make I_ring.one x in
             Monad.return (L.app df inp))
    in
    Monad.return (P.prod f df)

  let generate_vs ~input_dim (f : Vector.t -> Scalar.t k) :
      ((A.t -> R.t) * (A.t -> A.t -> unit)) Repr.m k =
    let* code = Generic_dimension.generate ~input_dim f in
    let*! f = P.fst code in
    let*! df = P.snd code in
    let*! f =
      L.lam (fun (x : A.t Repr.m) ->
          Monad.run
          @@ let*! out = A.make I_ring.one R.zero in
             L.(app (app f x) out) >>! fun () ->
             Monad.return (A.get out I_ring.zero))
    in
    Monad.return (P.prod f df)

  let generate_vv ~input_dim (f : Vector.t -> Vector.t k) :
      ((A.t -> A.t -> unit) * (A.t -> A.t -> unit)) Repr.m k =
    Generic_dimension.generate ~input_dim f

  module Export = struct
    type node_info = { tag : string; id : int; output_dim : I_ring.t m }

    module Node_info = struct
      type t = node_info

      let compare (a : t) (b : t) = Int.compare a.id b.id

      let equal (a : t) (b : t) = a.id = b.id

      let hash node = Hashtbl.hash node.id
    end

    module Node_set = Set.Make (Node_info)
    module Node_table = Hashtbl.Make (Node_info)

    type graph = Node_info.t list Node_table.t

    let info (node : Node.t) =
      { tag = Node.tag node;
        id = Node.id node;
        output_dim = Node.output_dim node
      }

    let add_edge (graph : graph) src dst =
      match Node_table.find_opt graph src with
      | None -> Node_table.add graph src [dst]
      | Some dsts ->
          let dsts = if List.mem dst dsts then dsts else dst :: dsts in
          Node_table.replace graph src dsts

    type mode = Backward_only | Full

    let compute_graph_internal mode node =
      let rec loop graph visited (node : Node.t) =
        let ninfo = info node in
        if Node_set.mem ninfo visited then ()
        else (
          Array.iter
            (fun back -> add_edge graph (info back) ninfo)
            (Node.back node) ;
          let visited = Node_set.add ninfo visited in
          Array.iter (fun back -> loop graph visited back) (Node.back node) ;
          match mode with
          | Backward_only -> ()
          | Full ->
              List.iter (fun fwd -> loop graph visited fwd) (Node.fwd node))
      in
      let visited = Node_set.empty in
      let graph = Node_table.create 511 in
      Node_table.add graph (info node) [] ;
      loop graph visited node ;
      graph

    let compute_graph mode node = compute_graph_internal mode node

    let to_adjacency_list graph =
      let nodes = Array.of_seq (Node_table.to_seq_keys graph) in
      Array.map (fun node -> (node, Node_table.find graph node)) nodes

    let to_dot ?(name = "export") ~mode ~input_dim f oc =
      let v = Generic_dimension.var ~tag:"entrypoint" ~dimension:input_dim in
      let body = f v in
      let graph = compute_graph mode body in
      let adj = to_adjacency_list graph in
      let fmtr = Format.formatter_of_out_channel oc in
      Format.fprintf fmtr "digraph %s {@." name ;
      Array.iter
        (fun (node, _) ->
          Format.fprintf
            fmtr
            "%s_%d [label=\"%s_%d\"];@."
            node.tag
            node.id
            node.tag
            node.id)
        adj ;
      Array.iter
        (fun (node, subnodes) ->
          List.iter
            (fun node' ->
              Format.fprintf
                fmtr
                "%s_%d -> %s_%d;@."
                node.tag
                node.id
                node'.tag
                node'.id)
            subnodes)
        adj ;
      Format.fprintf fmtr "}@." ;
      flush oc
  end
end
[@@inline]
