prbnmcn-grad
------------

This library implements a kernel for reverse-mode algorithmic differentiation.
The implementation is parameterized by an arbitrary ring of scalar and also
parameterized by the target language, so that jacobians can be evaluated or
converted to efficient code. Higher derivatives can be obtained by iterating
the functor.

Here's a simple example, minimizing a function by gradient descent.

```ocaml
open Grad
module G = Impl.Float

let rec gradient_descent_s grad v0 eps rate =
  let gradient = grad v0 in
  let norm = abs_float gradient in
  Format.eprintf "at %f, gradient = %f, norm = %f@." v0 gradient norm ;
  if norm < eps then v0
  else
    let v0' = v0 -. (rate *. gradient) in
    gradient_descent_s grad v0' eps rate

let expr : G.Scalar.t -> G.Scalar.t =
 fun x ->
  let open G.Scalar in
  let e = sub x (inj 10.) in
  mul e e

(* [fn] is the forward function corresponding to [expr], [diff] its gradient *)
let (fn, diff) : (float -> float) * (float -> float) = G.generate_ss expr

let () = Format.printf "minimizing f(x) = (x-10)^2@."

let minimizer = gradient_descent_s diff 0.0 0.01 0.1

let () = Format.printf "approximate minimizer found at x = %f@." minimizer
```

You can guess the outcome :)

Note that this library is rather experimental and still poorly optimized.
It is meant to be used as a backend by higher-level libraries.
