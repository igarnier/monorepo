open Basic_intf

module type Generator = sig
  type t

  val gen : t Crowbar.gen
end

module Make_abelian_group_test (N : sig
  val name : string
end) (G : sig
  include Abelian_group

  include Std with type t := t
end)
(Gen : Generator with type t = G.t) =
struct
  open G

  let associative op x y z = equal (op x (op y z)) (op (op x y) z)

  let commutative op x y = equal (op x y) (op y x)

  let zero_add x = equal (add x zero) x

  let neg_add x = equal (add x (neg x)) zero

  let apply k =
    let open Crowbar in
    map [Gen.gen] k

  let apply_pair k =
    let open Crowbar in
    map [Gen.gen; Gen.gen] k

  let apply_triple k =
    let open Crowbar in
    map [Gen.gen; Gen.gen; Gen.gen] k

  let add_associative = apply_triple (associative add)

  let add_commutative = apply_triple (associative add)

  let zero_add_neutral = apply zero_add

  let zero_neg_inverse = apply neg_add

  let add_test name bool_gen =
    Crowbar.add_test ~name:(Printf.sprintf "%s: %s" N.name name) [bool_gen]
    @@ Crowbar.check

  let () = add_test "add_associative" add_associative

  let () = add_test "add_commutative" add_commutative

  let () = add_test "zero_add_neutral" zero_add_neutral

  let () = add_test "zero_neg_inverse" zero_neg_inverse
end

module Make_ring_test (N : sig
  val name : string
end)
(R : Ring_std)
(Gen : Generator with type t = R.t) =
struct
  include Make_abelian_group_test (N) (R) (Gen)
  open R

  let associative op x y z = equal (op x (op y z)) (op (op x y) z)

  let commutative op x y = equal (op x y) (op y x)

  let distribute_l add mul x y z =
    equal (mul (add x y) z) (add (mul x z) (mul y z))

  let distribute_r add mul x y z =
    equal (mul z (add x y)) (add (mul z x) (mul z y))

  let zero_add x = equal (add x zero) x

  let one_mul x = equal (mul x one) x

  let mul_one x = equal (mul one x) x

  let neg_add x = equal (add x (neg x)) zero

  let apply k =
    let open Crowbar in
    map [Gen.gen] k

  let apply_pair k =
    let open Crowbar in
    map [Gen.gen; Gen.gen] k

  let apply_triple k =
    let open Crowbar in
    map [Gen.gen; Gen.gen; Gen.gen] k

  (* Instantiate Ring-specific tests *)
  let mul_associative = apply_triple (associative add)

  let add_mul_distribute_l = apply_triple (distribute_l add mul)

  let add_mul_distribute_r = apply_triple (distribute_r add mul)

  let one_mul_neutral = apply one_mul

  let mul_one_neutral = apply mul_one

  let add_test name bool_gen =
    Crowbar.add_test ~name:(Printf.sprintf "%s: %s" N.name name) [bool_gen]
    @@ Crowbar.check

  let () = add_test "mul_associative" mul_associative

  let () = add_test "add_mul_distribute_l" add_mul_distribute_l

  let () = add_test "add_mul_distribute_r" add_mul_distribute_r

  let () = add_test "one_mul_neutral" one_mul_neutral

  let () = add_test "mul_one_neutral" mul_one_neutral
end

module Make_commutative_ring_test (N : sig
  val name : string
end)
(R : Ring_std)
(Gen : Generator with type t = R.t) =
struct
  include Make_ring_test (N) (R) (Gen)

  let mul_commutative = apply_triple (associative R.add)

  let () = add_test "mul_commutative" mul_commutative
end

module Make_module_test (N : sig
  val name : string
end)
(M : Module_std)
(Ring_gen : Generator with type t = M.R.t)
(Gen : Generator with type t = M.t) =
struct
  include Make_abelian_group_test (N) (M) (Gen)
  open M

  let distribute_over_vector_add s x y =
    equal (smul s (add x y)) (add (smul s x) (smul s y))

  let scalar_add_distribute_over_smul s t x =
    equal (smul (M.R.add s t) x) (add (smul s x) (smul t x))

  let scalar_mul_distribute_over_smul s t x =
    equal (smul (M.R.mul s t) x) (smul s (smul t x))

  let scalar_mul_unit x = equal (smul M.R.one x) x

  let () =
    add_test
      "distribute_over_vector_add"
      Crowbar.(map [Ring_gen.gen; Gen.gen; Gen.gen] distribute_over_vector_add)

  let () =
    add_test
      "scalar_add_distribute_over_smul"
      Crowbar.(
        map
          [Ring_gen.gen; Ring_gen.gen; Gen.gen]
          scalar_add_distribute_over_smul)

  let () =
    add_test
      "scalar_mul_distribute_over_smul"
      Crowbar.(
        map
          [Ring_gen.gen; Ring_gen.gen; Gen.gen]
          scalar_mul_distribute_over_smul)

  let () = add_test "scalar_mul_unit" Crowbar.(map [Gen.gen] scalar_mul_unit)
end

module Z_commutative_ring =
  Make_commutative_ring_test
    (struct
      let name = "Z"
    end)
    (Basic_impl.Integer)
    (struct
      type t = Z.t

      let gen = Crowbar.(map [int64] Z.of_int64)
    end)

module Z2 = Basic_impl.Integer_mod2

module Z2_commutative_ring =
  Make_commutative_ring_test
    (struct
      let name = "Z2"
    end)
    (Basic_impl.Integer_mod2)
    (struct
      type t = Z2.t

      let gen =
        Crowbar.(map [bool] (function true -> Z2.Zero | false -> Z2.One))
    end)

module Q_generator = struct
  type t = Basic_impl.Lang.Rational.t

  let gen =
    Crowbar.(map [int64; int64] (fun n d -> Q.(of_int64 n / of_int64 d)))
end

module Q_commutative_ring =
  Make_commutative_ring_test
    (struct
      let name = "Q"
    end)
    (Basic_impl.Lang.Rational)
    (Q_generator)

(* Serious business starts here *)

module Free_module_generator
    (A : Free_module_std with type Basis.t = int) (Gens : sig
      val key_gen : A.basis Crowbar.gen

      val value_gen : A.R.t Crowbar.gen

      val pp_key : Format.formatter -> A.basis -> unit
    end) =
struct
  type t = A.t

  type key = A.basis

  type value = A.R.t

  type action = Set of key * value

  and actions = Seq of actions * action | Nil

  let set k v = Set (k, v)

  let ( &@ ) x y = Seq (x, y)

  let assign m k v =
    let current = A.eval m k in
    let diff = A.R.(add v (neg current)) in
    A.add m (A.smul diff (A.delta k))

  let rec interpret action m =
    match action with
    | Set (k, v) -> (
        try assign m k v
        with e ->
          Format.eprintf "Error (set %a %a)@." Gens.pp_key k A.R.pp v ;
          raise e)

  and interpret_seq action m =
    match action with Seq (l, r) -> interpret r (interpret_seq l m) | Nil -> m

  let set_action_gen =
    let open Crowbar in
    map [Gens.key_gen; Gens.value_gen] set

  let set_actions_gen =
    let open Crowbar in
    fix (fun actions_gen ->
        choose [map [actions_gen; set_action_gen] ( &@ ); const Nil])

  let gen =
    let open Crowbar in
    map [set_actions_gen; range 256] (fun actions init_size ->
        interpret_seq
          actions
          (A.of_list (List.init init_size (fun i -> (i, A.R.zero)))))
end

module Int_map = Map.Make (Basic_impl.Lang.Int)
module Sparse_vec =
  Basic_impl.Free_module.Make (Basic_impl.Lang.Int) (Basic_impl.Lang.Rational)
    (Int_map)

module Q_sparse_vec =
  Make_module_test
    (struct
      let name = "Sparse_vec(Q)"
    end)
    (Sparse_vec)
    (Q_generator)
    (Free_module_generator
       (Sparse_vec)
       (struct
         let key_gen = Crowbar.range 256

         let value_gen = Q_generator.gen

         let pp_key = Format.pp_print_int
       end))

module Q_array = Basic_impl.Lang.Make_array (Basic_impl.Lang.Rational)

(* TODO: implementation of free module interface based on array *)
