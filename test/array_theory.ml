open Basic_intf

module type Array = sig
  type 'a m

  val name : string

  type t

  type out

  type key

  type value

  val key_gen : key Crowbar.gen

  val distinct_key_gen : (key * key) Crowbar.gen

  val value_gen : value Crowbar.gen

  val compare_key : key -> key -> int

  val pp_key : Format.formatter -> key -> unit

  val compare_value : value -> value -> int

  val pp_value : Format.formatter -> value -> unit

  val create : unit -> (t * out) m

  val set : key -> value -> out -> unit m

  val get : key -> t -> value m

  val equal : t -> t -> bool m
end

module Make_test
    (M : Monad with type 'a res = 'a)
    (A : Array with type 'a m = 'a M.t) =
struct
  type array = A.t

  type key = A.key

  type value = A.value

  type action = Set of key * value | Get of key

  and actions = Seq of actions * action | Nil

  type outcome = Unit | Elt of value

  let set k v = Set (k, v)

  let get k = Get k

  let ( &@ ) x y = Seq (x, y)

  let ( >>= ) = M.bind

  let return = M.return

  let rec interpret action m o =
    match action with
    | Set (k, v) -> A.set k v o >>= fun () -> return Unit
    | Get k -> A.get k m >>= fun v -> return (Elt v)

  and interpret_seq action m o =
    match action with
    | Seq (l, r) -> interpret_seq l m o >>= fun _ -> interpret r m o
    | Nil -> return Unit

  let set_action_gen =
    let open Crowbar in
    map [A.key_gen; A.value_gen] set

  let set_actions_gen : actions Crowbar.gen =
    let open Crowbar in
    fix (fun actions_gen ->
        choose [map [actions_gen; set_action_gen] ( &@ ); const Nil])

  let get_after_set : (actions * value) Crowbar.gen =
    let open Crowbar in
    map [set_actions_gen; A.key_gen; A.value_gen] (fun actions k v ->
        (actions &@ set k v &@ get k, v))

  let get_after_disjoint_set : (actions * value) Crowbar.gen =
    let open Crowbar in
    map
      [set_actions_gen; A.distinct_key_gen; A.value_gen; A.value_gen]
      (fun actions (k, k') v v' ->
        (actions &@ set k v &@ set k' v' &@ get k, v))

  let () =
    Crowbar.add_test
      ~name:(Printf.sprintf "%s: set k v x; get k x = v" A.name)
      [get_after_set]
    @@ fun (actions, v) ->
    M.run
    @@ ( A.create () >>= fun (m, out) ->
         interpret_seq actions m out >>= function
         | Elt v' ->
             Crowbar.check (A.compare_value v v' = 0) ;
             M.return ()
         | _ -> assert false )

  let () =
    Crowbar.add_test
      ~name:
        (Printf.sprintf
           "%s: forall k != k', set k v x; set k' v' x; get k  = v"
           A.name)
      [get_after_disjoint_set]
    @@ fun (actions, v) ->
    M.run
    @@ ( A.create () >>= fun (m, out) ->
         interpret_seq actions m out >>= function
         | Elt v' ->
             Crowbar.check (A.compare_value v v' = 0) ;
             M.return ()
         | _ -> assert false )
end

module Linalg_vec_as_array : Array with type 'a m = 'a = struct
  module V = Linalg.Vec.Float

  type 'a m = 'a

  type t = int V.t

  type out = int V.out

  type key = int

  type value = float

  let pp_value = Format.pp_print_float

  let pp_key = Format.pp_print_int

  let name = "Linalg_vec"

  let length = 100

  let key_gen = Crowbar.range length

  let distinct_key_gen =
    Crowbar.(map [key_gen] (fun i -> (i, (i + 1) mod length)))

  let value_gen = Crowbar.float

  let compare_value = Float.compare

  let compare_key = Int.compare

  let create () : t * out =
    let open Linalg.Intf in
    let r = Array.init 100 (fun _ -> 0.0) in
    ( Vec (Linalg.Tensor.Int.rank_one length, fun i -> r.(i)),
      OVec (Linalg.Tensor.Int.rank_one length, fun i elt -> r.(i) <- elt) )

  let set k v m = V.set m k v

  let get k v = V.get v k

  let equal =
    let domain = Array.init length (fun i -> i) in
    fun (Linalg.Intf.Vec (_, f)) (Linalg.Intf.Vec (_, g)) ->
      Array.for_all (fun i -> Float.equal (f i) (g i)) domain
end

module Linalg_mat_as_array : Array with type 'a m = 'a = struct
  module V = Linalg.Mat.Float

  type 'a m = 'a

  type t = (int * int) V.t

  type out = (int * int) V.out

  type key = int * int

  type value = float

  let name = "Linalg_matrix"

  let rows = 2 + Random.int 512

  let cols = 2 + Random.int 512

  let () =
    Crowbar.pp
      Format.std_formatter
      "%s init: rows = %d, cols = %d@."
      name
      rows
      cols

  let key_gen = Crowbar.(pair (range cols) (range rows))

  let distinct_key_gen =
    Crowbar.(
      map [key_gen] (fun (col, row) ->
          let row' = (row + 1) mod rows in
          let col' = (col + 1) mod cols in
          ((col, row), (col', row'))))

  let value_gen = Crowbar.float

  let compare_value = Float.compare

  let compare_key ((k1c, k1r) : key) ((k2c, k2r) : key) =
    let c = Int.compare k1r k2r in
    if c = 0 then Int.compare k1c k2c else c

  let pp_value = Format.pp_print_float

  let pp_key fmtr (c, r) = Format.fprintf fmtr "@[{ c=%d; r=%d }@]" c r

  let create () =
    let open Linalg.Intf in
    let dims = Linalg.Tensor.Int.rank_two cols rows in
    let m = Array.make_matrix cols rows 0.0 in
    ( Vec (dims, fun (c, r) -> m.(c).(r)),
      OVec (dims, fun (c, r) elt -> m.(c).(r) <- elt) )

  let set i v m = V.set m i v

  let get i m = V.get m i

  let equal (Linalg.Intf.Vec (_, f) : t) (Linalg.Intf.Vec (_, g) : t) =
    let exception Break in
    try
      for i = 0 to cols - 1 do
        for j = 0 to rows - 1 do
          if not Float.(equal (f (i, j)) (g (i, j))) then raise Break else ()
        done
      done ;
      true
    with Break -> false
end

module Linalg_vec_test =
  Make_test (Basic_impl.Monad.Identity) (Linalg_vec_as_array)
module Linalg_mat_test =
  Make_test (Basic_impl.Monad.Identity) (Linalg_mat_as_array)

(* Todo: add sparse Linalg test *)
(* module Sparse_vec_test = Make_applicative_test (Sparse_vec_as_array)
 * module Sparse_mat_test = Make_applicative_test (Sparse_matrix_as_array) *)
