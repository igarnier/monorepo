## 0.0.2
- Added `merged_at` field in agglomerative clustering (@Willenbrink)
- Added `cluster_with_initial` in agglomerative clustering (@Willenbrink)
- Removed useless argument for agglomerative clustering functor
- Reduce computational complexity of agglomerative clustering implementation

## 0.0.1
- First release
