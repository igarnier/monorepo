#!/bin/bash

set -e

TARGET=$1
VERSION=$2
REPO=$(dune exec ./release_tools/releaser.exe -- homepage $1/dune-project)
NAME=$(dune exec ./release_tools/releaser.exe -- name $1/dune-project)

echo "Preparing to create empty repository for $1 to $REPO (name=$NAME). Ok? Y/N"

read answer

case $answer in
    "Y") ;;
    *) exit 0;;
esac

hub create $NAME
