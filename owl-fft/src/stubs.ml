open Bigarray

type float64_vec = (float, Bigarray.float64_elt, Bigarray.c_layout) Array1.t

type complex64_vec =
  (Complex.t, Bigarray.complex64_elt, Bigarray.c_layout) Array1.t

(* forward fft *)
external owl_float64_rfftf : float64_vec -> complex64_vec -> int -> unit
  = "float64_rfftf"

(* backward fft *)
external owl_float64_rfftb : complex64_vec -> float64_vec -> int -> unit
  = "float64_rfftb"
