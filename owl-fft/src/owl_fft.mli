open Bigarray

type float64_vec = (float, Bigarray.float64_elt, Bigarray.c_layout) Array1.t

type complex64_vec =
  (Complex.t, Bigarray.complex64_elt, Bigarray.c_layout) Array1.t

val rfft : float64_vec -> complex64_vec

val irfft : complex64_vec -> float64_vec
