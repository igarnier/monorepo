open Bigarray

type float64_vec = (float, Bigarray.float64_elt, Bigarray.c_layout) Array1.t

type complex64_vec =
  (Complex.t, Bigarray.complex64_elt, Bigarray.c_layout) Array1.t

let rfft (x : float64_vec) : complex64_vec =
  let dim = (Array1.dim x / 2) + 1 in
  let y = Array1.create Complex64 c_layout dim in
  Stubs.owl_float64_rfftf x y 0 ;
  y

let irfft (x : complex64_vec) : float64_vec =
  let dim = (Array1.dim x - 1) * 2 in
  let y = Array1.create Float64 c_layout dim in
  Stubs.owl_float64_rfftb x y 0 ;
  let inorm = 1. /. float_of_int dim in
  for i = 0 to dim - 1 do
    Array1.unsafe_set y i (Array1.unsafe_get y i *. inorm)
  done ;
  y
