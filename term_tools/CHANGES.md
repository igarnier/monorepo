## 0.0.3
- Add `example` directory

## 0.0.2
- First release of `term-tools`, a library to manipulate first-order terms.
