open Basic_intf

(** Module type produced by the functor {!Gradient.Make} *)
module type Gradient_S = sig
  type 'a k

  type 'a m

  type elt

  type index

  type arr

  (** Stopping rules *)
  module Stopping : sig
    type t

    (** Terminate after [niter] iterations *)
    val max_iter : niter:elt m -> t

    (** Terminate when the dot product of the gradient with itself is below the [bound] *)
    val squared_gradient_norm : bound:elt m -> t

    (** Terminate when the l2 norm of the gradient is below the [bound] *)
    val gradient_norm : bound:elt m -> t

    (** [conj c1 c2] terminate when both [c1] and [c2] terminate *)
    val conj : t -> t -> t

    (** [disj c1 c2] terminate when either [c1] or [c2] terminates *)
    val disj : t -> t -> t
  end

  (** Proximal operators *)
  module Proximal : sig
    type t

    val identity : t

    (** Soft thresolding operator, used for [lasso] *)
    val lasso : positive:bool -> lambda:elt m -> t

    (** Projection on the positive cone *)
    val positive : t
  end

  (** Learning rates *)
  module Rate : sig
    type t

    val constant : rate:elt m -> t

    val decay : initial_rate:elt m -> decay:elt m -> t

    (** Vanilla line search, using the Armijo-Goldstein condition.
        Constaints:
        - [slack] \in (0,1), a good default is 0.1
        - [shrinking_factor] \in (0,1), a good default is 0.5 *)
    val backtracking :
      max_rate:elt m ->
      slack:elt m ->
      shrinking_factor:elt m ->
      max_iter:index m ->
      t

    (** Modification of [backtracking] line search where the
       [shrinking_factor] parameter for the _next_ call of the line search
       is set to [shrinking_factor^{niter+1}] where [niter] is the number
       of line search loops performed in the last call.

       The goal is to perform less iterations in the next call
       by starting closer to a point satisfying the Armijo-Goldstein condition.
       This works well when eg the step size is decreasing monotonically. *)
    val adaptive_backtracking :
      max_rate:elt m ->
      slack:elt m ->
      shrinking_factor:elt m ->
      max_iter:elt m ->
      t

    (** Nonmonotone line search, adapted from the algorithm described in
        "A field guide to forward-backward splitting with a fasta implementation"
        by Goldstein et al.
        Taken originally from "A Nonmonotone Line Search Technique for Newton's Method"
          (Grippo et al.) *)
    val nonmonotone_line_search :
      rolling_width:index m ->
      max_rate:elt m ->
      shrinking_factor:elt m ->
      max_iter:index m ->
      proximal:Proximal.t ->
      t

    (** EXPERIMENTAL: Nonmonotone adaptive line search via the Barzilai-Borwein method.
        Use not recommended. *)
    val nonmonotone_adaptive :
      rolling_width:index m ->
      max_rate:elt m ->
      shrinking_factor:elt m ->
      max_iter:index m ->
      proximal:Proximal.t ->
      t
  end

  module Momentum : sig
    type t

    val none : t

    (** Fixed momentum *)
    val basic : momentum:elt m -> t

    (** Nesterov's accelerated gradient *)
    val nesterov : momentum:elt m -> t

    val adagrad : t

    (** Recommended parameter: [decay = 0.9] *)
    val rmsprop : decay:elt m -> t

    (** Adaptive moment estimation.
        Recommended: [beta1 = 0.9], [beta2 = 0.99] *)
    val adam : beta1:elt m -> beta2:elt m -> t
  end

  type objective_fun = arr -> elt

  type gradient_fun = arr -> arr -> unit

  (** [Raw] packs variants of the gradient descent algorithm that are not
      wrapped in lambdas. These can be used for direct inlining of the gradient
      descent code. *)
  module Raw : sig
    val gradient_descent :
      stopping:Stopping.t ->
      rate:Rate.t ->
      momentum:Momentum.t ->
      objective_fun m ->
      gradient_fun m ->
      arr m ->
      arr m k

    val forward_backward :
      rolling_width:index m ->
      max_rate:elt m ->
      shrinking_factor:elt m ->
      max_iter:index m ->
      stopping:Stopping.t ->
      proximal:Proximal.t ->
      objective_fun m ->
      gradient_fun m ->
      arr m ->
      arr m k

    val fasta :
      rolling_width:index m ->
      max_rate:elt m ->
      shrinking_factor:elt m ->
      max_iter:index m ->
      stopping:Stopping.t ->
      proximal:Proximal.t ->
      objective_fun m ->
      gradient_fun m ->
      arr m ->
      arr m k
  end

  val gradient_descent :
    stopping:Stopping.t ->
    rate:Rate.t ->
    momentum:Momentum.t ->
    (objective_fun -> gradient_fun -> arr -> arr) m

  val forward_backward :
    rolling_width:index m ->
    max_rate:elt m ->
    shrinking_factor:elt m ->
    max_iter:index m ->
    stopping:Stopping.t ->
    proximal:Proximal.t ->
    (objective_fun -> gradient_fun -> arr -> arr) m

  (** This implements the Barzilai-Borwein based adaptive proximal gradient descent algorithm
      described in
      "A field guide to forward-backward splitting with a fasta implementation", Goldstein et al.

      Recommended parameters: [shrinking_factor] \in [0.2, 0.5]
   *)
  val fasta :
    rolling_width:index m ->
    max_rate:elt m ->
    shrinking_factor:elt m ->
    max_iter:index m ->
    stopping:Stopping.t ->
    proximal:Proximal.t ->
    (objective_fun -> gradient_fun -> arr -> arr) m
end

(** Operations shared between the {!Differentiable_scalar} and {!Vector} module types. *)
module type Mappable = sig
  include Lang.Empty

  (** [t] is the type of scalars *)
  type t

  val ( + ) : t m -> t m -> t m

  val ( - ) : t m -> t m -> t m

  val ( * ) : t m -> t m -> t m

  val ( / ) : t m -> t m -> t m

  val pow : t m -> t m -> t m

  val pow_cst : t m -> float -> t m

  val sqrt : t m -> t m

  val exp : t m -> t m

  val log : t m -> t m

  val sin : t m -> t m

  val cos : t m -> t m

  val asin : t m -> t m

  val acos : t m -> t m

  val atan : t m -> t m

  val tanh : t m -> t m

  val cosh : t m -> t m

  val sinh : t m -> t m
end

(** The subset of operations on scalars that can be differentiated. *)
module type Differentiable_scalar = sig
  include Lang.Field

  include Mappable with type 'a m := 'a m and type t := t

  (** [of_float] injects a floating point number into the scalars. *)
  val of_float : float -> t m
end

(** The module type of scalars, including some non-differentiable operations. *)
module type Scalar = sig
  include Differentiable_scalar

  (** [is_nan] returns a computation evaluating to [true] if and only if its
      argument evaluates to NaN. *)
  val is_nan : t m -> bool m

  (** [is_inf] returns a computation evaluating to [true] if and only if its
      argument evaluates to plus or minus infinity. *)
  val is_inf : t m -> bool m
end

module type S = sig
  type 'a k

  type 'a m

  type 'a shape

  type arr

  type elt

  module G :
    Grad.Intf.S
      with type 'a k = 'a k
       and type 'a m = 'a m
       and type 'a shape = 'a shape
       and type elt = elt
       and type index = int
       and type arr = arr

  module Vec :
    Linalg.Intf.Vec
      with type 'a k = 'a k
       and type 'a m = 'a m
       and type 'a shape = 'a shape
       and type elt = elt

  module Mat :
    Linalg.Intf.Mat
      with type 'a k = 'a k
       and type 'a m = 'a m
       and type base_index = int
       and type 'a shape = 'a shape
       and type elt = elt

  module Vec_backend : sig
    val of_array : arr m -> int Vec.t * int Vec.out

    val in_of_array : arr m -> int Vec.t

    val out_of_array : arr m -> int Vec.out
  end

  module Mat_backend : sig
    type index = int * int

    val of_array :
      (int * int) shape -> arr m -> ((int * int) Mat.t * (int * int) Mat.out) k

    val in_of_array : (int * int) shape -> arr m -> (int * int) Mat.t k

    val out_of_array : (int * int) shape -> arr m -> (int * int) Mat.out k
  end

  module Diff_scalar : sig
    type t = G.Scalar.t

    val inj : elt m -> t

    val prj : t -> elt m

    val to_generic : t -> G.node

    val of_generic : G.node -> t

    val zero : t

    val add : t -> t -> t

    val neg : t -> t

    val one : t

    val mul : t -> t -> t

    val sub : t -> t -> t

    val of_int : int -> t

    val of_float : float -> t

    val of_scalar : elt m -> t

    val to_scalar : t -> elt m

    include Mappable with type t := t and type 'a m := 'a

    val ( ** ) : t -> t -> t
  end

  module Diff_vec : sig
    type index = int

    type array = floatarray

    type scalar = Diff_scalar.t

    type t = G.Vector.t

    val inj : arr m -> t

    val prj : t -> arr m

    val const : int Vec.t -> t

    val zero : index m -> t

    val one : index m -> t

    val add : t -> t -> t

    val sub : t -> t -> t

    val neg : t -> t

    val mul : t -> t -> t

    val get : t -> index m -> scalar

    val to_generic : t -> G.node

    val of_generic : G.node -> t

    val to_array : t -> arr m

    val of_array : arr m -> t

    val output_dim : G.Output_dim.t

    val of_vec : index Vec.t -> t

    val broadcast : index m -> scalar -> t

    val reduce_scalar_array : scalar Array.t -> scalar

    val reduce : t -> scalar

    val l2_squared : t -> scalar

    val l2 : t -> scalar

    val dot : t -> t -> scalar

    val const_mm : (int * int) Mat.t -> t -> t

    include Mappable with type t := t and type 'a m := 'a

    val ( ** ) : t -> t -> t
  end

  val generate_ss :
    (Diff_scalar.t -> Diff_scalar.t k) -> ((elt -> elt) * (elt -> elt)) m k

  val generate_sv :
    (Diff_scalar.t -> Diff_vec.t k) ->
    ((elt -> arr -> unit) * (elt -> arr -> unit)) m k

  val generate_vs :
    input_dim:int m ->
    (Diff_vec.t -> Diff_scalar.t k) ->
    ((arr -> elt) * (arr -> arr -> unit)) m k

  val generate_vv :
    input_dim:int m ->
    (Diff_vec.t -> Diff_vec.t k) ->
    ((arr -> arr -> unit) * (arr -> arr -> unit)) m k

  module Gradient_descent :
    Gradient_S
      with type 'a k = 'a k
       and type 'a m = 'a m
       and type elt = elt
       and type index = int
       and type arr = arr

  val default_stopping : Gradient_descent.Stopping.t

  val default_rate : Gradient_descent.Rate.t

  val default_momentum : Gradient_descent.Momentum.t

  module Regression : sig
    type inputs_data := arr

    type outputs_data := arr

    type starting_position := arr

    module Raw : sig
      val least_squares_generic :
        ?stopping:Gradient_descent.Stopping.t ->
        ?rate:Gradient_descent.Rate.t ->
        ?momentum:Gradient_descent.Momentum.t ->
        cols:int ->
        rows:int ->
        inputs_data m ->
        outputs_data m ->
        starting_position m ->
        arr m k

      val least_squares_proximal :
        ?stopping:Gradient_descent.Stopping.t ->
        proximal:Gradient_descent.Proximal.t ->
        cols:int ->
        rows:int ->
        inputs_data m ->
        outputs_data m ->
        starting_position m ->
        arr m k
    end

    val least_squares_generic :
      ?stopping:Gradient_descent.Stopping.t ->
      ?rate:Gradient_descent.Rate.t ->
      ?momentum:Gradient_descent.Momentum.t ->
      cols:int ->
      rows:int ->
      unit ->
      (inputs_data -> outputs_data -> starting_position -> arr) m

    val least_squares_proximal :
      ?stopping:Gradient_descent.Stopping.t ->
      proximal:Gradient_descent.Proximal.t ->
      cols:int ->
      rows:int ->
      unit ->
      (inputs_data -> outputs_data -> starting_position -> arr) m
  end
end
