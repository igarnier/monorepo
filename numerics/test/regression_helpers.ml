type rate =
  | Backtracking of { c : float; gamma : float }
  | Adaptive of { c : float; gamma : float }
  | Nonmonotone of { gamma : float }
  | Nonmonotone_adaptive of { gamma : float }
  | Constant of { alpha : float }
[@@deriving show]

type basis = Const | Linear | Square | Log | NlogN [@@deriving show]

type momentum =
  | Adagrad
  | Rmsprop of { decay : float }
  | Adam
  | Nesterov of { momentum : float }
[@@deriving show]

type gradient_descent =
  | Vanilla of { rate : rate; momentum : momentum }
  | Fasta
[@@deriving show]

module Make
    (Repr : Basic_intf.Lang.Empty)
    (R : Basic_intf.Lang.Const with type 'a m = 'a Repr.m and type t = float)
    (I : Basic_intf.Lang.Const with type 'a m = 'a Repr.m and type t = int)
    (NM : Numerics.Intf.S with type 'a m = 'a Repr.m and type elt = R.t) =
struct
  let quadratic_only =
    match Array.to_list Sys.argv with
    | _ :: "quadratic" :: _ -> true
    | _ -> false

  module Basis = struct
    type t = basis [@@deriving show]

    let all = [| Const; Linear; Square; Log; NlogN |]

    let sample =
      if quadratic_only then Stats.Gen.return Square else Stats.Gen.uniform all
  end

  module Rate = struct
    type t = rate [@@deriving show]

    module Backtracking = struct
      let gamma = [| 0.5; 0.9 |]

      let all = Array.map (fun gamma -> Backtracking { c = 0.1; gamma }) gamma
    end

    module Adaptive = struct
      let gamma = [| 0.5; 0.9 |]

      let all = Array.map (fun gamma -> Adaptive { c = 0.1; gamma }) gamma
    end

    module Nonmonotone = struct
      let gamma = [| 0.2; 0.5 |]

      let all = Array.map (fun gamma -> Nonmonotone { gamma }) gamma
    end

    module Nonmonotone_adaptive = struct
      let gamma = [| 0.2; 0.5 |]

      let all = Array.map (fun gamma -> Nonmonotone_adaptive { gamma }) gamma
    end

    module Constant = struct
      let alpha = [| 1.0; 0.1; 0.01 |]

      let all = Array.map (fun alpha -> Constant { alpha }) alpha
    end

    let make rate =
      match rate with
      | Backtracking { c; gamma } ->
          NM.Gradient_descent.Rate.backtracking
            ~max_rate:(R.const 100.0)
            ~slack:(R.const c)
            ~shrinking_factor:(R.const gamma)
            ~max_iter:(I.const 1_000)
      | Adaptive { c; gamma } ->
          NM.Gradient_descent.Rate.adaptive_backtracking
            ~max_rate:(R.const 100.0)
            ~slack:(R.const c)
            ~shrinking_factor:(R.const gamma)
            ~max_iter:(R.const 1_000.0)
      | Nonmonotone { gamma } ->
          NM.Gradient_descent.Rate.nonmonotone_line_search
            ~rolling_width:(I.const 10)
            ~max_rate:(R.const 100.0)
            ~shrinking_factor:(R.const gamma)
            ~max_iter:(I.const 1_000)
            ~proximal:NM.Gradient_descent.Proximal.identity
      | Nonmonotone_adaptive { gamma } ->
          NM.Gradient_descent.Rate.nonmonotone_line_search
            ~rolling_width:(I.const 10)
            ~max_rate:(R.const 100.0)
            ~shrinking_factor:(R.const gamma)
            ~max_iter:(I.const 1_000)
            ~proximal:NM.Gradient_descent.Proximal.identity
      | Constant { alpha } ->
          NM.Gradient_descent.Rate.constant ~rate:(R.const alpha)

    let all =
      Array.concat
        [ Backtracking.all;
          Adaptive.all;
          Nonmonotone.all;
          Nonmonotone_adaptive.all;
          Constant.all ]

    let sample = Stats.Gen.uniform all
  end

  module Momentum = struct
    type t = momentum [@@deriving show]

    module Nesterov = struct
      let momentum = [| 0.2; 0.5; 0.9 |]

      let all = Array.map (fun momentum -> Nesterov { momentum }) momentum
    end

    module Rmsprop = struct
      let decay = [| 0.2; 0.5; 0.9 |]

      let all = Array.map (fun decay -> Rmsprop { decay }) decay
    end

    let all = Array.concat [Rmsprop.all; [| Adagrad |]; [| Adam |]; Nesterov.all]

    let sample = Stats.Gen.uniform

    let make momentum =
      match momentum with
      | Nesterov { momentum } ->
          NM.Gradient_descent.Momentum.nesterov ~momentum:(R.const momentum)
      | Adagrad -> NM.Gradient_descent.Momentum.adagrad
      | Rmsprop { decay } ->
          NM.Gradient_descent.Momentum.rmsprop ~decay:(R.const decay)
      | Adam ->
          NM.Gradient_descent.Momentum.adam
            ~beta1:(R.const 0.9)
            ~beta2:(R.const 0.99)
  end

  module Gradient_descent = struct
    type t = gradient_descent [@@deriving show]
  end

  let make_problem ~dimension ~coeff1 ~coeff2 ~basis rng_state =
    let nonlinearity =
      match basis with
      | Const -> fun _ -> 1.
      | Linear -> fun x -> x
      | Square -> fun x -> x *. x
      | Log -> fun x -> log (1. +. x)
      | NlogN -> fun x -> x *. log (1. +. x)
    in
    let inputs =
      Array.init dimension (fun _i ->
          let x = Random.State.float rng_state 100.0 in
          [| 1.0; nonlinearity x |])
    in
    let outputs =
      Array.map
        (fun inputs -> (coeff1 *. inputs.(0)) +. (coeff2 *. inputs.(1)))
        inputs
    in
    let pb = Format.asprintf "\"%f + %f * %a\"" coeff1 coeff2 Basis.pp basis in
    (pb, inputs, outputs)

  let prepare_problem ~dimension rng_state =
    let coeff1 = Random.State.float rng_state 1000.0 in
    let coeff2 = Random.State.float rng_state 1000.0 in
    let basis = Basis.sample rng_state in
    (coeff1, coeff2, make_problem ~dimension ~coeff1 ~coeff2 ~basis rng_state)
end

module BL = Basic_impl.Lang
module NM = Numerics.Impl.Float

module Native =
  Make
    (BL.Empty)
    (BL.Make_const (struct
      type t = float
    end))
    (BL.Make_const (struct
      type t = int
    end))
    (NM)

let pp_float fmtr x = Format.fprintf fmtr "%.3f" x

let pp_arr pp fmtr (x : 'a array) =
  Format.open_hbox () ;
  for i = 0 to Array.length x - 1 do
    Format.fprintf fmtr "%a " pp (Array.unsafe_get x i)
  done ;
  Format.close_box ()

let pp fmtr (x : float array) =
  Format.open_hbox () ;
  for i = 0 to Array.length x - 1 do
    Format.fprintf fmtr "%a " pp_float (Array.unsafe_get x i)
  done ;
  Format.close_box ()

(* approximate equality *)

let fsum total = Array.fold_left ( +. ) 0.0 total

let l2norm x =
  let total = Array.map (fun x -> x *. x) x in
  let sum = fsum total in
  sqrt sum

let l2 x y =
  let delta = Array.map2 ( -. ) x y in
  l2norm delta

let near_enough ?(name = "no-name") target arr =
  Format.eprintf "Test \"%s\" " name ;
  if Basic_impl.Reals.Float.(l2 target arr < 0.01) then
    Format.eprintf "passed@."
  else (
    Format.eprintf " failed, expected %a, got %a@." pp target pp arr ;
    assert false)

let dot x y = fsum (Array.map2 ( *. ) x y)
