(* -- native *)
open Linalg
module GD = Numerics.Impl.Float.Gradient_descent
module Graph = Numerics.Impl.Float.G
module Vec_backend = Numerics.Impl.Float.Vec_backend

let ppa = Dense.Float_array.Native.pp

let debug = ref false

let counter = ref 0

let reset_counter () = counter := 0

let counter () = !counter

let near_enough ?(name = "no-name") target_x target_y arr =
  let x = Float.Array.get arr 0 in
  let y = Float.Array.get arr 1 in
  Format.printf "Test \"%s\", iterations: %d " name (counter ()) ;
  reset_counter () ;
  if abs_float (x -. target_x) <. 0.1 && abs_float (y -. target_y) <. 0.1 then
    Format.printf "passed@."
  else
    Format.printf
      " failed, expected (%f, %f), got (%f, %f)@."
      target_x
      target_y
      x
      y

let ( * ) = Graph.Scalar.mul

let ( + ) = Graph.Scalar.add

let ( - ) x y = Graph.Scalar.(add x (neg y))

module Quadratic () = struct
  let name = "quadratic"

  let () =
    Format.printf "@." ;
    Format.printf "Entering %s tests@." name ;
    Format.printf "-------------------------@."

  let f (v : Graph.Vector.t) =
    let open Graph.Scalar in
    let x = Graph.Vector.get v 0 - inj 1. in
    let y = Graph.Vector.get v 1 - inj 0.87 in
    (x * x) + (y * y)

  let (f, grad) = Graph.generate_vs ~input_dim:2 f

  let near_enough s result =
    near_enough ~name:(Printf.sprintf "%s: %s" name s) 1. 0.87 result

  let result =
    GD.(
      gradient_descent
        ~stopping:(Stopping.max_iter ~niter:200.)
        ~rate:(Rate.decay ~initial_rate:0.1 ~decay:0.1)
        ~momentum:Momentum.none
        f
        grad
        (Float.Array.map_from_array Fun.id [| 20.0; 20.0 |]))

  let () = near_enough "steepest descent, decay" result

  let result =
    GD.(
      gradient_descent
        ~stopping:(Stopping.gradient_norm ~bound:0.001)
        ~rate:
          (Rate.backtracking
             ~max_rate:1.0
             ~slack:0.1
             ~shrinking_factor:0.5
             ~max_iter:100)
        ~momentum:Momentum.none
        f
        grad
        (Float.Array.map_from_array Fun.id [| 20.0; 20.0 |]))

  let () = near_enough "steepest descent, backtracking" result

  let result =
    GD.(
      gradient_descent
        ~stopping:(Stopping.gradient_norm ~bound:0.001)
        ~rate:
          (Rate.backtracking
             ~max_rate:1.0
             ~slack:0.1
             ~shrinking_factor:0.5
             ~max_iter:100)
        ~momentum:(Momentum.nesterov ~momentum:0.9)
        f
        grad
        (Float.Array.map_from_array Fun.id [| 20.0; 20.0 |]))

  let () = near_enough "nesterov, backtracking" result
end

module Regression () = struct
  let name = "least-squares linear regression"

  let () =
    Format.printf "@." ;
    Format.printf "Entering %s tests@." name ;
    Format.printf "-------------------------@."

  let f _x = 1.0

  let g x = x *. x

  let () = debug := true

  (* let coeffs = Array.init 2 (fun _i -> Random.float 3.0) in *)
  let coeffs = [| 1.2; 2.5 |]

  let inputs =
    Array.init 100 (fun i ->
        let x = float_of_int i *. 0.1 in
        [| f x; g x |])

  let outputs =
    Array.map
      (fun components ->
        (coeffs.(0) *. components.(0)) +. (coeffs.(1) *. components.(1)))
      inputs

  let inputs_mat =
    Mat.Float.make Tensor.Int.(rank_two 2 (Array.length inputs))
    @@ fun (c, r) -> inputs.(r).(c)

  let outputs_vec =
    Vec_backend.in_of_array (Float.Array.map_from_array Fun.id outputs)

  let input_dim = 2

  module Diff_vec = struct
    include
      Vec.Make_native
        (Graph.Scalar)
        (Basic_impl.Lang.Make_storage (Graph.Scalar))
    include
      Vec.Array_backed (Basic_impl.Lang.Empty) (Basic_impl.Lang.Codegen)
        (Tensor.Int)
        (Basic_impl.Lang.Make_array (Graph.Scalar))
  end

  let to_be_optimized unknowns =
    let unknowns =
      Vec.Generic.vec (Tensor.Int.rank_one input_dim) @@ fun i ->
      Graph.Vector.get unknowns i
    in
    Vec.Generic.mapi
      (fun i outi ->
        let rowi =
          Vec.Float.map Graph.Scalar.inj @@ Mat.Float.row inputs_mat i
        in
        let dot = Diff_vec.dot rowi unknowns in
        Graph.Scalar.(
          let x = dot - inj outi in
          x * x))
      outputs_vec
    |> Diff_vec.reduce Graph.Scalar.add Graph.Scalar.zero
    |> Graph.Scalar.(mul (inj (1. /. float_of_int input_dim)))

  let (f, grad) = Graph.generate_vs ~input_dim:2 to_be_optimized

  let near_enough s result =
    near_enough
      ~name:(Printf.sprintf "%s: %s" name s)
      coeffs.(0)
      coeffs.(1)
      result

  let result =
    GD.(
      gradient_descent
        ~stopping:(Stopping.gradient_norm ~bound:0.2)
        ~rate:(Rate.constant ~rate:0.1)
        ~momentum:(Momentum.adam ~beta1:0.9 ~beta2:0.99)
        f
        grad
        (Float.Array.map_from_array Fun.id [| 5.0; 5.0 |]))

  let () = near_enough "adam, constant rate" result
end

module Rosenbrock () = struct
  (* The rosenbrock tests are deactivated. *)

  let name = "rosenbrock"

  let () =
    Format.printf "@." ;
    Format.printf "Entering %s tests@." name ;
    Format.printf "-------------------------@."

  let f (v : Graph.Vector.t) =
    let open Graph.Scalar in
    let x = Graph.Vector.get v 0 in
    let y = Graph.Vector.get v 1 in
    let first_term =
      let a = one - x in
      a * a
    in
    let second_term =
      let b = y - (x * x) in
      inj 100. * (b * b)
    in
    first_term + second_term

  let (f, grad) = Graph.generate_vs ~input_dim:2 f

  let rosenbrock_grad x y =
    ( 2. *. ((200. *. (x ** 3.)) -. (200. *. x *. y) +. x -. 1.),
      200. *. (y -. (x ** 2.)) )

  let () =
    let (dx, dy) = rosenbrock_grad 20. 20. in
    let storage = Float.Array.make 2 0.0 in
    let pos = Float.Array.map_from_array Fun.id [| 20.0; 20.0 |] in
    grad pos storage ;
    near_enough ~name:"initial test" dx dy storage

  let points = ref []

  let near_enough s result =
    near_enough ~name:(Printf.sprintf "%s: %s" name s) 1. 1. result

  let result =
    GD.(
      gradient_descent
        ~stopping:(Stopping.max_iter ~niter:1500.)
        ~rate:
          (Rate.backtracking
             ~max_rate:1.0
             ~slack:0.1
             ~shrinking_factor:0.5
             ~max_iter:100)
        ~momentum:(Momentum.nesterov ~momentum:0.9)
        f
        grad
        (Float.Array.map_from_array Fun.id [| 20.0; 20.0 |]))

  let () =
    let points = List.rev !points in
    List.iter (fun (x, y, z) -> Format.printf "%f %f %f@." x y z) points

  let () = near_enough "nesterov, backtracking" result
end

let () = Format.printf "gradient_descent_nat@."

let%expect_test "gradient descent, native" =
  let module _ = Quadratic () in
  let module _ = Regression () in
  let module _ = Rosenbrock () in
  () ;
  [%expect
    {|
    Entering quadratic tests
    -------------------------
    Test "quadratic: steepest descent, decay", iterations: 0 passed
    Test "quadratic: steepest descent, backtracking", iterations: 0 passed
    Test "quadratic: nesterov, backtracking", iterations: 0 passed

    Entering least-squares linear regression tests
    -------------------------
    Test "least-squares linear regression: adam, constant rate", iterations: 0 passed

    Entering rosenbrock tests
    -------------------------
    Test "initial test", iterations: 0 passed
    Test "rosenbrock: nesterov, backtracking", iterations: 0  failed, expected (1.000000, 1.000000), got (-3.962901, 15.711849) |}]
