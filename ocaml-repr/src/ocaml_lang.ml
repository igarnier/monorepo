open Basic_intf.Lang
open Ast_helper

let gensym =
  let x = ref 0 in
  fun () ->
    let v = !x in
    incr x ;
    Format.asprintf "_caml_repr_%d" v

let loc txt = { Asttypes.txt; loc = Location.none }

let loc_ident x = { Asttypes.txt = Longident.Lident x; loc = Location.none }

let loc_str (x : string) = { Asttypes.txt = x; loc = Location.none }

let ident x = Exp.ident (loc_ident x)

let pvar x = Pat.var (loc_str x)

let fresh_ident () = ident (gensym ())

let fresh_var () = pvar (gensym ())

let call f args =
  let f = Option.get @@ Longident.unflatten f in
  let args = List.map (fun x -> (Asttypes.Nolabel, x)) args in
  Exp.(apply (ident (loc f)) args)

let proj_constant_opt (e : Parsetree.expression) =
  let open Parsetree in
  match e.pexp_desc with Pexp_constant c -> Some c | _ -> None

let proj_integer_opt (c : Parsetree.constant) =
  match c with
  | Parsetree.Pconst_integer (s, None) -> Some (int_of_string s)
  | Parsetree.Pconst_integer (s, Some c) ->
      Some (int_of_string (s ^ String.make 1 c))
  | Parsetree.Pconst_char _
  | Parsetree.Pconst_string (_, _, _)
  | Parsetree.Pconst_float (_, _) ->
      None

let proj_integer (c : Parsetree.constant) =
  match c with
  | Parsetree.Pconst_integer (s, None) -> int_of_string s
  | Parsetree.Pconst_integer (s, Some c) -> int_of_string (s ^ String.make 1 c)
  | Parsetree.Pconst_char _
  | Parsetree.Pconst_string (_, _, _)
  | Parsetree.Pconst_float (_, _) ->
      assert false

let reduce (lhs : Parsetree.expression) (rhs : Parsetree.expression) op =
  let open Parsetree in
  match (lhs.pexp_desc, rhs.pexp_desc) with
  | (Pexp_constant c1, Pexp_constant c2) ->
      let i1 = proj_integer c1 in
      let i2 = proj_integer c2 in
      Some (op i1 i2)
  | _ -> None

(* ---------------------------------------------- *)

module Repr = struct
  type 'a m = Parsetree.expression
end

module Sequencing : Sequencing with type 'a m = 'a Repr.m = struct
  include Repr

  let seq (x : Parsetree.expression) f =
    match x.pexp_desc with
    | Parsetree.Pexp_construct ({ txt = Longident.Lident "()"; loc = _ }, _) ->
        f ()
    | _ -> Exp.sequence x (f ())

  let ( let* ) (m : Parsetree.expression) f =
    let open Parsetree in
    match m.pexp_desc with
    | Pexp_constant _ | Pexp_ident _
    | Pexp_construct ({ txt = Longident.Lident "()"; loc = _ }, _)
    | Pexp_construct ({ txt = Longident.Lident "true"; loc = _ }, _)
    | Pexp_construct ({ txt = Longident.Lident "false"; loc = _ }, _) ->
        f m
    | _ ->
        let sym = gensym () in
        let id = ident sym in
        let var = pvar sym in
        Exp.let_ Nonrecursive [Vb.mk var m] (f id)

  let unit = Exp.construct (loc_ident "()") None
end

module Loop : Loop with type 'a m = 'a Repr.m and type index = int = struct
  include Repr

  type index = int

  let for_ ~(start : Parsetree.expression) ~(stop : Parsetree.expression) body =
    let default () =
      let sym = gensym () in
      let index_var = pvar sym in
      let index = ident sym in
      Exp.for_ index_var start stop Upto (body index)
    in
    match (start.pexp_desc, stop.pexp_desc) with
    | (Pexp_constant c1, Pexp_constant c2) ->
        let i1 = proj_integer c1 in
        let i2 = proj_integer c2 in
        if i1 = i2 then body start
        else if i2 < i1 then Sequencing.unit
        else default ()
    | _ -> default ()

  let while_ ~cond body = Exp.while_ (cond ()) (body ())
end

module Lambda : Lambda with type 'a m = 'a Repr.m = struct
  include Repr

  let lam f =
    let sym = gensym () in
    let patt = pvar sym in
    let var = ident sym in
    Exp.fun_ Nolabel None patt (f var)

  let app x y = Exp.apply x [(Nolabel, y)]
end

module Product : Product with type 'a m = 'a Repr.m = struct
  include Repr

  let prod x y = Exp.tuple [x; y]

  let fst (x : Parsetree.expression) =
    match x.pexp_desc with
    | Parsetree.Pexp_tuple [e1; _] -> e1
    | _ ->
        let fst_id = Option.get @@ Longident.unflatten ["Stdlib"; "fst"] in
        Exp.(apply (ident (loc fst_id)) [(Nolabel, x)])

  let snd (x : Parsetree.expression) =
    match x.pexp_desc with
    | Parsetree.Pexp_tuple [_; e2] -> e2
    | _ ->
        let snd_id = Option.get @@ Longident.unflatten ["Stdlib"; "snd"] in
        Exp.(apply (ident (loc snd_id)) [(Nolabel, x)])
end

module Ref = struct
  let field_id = Longident.Lident "contents"

  let create init = Exp.record [(loc field_id, init)] None

  let set r x = Exp.setfield r (loc field_id) x

  let get r = Exp.field r (loc field_id)
end

module Make_storage (X : sig
  type t
end) : Storage with type 'a m = 'a Repr.m and type elt = X.t = struct
  include Repr

  type t = X.t ref

  type elt = X.t

  include Ref
end

module Bool : Bool with type 'a m = 'a Repr.m = struct
  include Repr

  let true_ = Exp.construct (loc_ident "true") None

  let false_ = Exp.construct (loc_ident "false") None

  let is_true (exp : Parsetree.expression) =
    let open Parsetree in
    match exp.pexp_desc with
    | Pexp_construct ({ txt = Longident.Lident "true"; loc = _ }, _) -> true
    | _ -> false

  let is_false (exp : Parsetree.expression) =
    let open Parsetree in
    match exp.pexp_desc with
    | Pexp_construct ({ txt = Longident.Lident "false"; loc = _ }, _) -> true
    | _ -> false

  let ( || ) (lhs : Parsetree.expression) (rhs : Parsetree.expression) =
    let open Parsetree in
    match (lhs.pexp_desc, rhs.pexp_desc) with
    | ( Pexp_construct ({ txt = Longident.Lident t1; loc = _ }, _),
        Pexp_construct ({ txt = Longident.Lident t2; loc = _ }, _) ) -> (
        match (t1, t2) with
        | ("true", _) | (_, "true") -> true_
        | ("false", "false") -> false_
        | _ -> assert false)
    | _ ->
        if is_true lhs then true_
        else if is_false lhs then rhs
        else if is_true rhs then true_
        else if is_false rhs then lhs
        else call ["||"] [lhs; rhs]

  let ( && ) (lhs : Parsetree.expression) (rhs : Parsetree.expression) =
    let open Parsetree in
    match (lhs.pexp_desc, rhs.pexp_desc) with
    | ( Pexp_construct ({ txt = Longident.Lident t1; loc = _ }, _),
        Pexp_construct ({ txt = Longident.Lident t2; loc = _ }, _) ) -> (
        match (t1, t2) with
        | ("false", _) | (_, "false") -> false_
        | ("true", "true") -> true_
        | _ -> assert false)
    | _ ->
        if is_false lhs then false_
        else if is_true lhs then rhs
        else if is_false rhs then false_
        else if is_true rhs then lhs
        else call ["&&"] [lhs; rhs]

  let dispatch (expr : Parsetree.expression) f =
    let open Parsetree in
    match expr.pexp_desc with
    | Pexp_construct ({ txt = Longident.Lident b; loc = _ }, _) -> (
        match b with "true" -> f true | "false" -> f false | _ -> assert false)
    | _ -> Exp.ifthenelse expr (f true) (Some (f false))
end

module Make_enum (X : sig
  type t

  val enum : t -> int

  val const : t -> Parsetree.expression

  val all : t array
end) : Enum with type 'a m = 'a Repr.m and type t = X.t = struct
  include Repr

  type t = X.t

  let enum = X.enum

  let const = X.const

  let all = X.all

  let dispatch x f =
    let cases =
      Array.to_list X.all
      |> List.map (fun x ->
             let patt = Pat.constant (Const.int (enum x)) in
             let body = f x in
             Exp.case patt body)
    in
    let catch_all = Exp.case (Pat.any ()) (call ["assert"] [Bool.false_]) in
    Exp.(match_ x (cases @ [catch_all]))
end

module Make_array (X : sig
  type t
end) :
  Array with type 'a m = 'a Repr.m and type elt = X.t and type index = int =
struct
  include Repr

  type t

  type elt = X.t

  type index = int

  let get a i = call ["Array"; "get"] [a; i]

  let set a i v = call ["Array"; "set"] [a; i; v]

  let make i v = call ["Array"; "make"] [i; v]

  let length a = call ["Array"; "length"] [a]

  let unsafe_get a i = call ["Array"; "unsafe_get"] [a; i]

  let unsafe_set a i v = call ["Array"; "unsafe_set"] [a; i; v]

  let copy a = call ["Array"; "copy"] [a]

  let blit src ofs1 dst ofs2 len =
    call ["Array"; "blit"] [src; ofs1; dst; ofs2; len]

  let sub a start len = call ["Array"; "sub"] [a; start; len]
end

module Exn : Exn with type 'a m = 'a Repr.m = struct
  include Repr

  let raise_ exn =
    let str = Printexc.to_string exn in
    call ["failwith"] [Exp.constant (Const.string str)]
end

(* ------------------------------------------------------------------------- *)

module Int : Ring with type 'a m = 'a Repr.m with type t = int = struct
  include Repr

  type t = int

  let zero = Exp.constant (Const.int 0)

  let one = Exp.constant (Const.int 1)

  let is_zero exp =
    match proj_constant_opt exp with
    | None -> false
    | Some c -> (
        match proj_integer_opt c with None -> false | Some i -> i = 0)

  let is_one exp =
    match proj_constant_opt exp with
    | None -> false
    | Some c -> (
        match proj_integer_opt c with None -> false | Some i -> i = 1)

  let add (x : Parsetree.expression) (y : Parsetree.expression) =
    match reduce x y (fun i1 i2 -> Exp.constant (Const.int (i1 + i2))) with
    | None -> if is_zero x then y else if is_zero y then x else call ["+"] [x; y]
    | Some res -> res

  let sub x y =
    match reduce x y (fun i1 i2 -> Exp.constant (Const.int (i1 - i2))) with
    | None -> if is_zero x then y else if is_zero y then x else call ["-"] [x; y]
    | Some res -> res

  let mul x y =
    match reduce x y (fun i1 i2 -> Exp.constant (Const.int (i1 * i2))) with
    | None -> if is_one x then y else if is_one y then x else call ["*"] [x; y]
    | Some res -> res

  let neg (x : Parsetree.expression) =
    let open Parsetree in
    match x.pexp_desc with
    | Pexp_constant c ->
        let i = proj_integer c in
        Exp.constant (Const.int ~-i)
    | _ -> call ["~-"] [x]

  let of_int i = Exp.constant (Const.int i)
end

module Int_order : Infix_order with type 'a m = 'a Repr.m and type t = int =
struct
  include Repr

  type t = int

  let ( < ) x y =
    match
      reduce x y (fun i1 i2 -> if i1 < i2 then Bool.true_ else Bool.false_)
    with
    | None -> call ["<"] [x; y]
    | Some res -> res

  let ( > ) x y =
    match
      reduce x y (fun i1 i2 -> if i1 > i2 then Bool.true_ else Bool.false_)
    with
    | None -> call [">"] [x; y]
    | Some res -> res

  let ( <= ) x y =
    match
      reduce x y (fun i1 i2 -> if i1 <= i2 then Bool.true_ else Bool.false_)
    with
    | None -> call ["<="] [x; y]
    | Some res -> res

  let ( >= ) x y =
    match
      reduce x y (fun i1 i2 -> if i1 >= i2 then Bool.true_ else Bool.false_)
    with
    | None -> call [">="] [x; y]
    | Some res -> res

  let ( = ) (x : Parsetree.expression) (y : Parsetree.expression) =
    let open Parsetree in
    match (x.pexp_desc, y.pexp_desc) with
    | (Pexp_constant c1, Pexp_constant c2) ->
        let i1 = proj_integer c1 in
        let i2 = proj_integer c2 in
        if i1 = i2 then Bool.true_ else Bool.false_
    | (Pexp_ident { txt = i1; loc = _ }, Pexp_ident { txt = i2; loc = _ }) ->
        if Stdlib.(i1 = i2) then Bool.true_ else call ["="] [x; y]
    | _ -> call ["="] [x; y]

  let ( <> ) (x : Parsetree.expression) (y : Parsetree.expression) =
    let open Parsetree in
    match (x.pexp_desc, y.pexp_desc) with
    | (Pexp_constant c1, Pexp_constant c2) ->
        let i1 = proj_integer c1 in
        let i2 = proj_integer c2 in
        if i1 <> i2 then Bool.true_ else Bool.false_
    | (Pexp_ident { txt = i1; loc = _ }, Pexp_ident { txt = i2; loc = _ }) ->
        if Stdlib.(i1 = i2) then Bool.false_ else call ["<>"] [x; y]
    | _ -> call ["<>"] [x; y]
end

module Int_const : Const with type 'a m = 'a Repr.m with type t = int = struct
  include Repr

  type t = int

  let const i = Exp.constant (Const.int i)
end

(* ------------------------------------------------------------------------- *)

module Float : Field with type 'a m = 'a Repr.m and type t = float = struct
  include Repr

  type t = float

  let zero = Exp.constant (Const.float "0.0")

  let one = Exp.constant (Const.float "1.0")

  let add x y = call ["+."] [x; y]

  let sub x y = call ["-."] [x; y]

  let mul x y = call ["*."] [x; y]

  let neg x = call ["~-."] [x]

  let div x y = call ["/."] [x; y]

  let of_int i = Exp.constant (Const.float (string_of_int i))
end

module Float_order : Infix_order with type 'a m = 'a Repr.m and type t = float =
struct
  include Repr

  type t = float

  let ( < ) x y = call ["<"] [x; y]

  let ( > ) x y = call [">"] [x; y]

  let ( <= ) x y = call ["<="] [x; y]

  let ( >= ) x y = call [">="] [x; y]

  let ( = ) x y = call ["="] [x; y]

  let ( <> ) x y = call ["<>"] [x; y]
end

module Float_const : Const with type 'a m = 'a Repr.m with type t = float =
struct
  include Repr

  type t = float

  let const f = Exp.constant (Const.float (string_of_float f))
end
