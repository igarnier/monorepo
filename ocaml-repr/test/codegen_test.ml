module Codegen = Ocaml_repr.Ocaml_lang
module BL = Basic_impl.Lang

let repeat n x = List.flatten @@ List.init n (fun _ -> x)

let _passes =
  let open Simplifier.Components in
  let module R = struct
    type t = int
  end in
  let module E = struct
    type t = float
  end in
  let pair_simpl = pair_simplification (module R) (module E) in
  let array_simpl = array_simplification (module R) (module E) in
  let bool_simpl = constant_folding_bool (module BL.Int) (module E) in
  let ring_simpl = constant_folding_ring (module BL.Int) (module E) in
  let simpl = [pair_simpl; array_simpl; bool_simpl; ring_simpl] in
  let unroll = loop_unrolling 2 (module E) in
  let dead_code = dead_code (module R) (module E) in
  compose @@ repeat 10 (repeat 10 simpl @ [unroll]) @ [dead_code]

let passes =
  let open Simplifier.Components in
  let module R = struct
    type t = int
  end in
  let module E = struct
    type t = float
  end in
  let pair_simpl = pair_simplification (module R) (module E) in
  let dead_code = dead_code (module R) (module E) in
  compose [pair_simpl; dead_code]

module Pipeline = (val passes)

module Simpl = Ocaml_repr.Simpl.Make_float (Codegen.Int)
module Lang = Simpl.Apply (Pipeline)
module Monad = Basic_impl.Monad.Codegen_cps (Lang.Repr) (Lang.Sequencing)
open Monad.Infix

let program =
  let open Lang in
  let*! useless = Sequencing.unit in
  let*! x = Product.prod (Lang.Int_cst.const 2) useless in
  let*! y = Int_cst.const 3 in
  let*! _z = Array.make (Int_cst.const 2) (Field_cst.const 0.0) in
  Monad.return (Int.add y (Product.fst x))

let result = Lang.prj @@ Monad.run program

let () = Pprintast.expression Format.std_formatter result
